// JS
// Note: touchstart event handler was introduced to fix safari ios bug that doesn't recognise click events

const buttons = document.querySelectorAll('.btn');
const signinBtn = document.querySelector('.signin-btn');
const signupModal = document.querySelector('.signup-modal');

function showElement(e) {
	//e.stopPropagation();
	e.preventDefault(); // prevents buttons from scrolling page to the top
	const formData = this.dataset.show; //gets value of data-show attribute from clicked button
	const displayForm = document.querySelector(`.form[data-show="${formData}"]`); // gets equivalent form for with data-show
	const modalCheck = displayForm.parentNode.className; // gets if the form belongs to a modal
	this.classList.add('active'); // adds active class css to the clicked button if any

	if (modalCheck == 'modal') {
		displayForm.parentNode.classList.add('show'); // displays modal
		document.querySelector('body').classList.add('no-scroll'); // prevents body from scrolling when modal pops up
	}
	setTimeout(function () {
		displayForm.classList.add('show'); // displays form
	}, 100);

	displayForm.addEventListener('click', noClick); // stops form from hiding itself when clicked
	displayForm.addEventListener('touchstart', noClick);

	// function to hide form once anywhere outside the window is clicked
	function hideElement(e) {
		displayForm.classList.remove('show');
		setTimeout(function (e) {
			displayForm.parentNode.classList.remove('show');
		}, 300);
		signinBtn.classList.remove('active');
		document.querySelector('body').classList.remove('no-scroll');
	}
	window.addEventListener('click', hideElement);
	window.addEventListener('touchstart', hideElement);
}


function noClick(e) {
	e.stopPropagation();
}

buttons.forEach(button => button.addEventListener("click", noClick));
buttons.forEach(button => button.addEventListener("touchstart", noClick));
buttons.forEach(button => button.addEventListener("click", showElement));
buttons.forEach(button => button.addEventListener("touchstart", showElement));


//i need user,s information to determin location
var visitor_infos = $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
	sessionStorage.setItem('user_country', data.country_code);
});

var api_url = document.head.querySelector("[property=api_url]").content;

//Start code for authentication and registration process
var hfhApp = angular.module('hfhApp', [
	'ngStorage'
])
.service('Auth',function($localStorage,$http){

	var token,
		store = $localStorage,
		hermes = $http,
		authService = {};

	/**
	*	Login
	*
	*	Authenticate User
	*
	*	@param object authentication credentials
	*/
	authService.login = function(params){
		return hermes.post(api_url+"login/store",params);
	};

	/**
	*	Signup
	*
	*	Register new User
	*
	*	@param object new user details
	*/
	authService.signup = function(params){
		return hermes.post(api_url+"register/store",params);
	};

	/**
	*	Set authorization token
	* 	@param string token
	*/
	authService.setAuthorization = function(token){

		store.Authorization = token;
	};

	/**
	* 	Set User
	*
	*	Saves info about the currently logged in user
	*
	* 	@param object user
	*/
	authService.saveUser = function(user){

		store.user = user;
	};

  /**
	 *
	 * Get user's authentication status
	 *
   * @returns {boolean} User's authentication status
   */
  authService.isLoggedIn = function () {

		return !!store.Authorization;
	};

  	/**
	*	Set User Roles
	*
	*	@param array roles
	*/
	authService.setUserRoles = function(roles){

		authService.roles = store.roles = roles; // temp measure
	};

	/**
	*	Set Freelancer Verification Status
	*
	*	@param array freelancer verification status stack
	*/
	authService.setFreelancerVerificationStatus = function(statusStack){

		authService.verificationStatusStack = statusStack;
	};

	return authService;
})
.controller('hfhAppCtrl', ['$scope', '$http', '$window', '$timeout', 'Auth', function ($scope, $http, $window, $timeout, Auth) {

	$scope.error = {};
	$scope.error.message = "";
	$scope.signinFormParams = {};
	$scope.isSignupSuccessful = false;

	$scope.doLogin = doLogin;
	$scope.doSignup = doSignup;

	/**
	 * Process the signing up of user
	 * 
	 * @return 
	 */
	function doLogin () {

		$scope.isLoading = true;
		$scope.error.message = "";

		//actually authenticate the user
		Auth.login($scope.signinFormParams).then(function(response){

			// persist the user			
			Auth.saveUser(response.data.user);
			Auth.setAuthorization(response.data.user.token);

			// transparently save the various roles this user has been asssigned
			var role_users = response.data.user.role_users;
			var userRoleArray = [];
			for (var j in role_users) {
				console.dir(role_users[j].role.name);
				userRoleArray.push(role_users[j].role.name);
			}
			Auth.setUserRoles(userRoleArray);

			// not yet set
			//Auth.setFreelancerVerificationStatus(response.data.user.verification);

			// wait for the above to be set
			$timeout(function(){
				window.location.href = window.location.href+'jobs';
			},300);

		},function(error){

		    console.dir(error.data.error);
		    
		    $scope.error.message = error.data.error;
		    
		}).finally(function(){
			$scope.isLoading = false;
		});
	}

	/**
	*	Signup
	*
	*	@param object new user credentials
	*/
	function doSignup(){

		console.dir("signup button clicked");

		$scope.isLoading = true;

		$scope.error.message = "";

		//actually authenticate the user
		Auth.signup($scope.signupFormParams).then(function(response){

			//persist user && prompt user to verify their email
			// Auth.saveUser(response.data.user);
			// Auth.setAuthorization(response.data.token);

			// //transparently save the various roles this user has been assigned
			// Auth.setUserRoles(response.data.roles);

			// //save freelancer verification status
			// Auth.setFreelancerVerificationStatus(response.data.verification);

			$scope.isSignupSuccessful = true;

			swal("Good job!", 
				"We have sent you an email to verify your account, please check your mail inbox or spam and click the verification link.", 
				"success"
			);
					 
		},function(error){

			$scope.error.message = error.data.message;

		}).finally(function(){
			$scope.isLoading = false;
		});
	};

}])
