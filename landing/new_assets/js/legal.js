// Javascript fot the Leagal anns Support pages



const links = document.querySelectorAll('.nav-link');

function showContent(e){
    const contentData = this.dataset.show;
    const displayContent = document.querySelector(`.content[data-show="${contentData}"]`);

    const visibleContent = document.querySelector('.content.show');
    console.log(displayContent);
    visibleContent.classList.remove('show');

    displayContent.classList.add('show');

    const activeLink = document.querySelector('.nav-link.active');
    activeLink.classList.remove('active');
    this.classList.add('active');
};


links.forEach(link => link.addEventListener("click", showContent));
