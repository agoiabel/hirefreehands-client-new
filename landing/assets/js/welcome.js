//Import Angular
var angular = require('../../../node_modules/angular');

//Import other dependencies
require('angular-ui-router');
require('../../../node_modules/angular-touch');
require('../../../node_modules/angular-material');
require('../../../node_modules/ng-storage');
require('../../../node_modules/angular-avatar');
require('../../../src/js/modules/users');
require('../../../public/lib/hammerjs/hammer.js');
require('../../../public/lib/angular-carousel/angular-carousel.js');

window.$ = window.jQuery = require('../../../public/lib/jquery/dist/jquery.js');

var config = require('../../../src/js/modules/config');


// JS
// Note: touchstart event handler was introduced to fix safari ios bug that doesn't recognise click events

const buttons = document.querySelectorAll('.btn');
const signinBtn = document.querySelector('.signin-btn');
const signupModal = document.querySelector('.signup-modal');

function showElement(e) {
	//e.stopPropagation();
	e.preventDefault(); // prevents buttons from scrolling page to the top
	const formData = this.dataset.show; //gets value of data-show attribute from clicked button
	const displayForm = document.querySelector(`.form[data-show="${formData}"]`); // gets equivalent form for with data-show
	const modalCheck = displayForm.parentNode.className; // gets if the form belongs to a modal
	this.classList.add('active'); // adds active class css to the clicked button if any

	if (modalCheck == 'modal') {
		displayForm.parentNode.classList.add('show'); // displays modal
		document.querySelector('body').classList.add('no-scroll'); // prevents body from scrolling when modal pops up
	}
	setTimeout(function () {
		displayForm.classList.add('show'); // displays form
	}, 100);

	displayForm.addEventListener('click', noClick); // stops form from hiding itself when clicked
	displayForm.addEventListener('touchstart', noClick);

	// function to hide form once anywhere outside the window is clicked
	function hideElement(e) {
		displayForm.classList.remove('show');
		setTimeout(function (e) {
			displayForm.parentNode.classList.remove('show');
		}, 300);
		signinBtn.classList.remove('active');
		document.querySelector('body').classList.remove('no-scroll');
	}
	window.addEventListener('click', hideElement);
	window.addEventListener('touchstart', hideElement);
}


function noClick(e) {
	e.stopPropagation();
}

buttons.forEach(button => button.addEventListener("click", noClick));
buttons.forEach(button => button.addEventListener("touchstart", noClick));
buttons.forEach(button => button.addEventListener("click", showElement));
buttons.forEach(button => button.addEventListener("touchstart", showElement));



//define app
angular.module('hfhAuth',[
	'ui.router',
	'ngStorage',
	'ngMaterial',
	'ngAvatar',
	'angular-carousel',
	'usersModule'
])
.config(function($stateProvider,$urlRouterProvider,$locationProvider){

	//switch on html5mode
	$locationProvider.html5Mode(true);

	//register routes
	$stateProvider
		.state('home',require('../../pages/route.js').landing)
		.state('about',require('../../pages/route.js').about)
		.state('help',require('../../pages/route.js').help)
		.state('legal',require('../../pages/route.js').legal);
})
.run(function($rootScope,$timeout,$location,$state,$http,$mdDialog,Auth){
    
    /**
     * This is use to display loading icon before page fully loads
     * 
     * @type 
     */
    $rootScope.showLoadingIcon = true;
    $rootScope.$on('$viewContentLoaded', function()
    {
        $rootScope.showLoadingIcon = false;
        console.dir("view inserted successfully");
    });

	$rootScope.activeForm = 'signup';

	$rootScope.setActiveForm = setActiveForm;
	$rootScope.toggleAuthDropdown = toggleAuthDropdown;
	$rootScope.isActiveForm = isActiveForm;
	$rootScope.closeAuthForm = closeAuthForm;
	$rootScope.showAuthForm = showAuthForm;

	$rootScope.doLogin = doLogin;
	$rootScope.doSignup = doSignup;
	$rootScope.error = {message:""};

	$rootScope.activePage = 'welcome';
	$rootScope.setActivePage = setActivePage;
	$rootScope.isActivePage = isActivePage;

	$rootScope.imageUrl = imageUrl;

	$rootScope.getUserInitials = getUserInitials;

	$rootScope.job = {addons: [],hasMilestones: '0'};

	//create job
	$rootScope.createJob = createJob;
	$rootScope.signinFormParams = {};
	$rootScope.signupFormParams = {};

	function createJob(e,category){

		$mdDialog.show({

	      	locals: {job: $rootScope.job, categories: $rootScope.categories},
	      	controller: function($scope,job,categories){

	      		job.category = category;

	      		$scope.job = job;

	      		$scope.job.moreSkills = [];
	      		$scope.postJob = postJob;

	      		$scope.addMoreSkills = false;
	      		$scope.toggleAddMoreSkills = toggleAddMoreSkills;
	      		$scope.querySkills = querySkills;
	      		$scope.setJobPrice = setJobPrice;
	      		$scope.customizeJobPrice = customizeJobPrice;

	      		$scope.addAddon = addAddon;
	      		$scope.closeDialog = closeDialog;

	      		//monitor for changes to the milestone option
			    $scope.$watch('job.hasMilestones',function(){
			    	addMilestones();
			    });

			    //get an aggregate of all skills, this is used when user searches for more skills
			    var allSkills = [];
			    categories.forEach(function(cat){
			    	allSkills = allSkills.concat(cat.skills.data);
			    });

			    //make the skills assigned to this category be selected by default
				$scope.job.skills = $scope.job.category.skills.data;

				//let's limit skills selected to 5
				$scope.job.skills.splice(5);

				function postJob(job){

					$mdDialog.cancel();
					document.getElementById('auth-overlay').style.display = 'block';
				}

			   	/**
			    *	Search for skills.
			    */
			    function querySkills (query) {

			      	var results = query ? allSkills.filter(createFilterFor(query)) : allSkills;
			      	return results;
			    }

			    /**
			    *	Create filter function for a query string
			    */
			    function createFilterFor(query) {

			      	var lowercaseQuery = angular.lowercase(query);

			      	return function filterFn(skill) {
			        	return skill.name.toLowerCase().indexOf(lowercaseQuery) !== -1;
			      	};
			    }

			    /**
			    *	Toggle Add More Skills
			    */
			    function toggleAddMoreSkills(){
			    	console.log('Toggled');
			    	$scope.addMoreSkills = !$scope.addMoreSkills;
			    }

			    /**
			    *	Set Job Price
			    *
			    *	@param string current size tier price point
			    */
			    function setJobPrice(tierPricePoint){
			    	$scope.job.price = tierPricePoint;
			    }

			    /**
			    *	Customize Job Price
			    *
			    *	Either increase or decrease base tier price by 10% max
			    *	in steps of 1000
			    *
			    *	@param string direction either increase or decrease to a max +/-10%
			    */
			    function customizeJobPrice(direction){

			    	if($scope.job.price === undefined){
			    		alert('You need to specify how complex your job is and how long it should take before modifying the evaluated job price');
			    		return;
			    	}

			    	$scope.job.price = Number($scope.job.price);
			    	$scope.job.tier.price_point = Number($scope.job.tier.price_point);

			    	var upperLimit = $scope.job.tier.price_point + ($scope.job.tier.price_point * 0.1);
			    	var lowerLimit = $scope.job.tier.price_point - ($scope.job.tier.price_point * 0.1);

			    	//steps to increase or decrease by
			    	var step = 1000;

			    	if(direction === 'increase' && $scope.job.price + step <= upperLimit){

			    		$scope.job.price = $scope.job.price + step;

			    	}else if(direction === 'decrease' && $scope.job.price - step > lowerLimit){

			    		$scope.job.price = $scope.job.price - step;

			    	}else if($scope.job.price - step < lowerLimit){

			    		alert('You cannot reduce the amount past this point');

			    	}else if($scope.job.price + step > upperLimit){

			    		alert('You cannot increase the amount past this point');
			    	}
			    }

			    /**
			    *	Add Adons
			    *
			    *	@param object addon
			    */
			    function addAddon(addon){

			    	if($scope.job.price === undefined){

				    	alert('You need to specify how complex your job is and how long it should take before adding addons.');

				    	//uncheck this addon
				    	addon.isChecked = false;
				    	return;
				    }

			    	var addonExists = $scope.job.addons.find(function(ad){
			    		return addon.id === ad.id;
			    	});

			    	addon.weight = Number(addon.weight);
			    	$scope.job.price = Number($rootScope.job.price);

			    	if(!Boolean(addonExists)){

			    		$scope.job.addons.push(addon);

			    		//modify the current job evaluated price
			    		$scope.job.price += $rootScope.job.tier.price_point * (addon.weight/100);
			    	}else{

			    		$scope.job.addons.forEach(function(ad,$index){
			    			if(addon.id === ad.id){

			    				console.log(addon);

			    				//modify the current job evaluated price
			    				$scope.job.price -= $rootScope.job.tier.price_point * (addon.weight/100);

			    				$scope.splice($index,1);
			    			}
			    		});
			    	}
			    }

			    /**
			    *	Add Milestones
			    *
			    *	Increases evaluated price by the given
			    *	categories milestone weight value
			    */
			    function addMilestones(){

			    	//if milestones have been selected, increase evaluated job price
			    	if($scope.job.price !== undefined){

			    		var milestoneWeight = Number($scope.job.category.milestone_weight);
			    		$scope.job.price = Number($scope.job.price);

				    	if($scope.job.hasMilestones === '0'){

				    		$scope.job.price -= Number($scope.job.tier.price_point) * milestoneWeight/100;
				    	}else{
				    		$scope.job.price += Number($scope.job.tier.price_point) * milestoneWeight/100;
				    	}
				    }else{

				    	if($scope.job.hasMilestones === '1'){

					    	alert('You need to specify how complex your job is and how long it should take before selecting milestones');

					    	//reset the radio button
					    	$scope.job.hasMilestones = '0';
					    }
				    }
			    }

			    function closeDialog(){

			    	//reset job template
			    	$scope.job.title = '';
			    	$scope.job.description = '';
			    	$scope.job.skills = [];
			    	$scope.job.addons = [];
					$scope.job.hasMilestones = '0';
					$scope.job.price = undefined;
					$scope.job.size = {options:[]};

			    	$mdDialog.cancel();
			  	}
	      	},
	      	templateUrl: 'templates/jobs/create-form.html',
	      	parent: angular.element(document.body),
	      	targetEvent: e,
	      	clickOutsideToClose:true
	    });
	}

	/**
	 * Get the visitors location
	 * 
	 * @type 
	 */
	var visitor_infos = $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
		console.dir(data);
		sessionStorage.setItem('user_country', data.country_code);
	});

	/**
	*	ImageUrl
	*
	*	Prefix with the base url for images
	*/
	function imageUrl(image){
		return config.imageUrl(image);
	};

	/**
	*	Set Active Page
	*
	*	@param string page title
	*	@param object event
	*/
	function setActivePage(page,e){

		var currentPath = $location.path();

		if(currentPath !== '/'){
			$state.go('home');
		}

		$rootScope.activePage = page;
		e.preventDefault()	;
	}

	/**
	*	Is Active Page
	*
	*	@param string page title
	*/
	function isActivePage(page){
		return $rootScope.activePage === page;
	}

	/**
	*	doLogin
	*
	*	@params object user credentials
	*/
	function doLogin(){

		console.dir($scope.signinFormParams);

		$rootScope.isLoading = true;
		$rootScope.error.message = "";

		//actually authenticate the user
		Auth.login($scope.signinFormParams).then(function(response){

			// persist the user
			Auth.saveUser(response.data.user);
			Auth.setAuthorization(response.data.token);

			// transparently save the various roles this user has been asssigned
			Auth.setUserRoles(response.data.roles);

			console.dir(response.data.roles);

			// save freelancer verification status
			// Auth.setFreelancerVerificationStatus(response.data.verification);

			// // wait for the above to be set
			// $timeout(function(){
			// 	window.location.href = window.location.href+'jobs';
			// },300);
		},function(error){

			/*$mdToast.show(
		      	$mdToast.simple().content(error.data.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );*/
		    console.dir(error.data.message);
		    
		    $rootScope.error.message = error.data.message;
		}).finally(function(){
			$rootScope.isLoading = false;
		});
	};

	/**
	*	Signup
	*
	*	@param object new user credentials
	*/
	function doSignup(){

		$rootScope.isLoading = true;

		$rootScope.error.message = "";

		console.dir($scope.signupFormParams);

		//actually authenticate the user
		Auth.signup($scope.signupFormParams).then(function(response){

			//persist user && prompt user to verify their email
			Auth.saveUser(response.data.user);
			Auth.setAuthorization(response.data.token);

			//transparently save the various roles this user has been assigned
			Auth.setUserRoles(response.data.roles);

			//save freelancer verification status
			Auth.setFreelancerVerificationStatus(response.data.verification);

			$rootScope.isSignupSuccessful = true;

		},function(error){

			/*$mdToast.show(
		      	$mdToast.simple().content(error.data.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );*/

			$rootScope.error.message = error.data.message;

		}).finally(function(){
			$rootScope.isLoading = false;
		});
	};

	/**
	*	Toggle auth dropdown
	*
	*	@param string form to set active
	*	@param object event
	*/
	function toggleAuthDropdown(form,$event){

		if(document.getElementById('auth-dropdown').style.display !== 'block'){
			document.getElementById('auth-dropdown').style.display = 'block';
		}else if(document.getElementById('auth-dropdown').style.display === 'block' && form === $rootScope.activeForm){
			document.getElementById('auth-dropdown').style.display = 'none';
		}

		setActiveForm(form,$event);
	}

	/**
	*	Set active form
	*
	*	@param string form to make active
	*	@param object form event object
	*/
	function setActiveForm(form,e){

		$rootScope.activeForm = form;
		e.preventDefault();
	}

	/**
	*	Is active form
	*
	*	@param string form to make active
	*/
	function isActiveForm(form){
		return form === $rootScope.activeForm;
	}

	/**
	*	Show Auth Form
	*
	*	@param object form event object
	*/
	function showAuthForm(e){
		document.getElementById('auth-overlay').style.display = 'block';
		e !== undefined ? e.preventDefault() : '';
	}

	/**
	*	Close/Hide Auth Form
	*
	*	@param object form event object
	*/
	function closeAuthForm(e){
		document.getElementById('auth-overlay').style.display = 'none';
		e !== undefined ? e.preventDefault() : '';
	}

	/**
	* Get user name initials.
	*/
	function getUserInitials (lastname, firstname) {

		return (lastname || firstname) ? (firstname[0] + lastname[0]).toUpperCase() : "";
	}

})
.service('Auth',function($localStorage,$http){

	var token,
		store = $localStorage,
		hermes = $http,
		authService = {};

	/**
	*	Login
	*
	*	Authenticate User
	*
	*	@param object authentication credentials
	*/
	authService.login = function(params){

		return hermes.post(config.baseAPI()+'auth',params);
	};

	/**
	*	Signup
	*
	*	Register new User
	*
	*	@param object new user details
	*/
	authService.signup = function(params){

		return hermes.post(config.baseAPI()+'auth/signup',params);
	};

	/**
	*	Set authorization token
	* 	@param string token
	*/
	authService.setAuthorization = function(token){

		store.Authorization = token;
	};

	/**
	* 	Set User
	*
	*	Saves info about the currently logged in user
	*
	* 	@param object user
	*/
	authService.saveUser = function(user){

		store.user = user;
	};

  /**
	 *
	 * Get user's authentication status
	 *
   * @returns {boolean} User's authentication status
   */
  authService.isLoggedIn = function () {

		return !!store.Authorization;
	};

  	/**
	*	Set User Roles
	*
	*	@param array roles
	*/
	authService.setUserRoles = function(roles){

		authService.roles = store.roles = roles; // temp measure
	};

	/**
	*	Set Freelancer Verification Status
	*
	*	@param array freelancer verification status stack
	*/
	authService.setFreelancerVerificationStatus = function(statusStack){

		authService.verificationStatusStack = statusStack;
	};

	return authService;
})
.service('Freelancer',function($http){

	var factory = {},
		hermes = $http;

	/**
	*	Get All
	*
	*	Retrieve all verified freelancers
	*/
	factory.getAll = function(){

		return hermes.get(config.baseAPI()+'freelancers');
	};

	return factory;
})
.directive('tab',require('../../../src/js/modules/utilities/directives/tab.js'))
.directive('starRating',require('../../../src/js/modules/utilities/directives/starRating.js'))
.directive('popover',require('../../../src/js/modules/utilities/directives/popover.js'))
.directive('accordion',require('../../../src/js/modules/utilities/directives/accordion.js'))
.directive('editContent',require('../../../src/js/modules/utilities/directives/editContent.js'))
.directive('unslider', require('../../../src/js/modules/utilities/directives/unslider.js'));
