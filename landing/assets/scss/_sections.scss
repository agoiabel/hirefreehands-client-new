section{
	width: 100%;

	.section-title{

		color: $color-grey-400;
      font-size: 2.5rem;
		font-weight: 300;
		line-height: 1.2;
	}

	.section-sub-title{

		color: $color-grey-300;
      font-size: 1.5rem;
		font-weight: 300;
		text-align: center;
	}

	.sub-section {
		box-sizing: border-box;
		margin: auto;
      padding: 20px;
	}
}

.container {

  box-sizing: border-box;
	margin: 0 auto;
	padding: 20px;
  position: relative;
	width: 80%;

	&.post-job-section,
	&.freelancer-section {min-height: 75vh;}

	@include mobile {
		width: 100%;
	}

	@include tablet {
		width: 100%;
	}

	@include desktop {
		width: 80%;
	}

	@include xl-desktop {
		width: 70%;
	}
}

.container-75{

	margin: 0 auto;
  width: 75%;
}

.container-40{

	margin: 0 auto;
  width: 40%;
}

.category-divider{

    color: #C7C7C7;
    font-size: 0.85rem;
    font-weight: 500;
    margin-bottom: 20px;
  text-align: center;
  text-transform: uppercase;
}

.tint{
	background: rgba(0,0,0,0.45);
	height: 100%;
  width: 100%;
}

#main-banner{

	background-repeat: no-repeat !important;
  background-size: cover !important;
  height: 95vh;
	width: 100%;

	slide{

		color: #fff;
		text-align: center;

      .container {
        align-items: center;
        display: flex;
        flex-direction: column;
        justify-content: center;

        @include mobile {
          width: 100%;
        }
      }

		h1{
			display: block;
    		font-size: 3.5rem;
          font-weight: 100;
          margin: 0;
          white-space: normal;

          @include mobile {
            font-size: 3rem;
          }
        }

      div.slide-desc {
			font-size: 1.2rem;
        position: relative;
        top: 12px;
        width: 100%;

        @include mobile {
          white-space: normal;
          width: 100%;
			}
		}

		.tint{
          align-items: center;
			background: rgba(0,0,0,0.5);
          display: inline-flex;
          justify-content: center;
        }
    }
}

#hfh-detail{

	p{
		color: $color-grey-200;
		font-size: 1.0rem;
		font-weight: 300;
		line-height: 1.5;
	}

	#explainer-vid{

		background-size: cover !important;
		background-repeat: no-repeat !important;
		height: 270px;
		margin-top: 2.9rem;
		margin-bottom: 2.8rem;
		text-align: center;

		.fa{font-size: 3rem;color: #fff;}

		.play{
			margin-top: 7rem;
		}
	}
	a{
		display:inline-block;
		color: $color-primary;
		margin-top: 1rem;
		border-radius: 3px;
		border: 1px solid $color-primary;
		padding: 1rem;
	}
}

.job-categories-section{

	background: white;

	.section-title{margin-bottom: 0;}
	.section-title,.section-sub-title{text-align: center;}
}

.jobs-section{
	.container{
		width: 75%;

		@include xl-desktop{
			width: 55%;
		}
	}
}

#freelancer-banner{

	height: 30rem;
	width: 100%;
	background-size: cover !important;
	background-repeat: no-repeat !important;
	background-attachment: fixed !important;
	text-align: center;

	.tint{background: rgba(0,0,0,0.7);}

	.pre-title{

		display: inline-block;
		text-transform: uppercase;
		color:rgba(255,255,255,0.75);
		padding: 10px 3px;
		margin-top: 50px;
		margin-bottom: 0;
		font-size: 0.9rem;
		font-weight: bold;
		border-bottom: 1px solid #fff;
	}

	h3{
		color: #fff;
		font-size: 3rem;
		font-weight: 300;
		margin: 20px 0;
	}

	.job-cat-starting-price {
		height: 20rem;
		width: 100%;
		margin: 0 auto;
	}

	.signup{
		margin-bottom: 50px;
	}
	.pure-g{
		@include mobile{
			display: none;
		}
	}
	.all_icons{
		display: none;
		@include mobile{
			display: block;
		}
	}
}

#freelance-jobs{

	background: #f7f7f7;

	.section-title{text-align: center;}
}

.single-page{
	h4{font-size: 1.2rem;color:$color-grey-300;}
}

footer{
	background: #333;

	h4{
		color: #fff;
		font-weight: bold;
		font-size: 0.95rem;
		margin-bottom: 30px;
	}

	span,a{
		display:block;
		font-size: 0.85rem;
		font-weight: 300;
		color: rgba(255,255,255,0.7);
		margin-bottom: 10px;

		&:hover{color: rgb(255,255,255);}
	}

	.logo{
		display: block;
		background: url('../imgs/logo-white.png');
		background-repeat: no-repeat;
		background-size:contain !important;
		width: 11.5rem;
		height: 2.5rem;
		margin-top: 10px;

		@include mobile{
			margin: 0 auto;
		}

		@include tablet{

			margin: 0 auto;
		}
	}

	#links{

		margin-top: 10px;
		padding-top: 11px;
		text-align: center;
		ul{
			display: inline-block;
			height: 40px;
			li{
				float:left;margin-right: 10px;

				a{font-size: 1rem;}
			}
		}
	}

	.copyright{

		@include mobile{
			display: block;
			text-align: center;
			margin-top: 20px;
			float: initial;
		}

		@include tablet{

			display: block;
			text-align: center;
			margin-top: 20px;
			float: initial;
		}
	}

	.social-btns-container{

		@include mobile{
			text-align:center;
		}

		@include tablet{
			text-align:center;
		}
	}

	.social-btns{
		margin-top: 10px;margin-bottom: 20px;float:right;

		@include mobile{

			display: block;
			margin: 0 auto;
			float: initial;
		}

		@include tablet{

			display: block;
			margin: 0 auto;
			float: initial;
		}
	}

	.social-btn{

		display:inline-block;
		background: rgba(255,255,255,0.7);
		width: 40px;
		height: 40px;
		margin-right: 10px;
		border-radius: 3px;
		text-align: center;
		padding: 12px;
		box-sizing: border-box;

		.fa{
			margin: 0;
			font-size: 1rem;
			color: #333;
		}
	}

	.bottom{

		margin-top: 10px;
		border-top: 1px solid rgba(255,255,255,0.2);
		padding: 20px 0;

		li{

			display:inline-block;
			margin-left: -5px;
			a{text-transform: capitalize;margin:0;}

			&:not(:last-child){padding: 0 10px;border-right: 1px solid rgba(255,255,255,0.5);}
			&:last-child{padding-left: 10px;}
		}
	}

	.subscribe{

		h4{margin-bottom: 30px;}
		div{
			background: #fff;
		}

		input{width: 100%;padding: 10px;font-size:0.95rem;border:0;outline:0;}
		.btn{width:100%;margin:0;border-radius:0;height: 37px;text-align:center;}
	}
}

#contact-us {

	width: 100%;
	height: 30rem;
	background-color: #f7f7f7;


	> div {

		height: inherit;
		width: inherit;
		display: block;
		background-size: cover !important;
		background-repeat: no-repeat !important;

		.tint{
			@extend .tint;
			background: rgba(0,0,0,0.7);
		}
	}

	#contact-us-text {
		font-size: 200%;
		color: #fff;
		margin-left: 45%;
		margin-right:auto;
		padding-top: 10%;
		display: inline-block;

		@include mobile{
			margin-left: 0;
			font-size: 100%;
			width: 100%;
			text-align: center;
			margin-top: 30%;
		}

		p {
			color: #fff;
		}
	}
}

#about-us-banner{
	width: 100%;
	height: 10rem;

	h1{
		font-size: 1.5rem;
		color: #fff;
		margin: 0;
		line-height: 10rem;
	}
}
