/**
 * Gulp Task Definitions
 */

//gather dependencies
var gulp = require('gulp');
var config = require('./tasks/config.js');

/**
 * Javascript Tasks
 */

//define javascript linting
var lint = require('./tasks/lint.js');
gulp.task('lint', lint);

//define browserify bundling task
var browserify = require('./tasks/browserify.js');
gulp.task('browserify',['lint'], browserify);

//define minification task for the bundle
var minify_js = require('./tasks/minify-js');
gulp.task('minify-js',['browserify'],minify_js);

//define landing scss compiling task
var combine_welcome_js = require('./tasks/combine-welcome-js.js');
gulp.task('combine-welcome-js',combine_welcome_js);

//define landing css minification task
var minify_welcome_js = require('./tasks/minify-welcome-js.js');
gulp.task('minify-welcome-js',['combine-welcome-js'],minify_welcome_js);

/**
* CSS tasks
*/

//define sass task
var sass = require('./tasks/sass.js');
gulp.task('sass', sass);

//define css minification task
var minify_css = require('./tasks/minify-css.js');
gulp.task('minify-css',['sass'], minify_css);

//define landing scss compiling task
var combine_welcome_css = require('./tasks/combine-welcome-css.js');
gulp.task('combine-welcome-css',combine_welcome_css);

//define landing css minification task
var minify_welcome_css = require('./tasks/minify-welcome-css.js');
gulp.task('minify-welcome-css',['combine-welcome-css'],minify_welcome_css);

/**
*	Utility Tasks
*/
var start_browser_sync_server = require('./tasks/start-browser-sync-server.js');
gulp.task('browserSync',['build'], start_browser_sync_server);

// Load in environment specific variables (App configurations).
var load_env_config = require('./tasks/load-enviroment-config.js');
gulp.task('load-env-config', load_env_config);

// Replace base url(hirefreehands.tech) with (staging.hirefreehands.com) on the staging env.
var replace_base_url = require('./tasks/replace-base-url.js');
gulp.task('replace-base-url', replace_base_url);

//define watch parameters
gulp.task('watch',['browserSync'],function(){

	// Watch our sass files
  gulp.watch([config.sass.watch],['minify-css']);

	// Watch our landing page sass files
  gulp.watch([config.sass.welcome_scss_glob],['minify-welcome-css']);

	// Watch our js files
  gulp.watch([config.js.watch],['minify-js']);

	// Watch our landing page js files
	gulp.watch([config.js.welcome_watch],['minify-welcome-js']);

});

// Build Pipeline.
var buildTasks = ['load-env-config', 'minify-js', 'minify-css', 'combine-welcome-css', 'combine-welcome-js'];

gulp.task('build', buildTasks);
gulp.task('build:production', buildTasks);
gulp.task('build:staging', ['replace-base-url'].concat(buildTasks));
gulp.task('default',['watch']);
