/**
 * Config File for TaskMe gulp tasks
 */
module.exports = {
	sass: {
		app: 'src/scss/',
		css: 'src/css/',
		production: 'public/dist/css/',
		watch: 'src/scss/**/*.scss',
		css_glob: 'src/css/**/*.css',
		welcome_css: 'landing/assets/css/',
		welcome_css_glob: 'landing/assets/css/**/*.css',
		welcome_scss_glob: 'landing/assets/scss/**/*.scss'
	},

	js: {
		app: 'src/js/',
		production: 'public/dist/js/',
		watch: 'src/js/**/*.js',
		index: 'src/js/app.js',  //entry point for the bundler
		bundle: 'public/dist/js/taskme.bundle.js',
		welcome: 'landing/assets/js/welcome.js',
		welcome_bundle: 'welcome.bundle.js',
		welcome_bundle_path: 'public/dist/js/welcome.bundle.js',
		welcome_watch: 'landing/assets/js/**/*.js',
	},

	html: {
		index: ['app.html', 'index.html']
	},

	app_config: {
		entry_point: "src/js/modules/config/config.json",
		output_dir: "src/js/modules/config/"
	}
};
