/**
 *	Browserify Gulp Task
 * 
 * 	Bundle all javascript for the landing page
 */

var browserify = require('browserify'),
	notify	   = require('gulp-notify'),
	source     = require('vinyl-source-stream'),
	gulp	   = require('gulp'),
    annotate   = require('gulp-ng-annotate'),
	config     = require('./config.js');	
	
//app dependency bundler
module.exports = function() {
    return browserify(config.js.welcome)
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe(source(config.js.welcome_bundle))
        //annotate bundle
        .pipe(annotate())
        // Start piping stream to tasks!        
        .pipe(notify('Welcome JS Bundled Successfully'))        
        .pipe(gulp.dest(config.js.production));
};