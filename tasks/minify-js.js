/**
 * Gulp Minification Task for the TaskMe bundle
 */

var notify = require('gulp-notify'),
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    config = require('./config.js');

module.exports = function () {

    return gulp.src(config.js.bundle)
        .pipe(uglify({
            // inSourceMap:
            // outSourceMap: "app.js.map"
        }))
        .pipe(rename({suffix: ".min"}))
        .pipe(notify("TaskMe AppJS Minification Complete"))
        .pipe(gulp.dest(config.js.production))
};
