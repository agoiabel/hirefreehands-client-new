/**
 * Gulp task to compile welcome/home page CSS assets
 */

var sass = require('gulp-sass'),
    notify = require('gulp-notify'),
    prefix = require('gulp-autoprefixer'),
    gulp = require('gulp'),
    config = require('./config.js');

//SASS & Auto-Prefixing function definition
module.exports = function () {

    return gulp.src(config.sass.welcome_scss_glob)
        .pipe(sass().on('error', sass.logError))
        .on('error', sass.logError)
        .pipe(notify("Landing SASS Conversion Complete."))
        .pipe(prefix("last 2 versions", "> 1%", "ie 8"))
        .pipe(notify("Auto-prefixing Complete."))
        .pipe(gulp.dest(config.sass.welcome_css));
};