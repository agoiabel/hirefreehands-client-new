/**
 * Gulp task to minify compiled CSS
 */

var notify = require('gulp-notify'),
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    minifycss = require('gulp-minify-css'),
    config = require('./config.js');

module.exports = function () {

    var opts = {comments: true, spare: true};

    return gulp.src(config.sass.css_glob)

        .pipe(minifycss(opts))
        .pipe(notify("CSS Minification Complete"))
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(config.sass.production));
};
