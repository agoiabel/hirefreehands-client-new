/**
 * SCSS Gulp Task
 * 
 * Gulp Task to convert scss to valid css
 */

//include depedencies
var config = require('./config.js');

var sass = require('gulp-sass');
var notify = require('gulp-notify');
var prefix = require('gulp-autoprefixer');
var gulp = require('gulp');

//SASS & Auto-Prefixing function definition
module.exports = function(){      
    return gulp.src(config.sass.watch)
      .pipe(sass().on('error', sass.logError))
      .pipe(notify("SASS Conversion Complete."))
      .pipe(prefix("last 2 versions", "> 1%", "ie 8"))
      .pipe(notify("Auto-prefixing Complete."))
      .pipe(gulp.dest(config.sass.css))
};