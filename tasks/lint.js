/**
 * Gulp lint task
 */

var notify = require('gulp-notify'),
    gulp = require('gulp'),
    stylish = require('jshint-stylish'),
    jshint = require('gulp-jshint'),
    config = require('./config.js');

module.exports = function () {

    return gulp.src(config.js.watch)
        .pipe(jshint())
        //.pipe(notify("Lint Complete"))
        .pipe(jshint.reporter(stylish));
};
