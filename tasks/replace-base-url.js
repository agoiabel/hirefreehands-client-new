var gulp = require('gulp'),
    notify = require('gulp-notify'),
    inject = require('gulp-inject-string'),
    config = require('./config.js');

module.exports = function () {

    return gulp.src(config.html.index)
        .pipe(inject.replace('http://hirefreehands.com/', 'http://staging.hirefreehands.com/'))
        .pipe(gulp.dest('./'));
};

