/**
 * Browserify Gulp Task
 * 
 * Bundle all javascript
 */

var browserify = require('browserify'),
	notify	   = require('gulp-notify'),
	source     = require('vinyl-source-stream'),
	gulp	   = require('gulp'), 
    annotate   = require('gulp-ng-annotate'),
	config     = require('./config.js');
	
//app dependency bundler
module.exports = function() {
    return browserify(config.js.index)
        .bundle()
        //Pass desired output filename to vinyl-source-stream
        .pipe(source('taskme.bundle.js'))
        //annotate bundle
        .pipe(annotate())
        // Start piping stream to tasks!
        .pipe(notify('Taskme AppJS Bundled Successfully'))
        .pipe(gulp.dest(config.js.production));
};