/**
 * Created by Lawrence on 24/11/2016.
 */

var gulp = require('gulp'),
    gulpNgConfig = require('gulp-ng-config'),
    config = require('./config.js');

module.exports = function () {

    return gulp.src(config.app_config.entry_point)
        .pipe(gulpNgConfig('Config', { environment: process.env.NODE_ENV || 'development' }))
        .pipe(gulp.dest(config.app_config.output_dir))
};