/**
 * 	Browser Sync Server Start Gulp Task
 *
 * 	Gulp task to start up the browser sync server
 *	to refresh page after certain files have been changed
 */

//include depedencies
var browserSync = require('browser-sync').create(),
	config		= require('./config.js');

//Start server and watch files for changes.
module.exports = function() {

  // Listen to change events on HTML and reload
  browserSync.watch("./**/*.html").on("change", browserSync.reload);

// Listen to change events on JS and reload
  browserSync.watch([config.js.bundle,config.js.welcome_bundle_path]).on("change", browserSync.reload);

// Provide a callback to capture ALL events to CSS
// files - then filter for 'change' and reload all
// css files on the page.
  browserSync.watch([config.sass.css_glob,config.sass.welcome_css_glob], function (event, file) {
    if (event === "change") {
      browserSync.reload("*.css");
      console.log(file);
    }
  });

  return browserSync.init({

  	proxy: "hirefreehands.demo",
  	logLevel: "info"
  })
  
};
