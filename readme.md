#Hirefreehands Client

Hirefreehands helps you find and hire qualified freelancers.

This repository contains the app frontend - hirefreehands.tech. Hirefreehands client is built with
```
Angular 1
Firebase
Jquery
Lodash
RestAngular
```

## Installation

###Prerequisites
* NPM
* Bower
* Git
* Nginx/Apache

### Config

* Clone repository and run ```npm install``` to install dependencies.
* Run ```bower install```
* Configure Apache/Nginx Virtual host at hirefreehands.tech to proxy the server.

![Hirefreehands Home](home.png)

## Codebase

Hirefreehands has 3 major branches - Master, Development, Staging. During development, create a branch for each feature, chore or bug you want to fix in this format - type/short-desc e.g

``` chore/readme-update```

