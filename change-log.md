Change Log for Hirefreehands
=============================

The following stipulates the features and changes that constitute the Parolz Merchant Mobile App platform
as a whole at different points in history.

## Format Layout

Changes are to be spelt out using the following format.


- Version number and/or name; Or range of version numbers and names eg v0.0.1[inception] - v0.0.8[reception]
- Date
- Feature
- Description
---------------------------------------------------------

### v0.0.0
**Date:** 23rd December, 2015

Authentication Complete
---------------------------------------------
Merchant authentication complete

### v0.1.0
**Date:** 2nd March, 2016

Integration with Paystack complete
---------------------------------------------
The necessary functionality(transaction initiation, payment verification and recurrent billing) have been fully implemented.


### v0.1.1
**Date:** 3rd March, 2016

Email/Password signup and LinkedIN Integration
------------------------------------------------
The the facility for retrieving a linkedin user's details via oauth2 as well as sign up & verification with email & password have been fully implented

### v0.1.2
**Date:**  3rd March, 2016

User roles and verification status evaluation
----------------------------------------------
After successful authentication, the response now includes the roles that have been assigned to the user
and the freelancer verification status stack.

### v0.1.3
**Date:**  19th May, 2016

Freelancer verification,Job milestones,Bidding all updated
----------------------------------------------------------

Freelancer Verification
=======================

*	Freelancer verification now roles and checklist now refresh on page refresh
*	Country code is now prepended to freelancer number
*	Changed upload icon to "paperclip"
*	Freelancer chat history now appends new verification messages as opposed to overwriting

Job Milestones
=======================
*	Confirmation messages are now shown before the milestone status update is persisted[client/freelancer]

Job Bidding
=======================
*	Implemented upload from all bidding windows

Finance
=======
*	Implemented collation of "total spent" to be shown in the view
