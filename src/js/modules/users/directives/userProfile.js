/**
*	User Profile Directive
*
*	Encompass the core elements for the user profile page
*	in a common place
*/
module.exports = function(){
	
	return {

		restrict: 'A',
		templateUrl: 'templates/users/profile-directive-tmp.html',
		link: function($scope,element,attrs){}
	};
};	