/**
*	Users Module
*/

//require the dependencies
var angular = require('angular');

var usersModule = angular.module('usersModule',[]);

//define controllers
usersModule.controller('UserCtrl',require('./controllers/UserCtrl.js'));
usersModule.controller('SupportCtrl',require('./controllers/SupportCtrl.js'));

//define factories
usersModule.factory('User',require('./services/User.js'));
usersModule.factory('Support',require('./services/Support.js'));

//define directives
usersModule.directive('userProfile',require('./directives/userProfile.js'));
