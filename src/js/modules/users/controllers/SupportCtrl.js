/**
*	Support Controller
*
*	Handles support, knowledge base and FAQ facilities
*/
module.exports = function($scope,$mdDialog,$mdToast,Support){

  'use strict';
  
  //make bindings to expose data and functionality
  $scope.createTicket = createTicket;
  $scope.viewTicket = viewTicket;
  $scope.isLoadingTickets = true;
  $scope.tickets = [];

  //retrieve all tickets for the logged in user
  Support.getAllUserTickets().then(function(response){
    $scope.tickets = response.data;
  },function(error){
    $mdToast.show(
        $mdToast.simple().content(error.data.message)
        .position('bottom right')
        .hideDelay(2000)
    );
  }).finally(function(){
    $scope.isLoadingTickets = false;
  });

  //function definitions go here

  /**
  * Create Ticket
  *
  * Brings up the dialog to do actually create the ticket
  *
  * @param object ev
  */
  function createTicket(ev){

    $mdDialog.show({

  			locals:{tickets:$scope.tickets},
      	controller: function($scope,Auth,Support,tickets){

          $scope.ticketTypes = ['General Inquiry', 'Payment', 'Job Dispute', 'Technical Issue'];

          $scope.ticket = {
            'priority': 2,
            'status':2,
            'type': 0
          };
          $scope.isCreatingTicket = false;

          $scope.closeDialog = closeDialog;
          $scope.doCreateTicket = doCreateTicket;

          /**
          * Do Create Ticket
          *
          * Makes call to Support platform(Freshdesk)
          * to create the ticket
          *
          * @param object ticket
          */
          function doCreateTicket(ticket){

            ticket = angular.copy(ticket);

            //evaluate actual type
            ticket.type = $scope.ticketTypes[ticket.type];

            $scope.isCreatingTicket = true;
            Support.createTicket(ticket).then(function(response){
              $mdToast.show(
        	      	$mdToast.simple().content('Your message was sent successfully.')
        	        .position('bottom right')
        	        .hideDelay(2000)
        	    );

              //append to Tickets
              ticket.created_at = (new Date()).getTime();
              tickets.unshift(ticket);

              $mdDialog.cancel();
            },function(error){

              $mdToast.show(
        	      	$mdToast.simple().content(error.data.message)
        	        .position('bottom right')
        	        .hideDelay(2000)
        	    );
            }).finally(function(){
              $scope.isCreatingTicket = false;
            });
          }

          function closeDialog(){
            $mdDialog.cancel();
          }
      	},

      	templateUrl: 'templates/users/create-ticket.html',
      	parent: angular.element(document.body),
      	targetEvent: ev,
      	clickOutsideToClose:true
	    });
  }

  /**
  * View Ticket
  *
  * Retrieves ticket conversations
  *
  * @param object Ticket
  * @param object event
  */
  function viewTicket(ticket,ev){

    $mdDialog.show({

  			locals:{ticket:ticket},
      	controller: function($scope,Auth,Support,ticket){

          $scope.reply = {body:''};
          $scope.ticket = ticket;
          $scope.isLoading = true;
          $scope.isSendingReply = false;
          $scope.conversations = [];

          $scope.sendReply = sendReply;
          $scope.getTicketStatus = getTicketStatus;
          $scope.ownsMesage = ownsMesage;
          $scope.closeDialog = closeDialog;

          //retrieve ticket conversations
          Support.getTicketConversations(ticket.id).then(function(response){
            $scope.conversations = response.data;
          },function(error){

            $mdToast.show(
                $mdToast.simple().content(error.data.message)
                .position('bottom right')
                .hideDelay(2000)
            );
          }).finally(function(){
            $scope.isLoading = false;
          });

          /**
          * Send Reply
          *
          * @param object reply
          */
          function sendReply(reply){

            if(reply.body === ''){return;}

            //dress the reply resource
            reply.body_text = reply.body;
            reply.user_id = ticket.requester_id;
            reply.ticket_id = ticket.id;
            reply.created_at = (new Date()).getTime();

            //append to the current thred of conversations
            $scope.conversations.push(reply);

            //get the position of this reply
            var index = $scope.conversations.length - 1;

            //remove the body_text and created_at fields
            reply = angular.copy(reply);delete reply.body_text;delete reply.created_at;

            //send to Freshdesk
            $scope.isSendingReply = true;$scope.reply.body = '';
            Support.sendReply(reply).then(function(){},function(error){

                //if there's an error, alert the user and retract the message
                $mdToast.show(
                    $mdToast.simple().content('We couldn\'t post your message at this time, please check your connection try again later')
                    .position('bottom right')
                    .hideDelay(2000)
                );

                $scope.conversations.splice(index,1);

            }).finally(function(){
                $scope.isSendingReply = false;
            });
          }

          /**
          * Get Ticket Status
          *
          * Resolves from the number value to human understandable value
          *
          * @param int Freshdesk status
          */
          function getTicketStatus(status){

            switch(status){
              case 2: status = "open";break;
              case 3: status = "pending";break;
              case 4: status = "resolved";break;
              case 5: status = "closed";break;
            }

            return status;
          }

          /**
          * Owns Message
          *
          * Evaluates if the given message was by the logged in user.
          * This assumes only hirefreehands customers NOT agents will ever initiate ticket creation
          *
          * @param object message
          */
          function ownsMesage(message){
            return message.user_id === ticket.requester_id;
          }

          function closeDialog(){
            $mdDialog.cancel();
          }
        },
      	templateUrl: 'templates/users/ticket-view.html',
      	parent: angular.element(document.body),
      	targetEvent: ev,
      	clickOutsideToClose:true
	    });
  }
};
