/**
*	User Controller Definition
*
*	Handles all activities that have to do with users[in case it wasnt already obvious]
*/

module.exports = function($scope,$mdDialog,$mdToast,$auth,$filter,$window,User,Job,Auth,Uploader){

	'use strict';

	// this view is for the freelancer to view/edit
	$scope.isOwner = true;

	//store retrieved information for the current user
	$scope.user = {qualificationChips:[]};

	//store password change
	$scope.password = {"current":"","new":"","confirm":""};

	//manage page load wait period
	$scope.page = {isLoading: true};

	//create filter based on user typed data
	$scope.querySkills = querySkills;

	//scope variable to determine whether or not to activate the profile for editing
	$scope.activateEditProfile = false;

	//give user option of adding more skils
	$scope.toggleAddMoreSkills = toggleAddMoreSkills;
	$scope.addMoreSkills = false;

	$scope.uploadProfilePhoto = uploadProfilePhoto;

	$scope.saveUserProfile = saveUserProfile;

	$scope.userLinkedinAccess = Auth.getUser().linkedin_access;

	//get an aggregate of all skills
	var allSkills = [];
	Job.getSkills()
	.then(function(skills){
		allSkills = skills.data;
	});

	//get info for this current user/freelancer
	User.getCurrent().then(function(user){

		$scope.user = user.data;

		console.dir($scope.user);
		
		$scope.user.moreSkills = [];
		$scope.user.qualificationChips = ($scope.user.qualifications === "") ? [] : $scope.user.qualifications.split(',');
		$scope.user.skills = $scope.user.skills.data;
		$scope.user.linkedin = JSON.parse(user.data.linkedin);

		//stop page loading placeholder
		$scope.page.isLoading = false;

		/**
		*	Go To Tab
		*
		*	Makes selected tab index active
		*
		*	@param int tab index
		*/
		$scope.goToTab = function(tabIndex){
			$scope.selectedIndex = tabIndex;
		};

	},function(error){

		$mdToast.show(
	      	$mdToast.simple().content('Error while loading your profile')
	        .position('bottom right')
	        .hideDelay(2000)
	    );
	});

	/**
	*	Edit Profile
	*
	*	Launch modal/form to edit user freelancer profile
	*/
	$scope.editProfile = function(ev){

		$scope.activateEditProfile = !$scope.activateEditProfile;
	};

	/**
	*	Update Settings
	*
	*	@param object user settings
	*/
	$scope.updateSettings = function(settings){

		$scope.isLoading = true;

		User.updateSettings(settings)
		.then(function(){
			$mdToast.show(
				$mdToast.simple().content('Your settings have been saved successfully')
	        	.position('bottom right')
	        	.hideDelay(2000)
	        );
		},function(error){
			$mdToast.show(
				$mdToast.simple().content(error.data.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		}).finally(function(){
			$scope.isLoading = false;
		});
	};

	/**
	*	Update Password
	*
	*	@param object user password(s)
	*/
	$scope.updatePassword = function(password) {

		$scope.isLoading = true;

		User.updatePassword(password)
		.then(function() {

			$mdToast.show(
				$mdToast.simple().content('Your password has been saved successfully')
				        .position('bottom right')
				        .hideDelay(2000)
		    );
		},function(error) {

			$mdToast.show(
				$mdToast.simple().content(error.data.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		}).finally(function(){
			$scope.isLoading = false;
		});
	};

	/**
	*	Update Finance
	*
	*	@param object user finance settings
	*/
	$scope.updateFinance = function(finance) {

		$scope.isLoading = true;

		//since account number is not shown by default, another scope variable is used, update actual variable here
		finance.acc_number = finance.new_acc_number;

		User.updateFinance(finance)
		.then(function(){
			$mdToast.show(
				$mdToast.simple().content('Your financial records have been saved successfully')
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		},function(error) {

			$mdToast.show(
				$mdToast.simple().content(error.data.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		}).finally(function() {
			$scope.isLoading = false;
		});
	};

	/**
	 *       
	 * 	Add linkedin profile
	 *
	 *  Add linkedIn profile link to Hirefreehands profile via
	 *  LinkedIn authentication.
	 */
	$scope.addLinkedin = function(){

		$scope.isLoadingLinkedin = true;

		$auth.authenticate('linkedin')
		.then(function(response){
			$scope.user.linkedin = JSON.parse(response.data.linkedin.linkedin_access);

		},
		function(){

			$mdToast.show(
				$mdToast.simple().content('Something went wrong while retrieving your LinkedIn profile. Please try again.')
					.position('bottom right')
					.hideDelay(2000));
		}).finally(function () {

      		$scope.isLoadingLinkedin = false;
		});
	};

	/**
  *	Upload Portfolio
  */
  $scope.uploadPortfolio = function(file) {

		if(file !== null) {

			var fileExt = file.name.split('.')[1];
			var fileType = file.type.split('/')[0];

			var date = new Date();
			var filename = $filter('hyphenate')(file.name)+'-'+date.getDay()+''+date.getMonth()+''+date.getYear()+']';

			$scope.showPortfolioUploadProgress = true;

		//save in the samples folder
    	Uploader.upload(file,'portfolio/'+Auth.getUser().uid + '/' + filename)
    	.then(function(response) {

    		//retrieve the file url
				var url = Uploader.getAssetUrl(response);

				var newAsset = {
					url: url,
					name: filename,
					type: fileType.toUpperCase(),
					extension: fileExt.toUpperCase(),
					created: (new Date()).getTime()/1000
				};

				//append to uploads
				if($scope.user.portfolio.length === 0) {
					$scope.user.portfolio = [newAsset];
				}else {
					$scope.user.portfolio.push(newAsset);
				}
	    },
			function(error) {

				$mdToast.show(
		      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
						        .position('bottom right')
						        .hideDelay(2000)
		    );

			},function(evt) {

				//reveal and update upload bar
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadProgress = progressPercentage+'%';
				$scope.showPortfolioUploadProgress = true;

			}).finally(function() {

				//hide progress bar
				$scope.showPortfolioUploadProgress = false;
			});
		}
	};

	/**
	*	Get All Banks
	*/
	$scope.getAllBanks = function(){

		return User.getAllBanks()
						   .then(function(response){
						   		$scope.banks = response.data;
						   });
	};

	/**
	*	Upload profile photo
	*
	*	@param file object
	*/
	function uploadProfilePhoto(file){

		if(file !== null){

			var fileExt = file.type.split('/')[1];
			var filename = $scope.user.firstname+'_'+$scope.user.lastname+'_'+$scope.user.uid+'.'+fileExt;

			$scope.showProfilePicUploadProgress = true;

			//save image on S3
	    Uploader.upload(file,'profile_photos/'+filename)
	    	.then(function(response){

	    		//retrieve the file url
					var url = Uploader.getAssetUrl(response);

					//show uploaded photo
					$scope.user.image = url;

					//persist new photo
					User.updateProfilePhoto({image_url:url,uid:Auth.getUser().uid}).then(function() {
						$scope.setProfilePhoto(url);
					},
					function() {
						$mdToast.show(
				      	$mdToast.simple().content(error.message)
				        .position('bottom right')
				        .hideDelay(2000)
				    );
					});

	    	},function(error){

						$mdToast.show(
				      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
				        .position('bottom right')
				        .hideDelay(2000)
				    );

			},function(evt){

				//reveal and update upload bar
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadProgress = progressPercentage+'%';
				$scope.showProfilePicUploadProgress = true;

			}).finally(function(){

				//hide progress bar
				$scope.showProfilePicUploadProgress = false;
			});
		}
	}

	/**
  *	Search for skills.
  */
  function querySkills(query) {

    	var results = query ? allSkills.filter(createFilterFor(query)) : allSkills;
    	return results;
  }

	/**
  *	Create filter function for a query string
  */
  function createFilterFor(query) {

    	var lowercaseQuery = angular.lowercase(query);

    	return function filterFn(skill) {
      	return skill.name.toLowerCase().indexOf(lowercaseQuery) !== -1;
    	};
  }

  /**
  *	Toggle Add More Skills
  */
  function toggleAddMoreSkills() {
  	$scope.addMoreSkills = !$scope.addMoreSkills;
  }

  /**
  *	Save User Profile
  */
  function saveUserProfile(user) {

  	$scope.isUpdatingProfile = true;

  	//concatenate chips for qualification
  	user.qualifications = user.qualificationChips.join(',');

  	User.save(user)
  	.then(function(response){

  		$mdToast.show(
	      	$mdToast.simple().content('Your profile was updated successfully')
	        .position('bottom right')
	        .hideDelay(2000)
	    );
  	},function(error){

  		var errorRhetoric = error.message || 'There was a problem while we were trying to save your changes';
  		$mdToast.show(
	      	$mdToast.simple().content(errorRhetoric)
	        .position('bottom right')
	        .hideDelay(2000)
	    );
  	}).finally(function(){
  		$scope.isUpdatingProfile = false;
  	});
  }
};
