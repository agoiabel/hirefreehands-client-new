/**
*	User Service Definition
*/
module.exports = function($localStorage,Restangular,Auth){

	'use strict';

	var userFactory = {},
		hermes = Restangular,
		store = $localStorage;

	/**
	*	Get
	*
	*	Get profile information about the specified user
	*
	*	@param string user uid
	*	@param bool whether or not to include more detail
	*/
	userFactory.get = function(uid,includeDetails){

		var detail = {uid:uid};

		if(includeDetails){
			detail.includeDetails = true;
		}
		return hermes.one('users/get').get(detail);
	};

	/**
	*	Save
	*
	*	Save profile information about the specified user
	*
	*	@param object user
	*/
	userFactory.save = function(user){

		return hermes.all('users/save').post(user);
	};

	/**
	*	Update Password
	*
	*	@param object user password(current,new,confirmation)
	*/
	userFactory.updatePassword = function(password){

		return hermes.all('users/update_password').post(password);
	};

	/**
	*	Update Settings
	*
	*	@param object settings
	*/
	userFactory.updateSettings = function(settings){

		return hermes.all('users/update_settings').post(settings);
	};

	/**
	*	Update Profile Photo
	*
	*	@param object data{image_url,uid}
	*/
	userFactory.updateProfilePhoto = function(data){

		return hermes.all('users/update_profile_photo').post(data);
	};

	/**
	*	Update Financial Records
	*
	*	@param object financial records
	*/
	userFactory.updateFinance = function(records){

		return hermes.all('users/update_financial_records').post(records);
	};

	/**
	*	Update Phone Number
	*
	*	@param object phone number
	*/
	userFactory.updatePhoneNumber = function(number){

		return hermes.all('users/update_phone_number').post(number);
	};

	/**
	*	Get Current User
	*/
	userFactory.getCurrent = function(){

		//get the id of the logged in user
		var user = Auth.getUser();

		return userFactory.get(user.uid,true);
	};

	/**
	*	Get All Banks
	*/
	userFactory.getAllBanks = function(){

		return hermes.one('users/get_banks').get();
	};

	return userFactory;
};
