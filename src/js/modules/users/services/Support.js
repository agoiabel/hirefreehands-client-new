/**
*	Support Service Definition
*
* Proxies the Freshdesk API integration
*/
module.exports = function(Restangular,Auth){

	'use strict';
	
	var supportFactory = {},
		hermes = Restangular;

	/**
	*	Create Ticket
	*
	*	@param object ticket
	*/
	supportFactory.createTicket = function(ticket){
		return hermes.all('support/create_ticket').post(ticket);
	};

	/**
	*	Get All User Tickets
	*/
	supportFactory.getAllUserTickets= function(){
		return hermes.one('support/get_all_tickets').get();
	};

	/**
	*	Get Ticket Conversations
	*
	*	@param int Ticket ID
	*/
	supportFactory.getTicketConversations= function(ticketId){
		return hermes.one('support/get_ticket_conversations').get({ticket_id:ticketId});
	};

	/**
	*	Send Reply
	*
	*	@param object Reply
	*/
	supportFactory.sendReply= function(reply){
		return hermes.all('support/reply').post(reply);
	};

  return supportFactory;
};
