/**
*	Users Module Routes
*/
module.exports = {

	profile:{
		url: "/profile",		
		views:{
			"app-sub-view":{
				controller: "UserCtrl",
				templateUrl: "templates/users/profile.html"
			}		
		}
	},
	settings:{
		url: "/settings",		
		views:{
			"app-sub-view":{
				controller: "UserCtrl",
				templateUrl: "templates/users/settings.html"
			}		
		}
	},
	support:{
		url: "/support",		
		views:{
			"app-sub-view":{
				controller: "SupportCtrl",
				templateUrl: "templates/users/support.html"
			}		
		}
	},
	analytics:{
		url: "/analytics",		
		views:{
			"app-sub-view":{
				controller: "UserCtrl",
				templateUrl: "templates/users/analytics.html"
			}		
		}
	}
};