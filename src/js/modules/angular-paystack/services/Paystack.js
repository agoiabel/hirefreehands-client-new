/**
*	The Paystack angular service
*/
module.exports = function($interval,Restangular){

	var hermes = Restangular,
		that = this;//container for all the functionality	

	/**
	*	Store merchants public key
	*/
	that.publicKey = '';

	/**
	*	Set Public Key
	*
	*	Set merchants public key
	*
	*	@param string public key
	*/
	that.setPublicKey = function(publicKey){
		that.publicKey = publicKey;
	};

	/**
	*	Init
	*	
	*	Initiate a new payment transaction with Paystack
	*
	*	@param object configuration
	*/
	that.init = function(config){

		//setup paystack
		config.key = that.publicKey;
		
		var paystackInstance = PaystackPop.setup(config);
		
		//load the payment iframe
		paystackInstance.openIframe();

		//check to see if we need to run some code once we are sure iframe is done loading
		if(config.onIframeLoaded !== undefined){
			onIframeLoaded(paystackInstance,config.onIframeLoaded);
		}
	};

	/**
	*	Verify
	*	
	*	Verify Paystack transaction serverside
	*
	*	@param string server endpoint
	*	@param object Paystack transx data to send to the server 
	*/
	that.verify = function(url,data){
		return hermes.all(url).post(data);
	};


	/**
	*	Get Transx ID
	*	
	*	Obtain a unique transaction ID from the server
	*
	*	@param string server endpoint
	*	@param object params to send to the server
	*/
	that.generateTransxId = function(url,data){
		return hermes.all(url).post(data);
	};

	/**
	*	On iFrame Loaded
	*
	*	Checks/Watches to see if iFrame has been loaded and runs
	*	function passed in.
	*
	*	@param fxn function to be run only after iFrame has loaded
	*/
	function onIframeLoaded(paystackInstance,callback){

		/**
		*	Keep checking every second to know when the Iframe is done loading.
		*	Take note of the time when we started and ended this timer
		*/
		var initialMs,currentMs,timeElapsed;
		initialMs = currentMs = (new Date()).getTime();

		var hasIframeLoadedTimer = $interval(function(){

			//get current time
			currentMs= (new Date()).getTime();

			//get time elapsed
			timeElapsed = currentMs - initialMs;

			if(paystackInstance.iframeLoaded){

				/**
				*	After we have initialized the transactions on the server
				*	and the iframe is done loading, cancel the timer and run the function.
				*/
				$interval.cancel(hasIframeLoadedTimer);	
				
				//run the passed in callback
				callback();
				
			}else if(!paystackInstance.iframeLoaded && timeElapsed > 10000){

				//when timer has gone more than 10 seconds without the iframe Loading, call reject
				$interval.cancel(hasIframeLoadedTimer);
				console.log('Request is taking too long');
			}	
			
		},1000);
	}

	return that;
};