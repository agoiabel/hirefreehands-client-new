/**
*	The AngularJS Paystack Module
*
*	This provides an "Angular" way to access
*	the already excellent Paystack javascript
*	library.
*/

//require the asynchrous loader
require('angular-load/angular-load.js');

//require the angular lib dependency
var angular = require('angular');

//define the Paystack module
var angularPaystack = angular.module('angularPaystack',['angularLoad']);

//define the Paystack service
angularPaystack.factory('Paystack',require('./services/Paystack.js'));

//Asynchronouly load the paystack module
angularPaystack.run(function($rootScope,$mdToast,angularLoad,Paystack){

	angularLoad.loadScript('https://js.paystack.co/v1/inline.js').then(function() {
    	
    	//let everyone know Paystack is done loading    	
    	$rootScope.$broadcast('paystackLoaded');

	}).catch(function() {
	    
	    $mdToast.show(
				      	$mdToast.simple().content('An error occured while loading Paystack library')		        
				        .position('bottom right')
				        .hideDelay(2000)
				    );
	    //alert("Error loading Paystack library");
	});
});