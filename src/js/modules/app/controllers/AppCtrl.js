/**
*	App Controller Definition
*	
*	Contains common functionality to share
*	between modules that extnd or are sub modules.
*/	

module.exports = function($scope, Auth, $timeout){

	//global variable for the currently selected top level navigation option
	$scope.activeNav = 'jobs';

	/**
	*	Set Active Nav
	*	
	*	@param string nav title
	*/
	$scope.setActiveNav = function(nav){

		$scope.activeNav = nav;
	};

	/**
	*	Is Active Nav
	*	
	*	@param string nav title
	*/
	$scope.isActiveNav = function(nav){

		return ($scope.activeNav === nav);
	};

	$scope.isAgent = function () {

		return Auth.isVerifiedRole('HFH_AGENT') ? 'Verify Freelancers' : 'Become a Freehand';
	};

	$scope.onStartJob = function (job) {
		$scope.$broadcast('jobStart', job);
	};


};