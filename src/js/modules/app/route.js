/**
 * Route definitions for the AppModule
 */
module.exports = {
	
	app: {		
		abstract: true,
		controller: "AppCtrl",
		templateUrl: "templates/app/index.html"
	}
};