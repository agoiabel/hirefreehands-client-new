/**
 *	App Module Definition
 *	
 *	This module acts as the consolidator for
 *	all the sub modules which are the modules
 *	that require authentication to access
 *	and contains common functionality that
 *	exists between those modules.
 */

//require the dependencies
var angular = require('angular');

//define the Dashboard module
var appModule = angular.module('appModule',[]);

//define controllers
appModule.controller('AppCtrl',require('./controllers/AppCtrl.js'));