/**
 * Route definitions for the AuthModule
 */
module.exports = {

	login: {
		url: "/login",
		controller: "AuthCtrl",
		params: {successMessage: ''},
		templateUrl: "templates/auth/login.html"
	},
	signup: {
		url: "/signup",
		controller: "AuthCtrl",
		templateUrl: "templates/auth/signup.html"
	},
	verify: {
		url: "/verify/:verificationString",
		controller: "AuthCtrl",
		templateUrl: "templates/auth/login.html"
	},
	forgot_password: {

		url: "/forgot-password",
		controller: function($scope,Auth){

			$scope.data = {email: ''};
			$scope.error = {message: ''};
			$scope.isLoading = $scope.success = false;
			$scope.doForgotPassword = doForgotPassword;

			/**
			*	Do Forgot Password
			*
			*	Begin forgot password sequence
			*
			*	@param object email details
			*/
			function doForgotPassword(data){

				$scope.isLoading = true;
				$scope.error.message = ''; //reset error field

				Auth.doForgotPassword(data).then(function(response){
					$scope.success = true;
				},function(err){
					$scope.error.message = err.data.message;
				}).finally(function(){
					$scope.isLoading = false;
				});
			}
		},
		templateUrl: "templates/auth/forgot-password.html"
	},

	change_password: {

		url: "/change-password/:verificationString",
		controller: function($scope,$state,$stateParams,Auth){

			$scope.data = {password: '',conf_password: ''};
			$scope.error = {message: ''};
			$scope.isLoading = $scope.success = false;
			$scope.doChangePassword = doChangePassword;

			//if verification string was not sent, make sure the user knows link is valid
			if(!Boolean($stateParams.verificationString)){
				$scope.error.message = 'Your change password link has been tampererd with, please try again.';
			}else{
				$scope.data.token = $stateParams.verificationString;
			}

			/**
			*	Do Change Password
			*
			*	Begin change password sequence
			*
			*	@param object password and conf_password
			*/
			function doChangePassword(data){

				$scope.isLoading = true;
				$scope.error.message = ''; //reset error field

				Auth.doChangePassword(data).then(function(response){

					//move user to login package
					$state.go('login',{successMessage:'Your password has been changed successfully, go on and login'});
				},function(err){
					$scope.error.message = err.data.message;
				}).finally(function(){
					$scope.isLoading = false;
				});
			}
		},
		templateUrl: "templates/auth/change-password.html"
	}
};
