/**
 * Auth Controller
 *
 * This controller handles all authentication
 * logic and bindings
 */

module.exports = function($scope,$state,$stateParams,$mdToast,$auth,Auth){

	$scope.params = {};
	$scope.isSignupSuccessful = false;

	//check if we are seeking to verify a user's account
	if($stateParams.verificationString !== undefined){

		$scope.isLoading = true;

		Auth.verifyUser($stateParams.verificationString).then(function(){

			$mdToast.show(
		      	$mdToast.simple().content('Your account has been verified successfully! Login to view your jobs.')
		        .position('top right')
		        .hideDelay(2000)
		    );

		},function(error) {
			$mdToast.show(
		      	$mdToast.simple().content(error.data.message)
		        .position('top right')
		        .hideDelay(2000)
		    );
		}).finally(function() {
			$scope.isLoading = false;
		});
	}

	//if we have a success message, let's see them
	if($stateParams.successMessage !== '') {
		$scope.successMessage = $stateParams.successMessage;
	}else {
		//if we have an error message, let's see them
		$scope.errorMessage = $stateParams.errorMessage;
	}

	/**
	*	doLogin
	*
	*	@params object user credentials
	*/
	$scope.doLogin = function(params){

		$scope.isLoading = true;

		//actually authenticate the user
		Auth.login(params).then(function(response){

			console.dir(response);


			// persist the user			
			Auth.saveUser(response.data.user);
			Auth.setAuthorization(response.data.user.token);

			// transparently save the various roles this user has been asssigned
			var role_users = response.data.user.role_users;
			var userRoleArray = [];
			for (var j in role_users) {
				console.dir(role_users[j].role.name);
				userRoleArray.push(role_users[j].role.name);
			}
			Auth.setUserRoles(userRoleArray);

			// not yet set
			//Auth.setFreelancerVerificationStatus(response.data.user.verification);

			// wait for the above to be set
			$timeout(function(){
				window.location.href = window.location.href+'jobs';
			},300);

		},function(error){
			$mdToast.show(
		      	$mdToast.simple().content(error.data.message)
		        .position('top right')
		        .hideDelay(2000)
		    );
		}).finally(function(){
			$scope.isLoading = false;
		});
	};

	/**
	*	Signup
	*
	*	@param object new user credentials
	*/
	$scope.doSignup = function(params){

		$scope.isLoading = true;

		//actually authenticate the user
		Auth.signup(params).then(function(response){

			//persist user && prompt user to verify their email
			Auth.saveUser(response.user);
			Auth.setAuthorization(response.token);

			//transparently save the various roles this user has been asssigned
			Auth.setUserRoles(response.roles);

			//save freelancer verification status
			Auth.setFreelancerVerificationStatus(response.verification);

			$scope.isSignupSuccessful = true;

		},function(error){

			$mdToast.show(
		      	$mdToast.simple().content(error.data.message)
		        .position('top right')
		        .hideDelay(2000)
		    );
		}).finally(function(){
			$scope.isLoading = false;
		});
	};

	/**
	*	SocialSignin
	*
	*	@param string provider
	*/
	$scope.socialSignin = function(provider){

		switch(provider){
			case "linkedin": linkedin();
		}
	};

	/**
	*	Initiate signup sequence with Linkedin
	*/
	function linkedin(provider){

		$scope.isLoading = true;
		$auth.authenticate('linkedin').then(function(response){

			$scope.isLoading = false;

			$auth.logout();

			//persist the user and the token
			Auth.saveUser(response.data.user);
			Auth.setAuthorization(response.data.token);

			//transparently save the various roles this user has been asssigned
			Auth.setUserRoles(response.data.roles);

			//save freelancer verification status
			Auth.setFreelancerVerificationStatus(response.data.verification);

			$state.go('app.jobs');
		},function(){
			$mdToast.show(
		      	$mdToast.simple().content('An error occured while signing in')
		        .position('top right')
		        .hideDelay(2000)
		    );
		});
	}
};
