/**
*	Auth Module Definition
*/
require('satellizer/dist/satellizer.js');
var config = require('../config');

//require the dependencies
var angular = require('angular');

//define the Auth module
var authModule = angular.module('authModule',['satellizer']);

authModule.config(function($authProvider){

	$authProvider.authToken = '';
  $authProvider.tokenName = '';
  $authProvider.tokenPrefix = '';
  $authProvider.tokenHeader = '';
  $authProvider.tokenType = '';
  $authProvider.storageType = '';

	$authProvider.linkedin({

      url: config.baseAPI()+'auth/linkedin',
      clientId: '77mp5uuh5kmwaj',
      redirectUri: window.location.origin + '/profile'
  });
});

//define controllers
authModule.controller('AuthCtrl',require('./controllers/AuthCtrl.js'));

//define services
authModule.factory('Auth',require('./services/auth.js'));
