/**
*	Auth Login Service
*
*	Handle All Authentication
*	Activities With The Server
*
*	@param object $resource object for ajax request
*/
module.exports = function($localStorage,Restangular){

	'use strict';

	var token,
		authService = {
			roles: [], //store transparently the different values this user has been assigned
			verificationStatusStack: [] //store transparently the verification status for the diff verification criteria
		},
		store = $localStorage,
		hermes = Restangular,

		//create whitelist of states to allow without authentication
  	whitelist = [
  		'login',
      'signup',
      'forgot_password',
      'change_password'
		];

	/**
	*	Get Whitelist
	*
	*	Returns list of states users can access without authentication
	*/
	authService.getWhitelist = function() {

		return whitelist;
	};

	/**
	*	Login
	*
	*	Authenticate User
	*
	*	@param object authentication credentials
	*/
	authService.login = function(params) {

		return hermes.all('login/store').post(params);
	};

	/**
	*	Signup
	*
	*	Register new User
	*
	*	@param object new user details
	*/
	authService.signup = function(params){

		return hermes.all('auth/signup').post(params);
	};

	/**
	*	Verify User
	*
	*	Verify user's account via the email link they
	*	clicked to initiate a verification.
	*
	*	@param string encrypted user data
	*/
	authService.verifyUser = function(verificationString){
		return hermes.one('verify/email/'+verificationString).get();
	};

	/**
	*	Do Forgot Password
	*
	*	Initiate sequence of events that eventually
	*	lead to sending change password requests to relevant users
	*
	*	@param object user email
	*/
	authService.doForgotPassword = function(data){
		return hermes.all('auth/forgot_password').post(data);
	};

	/**
	*	Do Change Password
	*
	*	Initiate sequence of events that eventually
	*	lead to relevant users changing their password
	*
	*	@param object verification string,new and conf password
	*/
	authService.doChangePassword = function(data){
		return hermes.all('auth/change_password').post(data);
	};

	/**
	* Set User
	*
	*	Saves info about the currently logged in user
	*
	* @param object user
	*/
	authService.saveUser = function(user){
		store.user = user;
	};

	/**
	* Get User
	*
	* Retrieves info about the currently logged in user
	*/
	authService.getUser = function(){
  		return store.user;
 	};

 	/**
  * Remove User
  *
  * Deletes info about the currently logged in user
  */
	authService.removeUser = function(){
   	delete store.user;
  };

    /**
	*	Set authorization token
	* 	@param string token
	*/
	authService.setAuthorization = function(token){

		store.Authorization = token;
	};

	/**
	*	Get authorization token
	*/
	authService.getAuthorization = function(){

		return store.Authorization;
	};

	/**
	*	Getter for logged in status
	*	User is logged in iff
	*	Authorization key exists in local storage
	*/
	authService.isLoggedIn = function (){

		return store.Authorization !== undefined && store.Authorization !== '';
	};

	/**
	*	Remove authorization token
	*/
	authService.removeAuthorization = function(){

		delete store.Authorization;
	};

	/**
	*	Is Verified Role
	*
	*	Determine if current logged in user has been assigned
	*	the specified role.
	*
	*	@param string role to confirm
	*/
	authService.isVerifiedRole = function(role){
		
		return _.indexOf(authService.getUserRoles(), role) !== -1 ? true: false;
	};

	/**
	*	Set User Roles
	*
	*	@param array roles
	*/
	authService.setUserRoles = function(roles){
		authService.roles = roles;
	};

	/**
	*	Get User Roles
	*/
	authService.getUserRoles = function(){

		return authService.roles;
	};

	/**
	*	Refresh User Roles
	*/
	authService.refreshUserRoles = function(){
		return hermes.one('users/get_roles').get();
	};

	/**
	*	Set Freelancer Verification Status
	*
	*	@param array freelancer verification status stack
	*/
	authService.setFreelancerVerificationStatus = function(statusStack){
		authService.verificationStatusStack = statusStack;
	};

	/**
	*	Get Verification Checklist
	*/
	authService.getVerificationChecklist = function(){
		return authService.verificationStatusStack;
	};

	/**
	*	Refrsh Freelancer Verification Status
	*/
	authService.refreshFreelancerVerificationChecklist = function(){
		return hermes.one('freelancers/get_verification_status').get();
	};

	/**
	*	Authorize Route
	*
	*	Check state to be transitioned to against
	*	white list to make sure the user
	*	cannot access a privileged page
	*	without authentication
	*
	*	@param string state to transition to
	*/
	authService.isAuthorized = function(toState){

		//Authenticte request against current user
		return !authService.isLoggedIn() && _.indexOf(whitelist, toState) === -1;
	};

	/**
	*	Logout functionality
	*	Remove authorization detail and
	*	Locally stored user information
	*/
	authService.logout = function(){

		authService.removeAuthorization();
		authService.removeUser();
	};

  	return authService;
};
