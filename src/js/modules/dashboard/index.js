/**
 * Dashboard Module Definition
 */

//require the dependencies
var angular = require('angular');

//define the Dashboard module
var dashboardModule = angular.module('dashboardModule',[]);

//define controllers
dashboardModule.controller('DashboardCtrl',require('./controllers/DashboardCtrl.js'));