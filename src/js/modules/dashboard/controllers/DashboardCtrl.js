/**
 * Dashboard Controller
 * 
 * This controller handles the dashboard
 * reporting for all roles
 */

module.exports = function($scope,$mdToast,Job){

  //initialise scope variable for jobs
  $scope.jobs = [];

  //initialise scope variables for loading assets
  $scope.isLoadingUpdates = $scope.isLoadingJobs = true;

  //initialise scope variable for user updates
  $scope.updates = [];

  //retrieve jobs
  Job.getAll().then(function(jobs){    
    
    $scope.jobs = jobs.data.all;
        
  },function(){

    $mdToast.show(
          $mdToast.simple().content('Error while retrieving jobs')            
          .position('bottom right')
          .hideDelay(2000)
      );
  }).finally(function(){
    $scope.isLoadingJobs = false;
  });

  //retrieve user job updates/logs
  Job.getUpdates().then(function(response){    
    
    $scope.updates = response.data.updates;
    $scope.rating = response.data.rating;
        
  },function(){

    $mdToast.show(
          $mdToast.simple().content('Error while retrieving job updates')            
          .position('bottom right')
          .hideDelay(2000)
      );
  }).finally(function(){
    $scope.isLoadingUpdates = false;
  });
};