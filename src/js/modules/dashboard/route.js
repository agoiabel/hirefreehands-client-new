/**
 * Route definitions for the AuthModule
 */
module.exports = {
	
	dashboard: {
		
		url: "/dashboard",
		views:{
			"app-sub-view":{
				controller: "DashboardCtrl",
				templateUrl: "templates/dashboard/index.html"
			}			
		}		
	}
};