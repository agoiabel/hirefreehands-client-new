/**
*	Utilities Module
*
*	Module contains all app wide
*	related features ie filters, directives etc
*/

var angular = require('angular');
var utilitiesModule = angular.module('utilitiesModule',[]);

//declare filters
utilitiesModule.filter('hyphenate',require('./filters/hyphenate.js'));
utilitiesModule.filter('dehyphenate',require('./filters/dehyphenate.js'));
utilitiesModule.filter('nbsp2br',require('./filters/nbsp2br.js'));
utilitiesModule.filter('makeDateFromString',require('./filters/makeDateFromString.js'));
utilitiesModule.filter('cleanContact',require('./filters/cleanContact.js'));
utilitiesModule.filter('currency',require('./filters/currency.js'));

//declare services
utilitiesModule.factory('Notification',require('./services/Notification.js'));
utilitiesModule.factory('Uploader',require('./services/Uploader.js'));
utilitiesModule.factory('Confirm',require('./services/Confirm.js'));
utilitiesModule.factory('Search',require('./services/Search.js'));
utilitiesModule.factory('Mailer',require('./services/Mailer.js'));

//define directives
utilitiesModule.directive('starRating',require('./directives/starRating.js'));
utilitiesModule.directive('tab',require('./directives/tab.js'));
utilitiesModule.directive('toggleDropdown',require('./directives/toggleDropdown.js'));
utilitiesModule.directive('hideAllDropdowns',require('./directives/hideAllDropdowns.js'));
utilitiesModule.directive('toggleSidebar',require('./directives/toggleSidebar.js'));
utilitiesModule.directive('popover',require('./directives/popover.js'));
utilitiesModule.directive('editContent',require('./directives/editContent.js'));
utilitiesModule.directive('accordion',require('./directives/accordion.js'));
utilitiesModule.directive('toggleSidePanel',require('./directives/toggleSidePanel.js'));
utilitiesModule.directive('scrollToBottom', require('./directives/scrollToBottom.js'));
utilitiesModule.directive('toggleTrayMenu', require('./directives/toggleTrayMenu.js'));
utilitiesModule.directive('newTabLink', require('./directives/newTabLink.js'));
