module.exports = function ($compile) {

  'use strict';

  return {
    restrict: 'A',
    link: function (scope, element) {
      $(element).click(function (e) {

        e.preventDefault();

        var $regularMenuContainer = $('#regular-menu-content');
        var $trayMenuItemsContainer = $('#tray-menu-content').find('> .menu-utilities');
        var $trayMenuItems = $trayMenuItemsContainer.children();

        // Grab tray menu items from existing menu items, and make them angular-aware.
        var loadMenuItems = function () {

          var $notificationsMarkup = $compile($regularMenuContainer.find('.notifications').clone(true))(scope);
          var $loggedInMarkup = $compile($regularMenuContainer.find('.logged-in-profile-pic').clone(true))(scope);

          $trayMenuItemsContainer.append(
            $notificationsMarkup,
            $loggedInMarkup
          );
        };

        var toggleTrayMenu = function () {
          $('#tray-menu-content').toggleClass('active');
        };

        // No menu items? load it!
        if ($trayMenuItems.length === 0) {
          loadMenuItems();
        }

        toggleTrayMenu();
      });
    }
  };
};