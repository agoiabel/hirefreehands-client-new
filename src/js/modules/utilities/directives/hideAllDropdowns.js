/**
*	The hide all dropdowns directive directive
*/

module.exports = function(){

	return {

		restrict: 'A',
		link: function($scope,element, attrs){

			$(element).on('click',function(e){

				$('.toggled-dropdown').slideUp('fast');
			});
		}
	};
};