/**
*	Star Rating Directive
*
*	Includes readonly modes
*/
module.exports = function(){

	/**
	*	Reset Rating
	*
	*	@param int current rating
	*	@param object directive container
	*/
	function resetRating(rating,element){

		//remove active class first of all
		$(element).children('.star-rating').children().removeClass('active');

		//set the stars to the amount selected by the user
		if(rating !== undefined && rating !== 0){

			var index = rating - 1;
			var stars = $(element).children('.star-rating');

			stars.children(':lt('+index+')').addClass('active');
			stars.children(':eq('+index+')').addClass('active');
		}
	}

	return {

		restrict: 'A',
		required: ['ngModel','readonly'],
		templateUrl: 'templates/utilities/star-rating.html',
		scope:{
			"rating": "=ngModel",
			"readonly": "@"
		},
		link: function($scope,element,attrs){

			//make sure the rating is not in readonly mode
			if($scope.readonly === 'false'){

				$(element).children('.star-rating').children('.fa-star')
				.on('mouseover',function(){

					//get index of the star currently hovered on
					var index = $(this).index();

					//make preceeding stars active
					$(this).siblings(':lt('+index+')').addClass('active');
					$(this).addClass('active');
				})
				.on('mouseout',function(){

					//reset rating
					resetRating($scope.rating,element);
				})
				.on('click',function(){
					
					//get index of the star currently hovered on
					var index = $(this).index();

					$scope.rating = Number(index) + 1;

					resetRating($scope.rating,element);
				});
			}else{
				
				resetRating($scope.rating,element);
			}			
		}
	};
};	