/**
*	Toggle Dropdown Directive
*
*	Toggle the specified dropdown[specified with the css selector].	
*/
module.exports = function($document){

	return {

		restrict: 'A',
		link: function($scope,element,attrs){

			element.bind('click',function(evt){				
				$(attrs.toggleDropdown).slideToggle('fast');	

				//make the caret toggle right or down if there's one
				var isNotActivated = $(element).find('.fa-caret-right');
				var isActivated = $(element).find('.fa-caret-down');
				
				if(isActivated.get(0) !== undefined){
					$(element).find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
				}else if(isNotActivated.get(0) !== undefined){
					$(element).find('.fa-caret-right').removeClass('fa-caret-right').addClass('fa-caret-down');
				}			
			});
		}
	};
};