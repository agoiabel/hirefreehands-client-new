/**
*	The tab directive
*
*	Creates infrastructure for tabbing to already existing
*	HTML structure.
*/

module.exports = function(){

	return {

		restrict: 'A',
		link: function($scope,element, attrs){

			$(element).on('click',function(e){

				//make active
				$(this).addClass('active').siblings().removeClass('active');

				//obtain target tab
				var target = attrs.target;				
				$('#'+target).show().siblings().hide();

				e.preventDefault();
			});
		}
	};
};