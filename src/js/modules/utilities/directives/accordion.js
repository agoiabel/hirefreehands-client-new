/**
*	The accordion directive
*
*	Creates infrastructure for generic accordion functionality
*	to existing HTML structure.
*
*	@param string oneAtATime could be one[mutually exclusive]/multiple
 *
 * TODO: Refactor.
*/

module.exports = function(){

	return {

		restrict: 'C',
		scope: {
			"oneAtATime": "@"
		},
		link: function($scope,element){

			var accordionItems = $(element).children();

			// Open up any accordion item with an active class by default.
			$.each(accordionItems, function () {
				if ($(this).hasClass('active')) {
					$(this).find('.accordion-body').slideDown(250);
          $(this).find('[icon]').addClass('rotate-180');
				}
      });

      accordionItems.on('click',function(e){

				// make this item active/not active
				$(this).toggleClass('active');

				// animate icon
				if($(this).hasClass('active')){
					$(this).find('[icon]').addClass('rotate-180');
				}else{
					$(this).find('[icon]').removeClass('rotate-180');
				}				

				// decide how to proceed based on the mode set
				if($scope.oneAtATime){

					//reveal the clicked item's content
					$(this).find('.accordion-body').slideToggle(250);

					// hide the content of its siblings
					$(this).siblings().find('.accordion-body').hide();
				}else{

					// allow multiple open at the same time
					$(this).find('.accordion-body').slideToggle(250);
				}

				e.preventDefault();				
			});
		}
	};
};