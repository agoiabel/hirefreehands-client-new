module.exports = function () {
  return {
    restrict: 'AC',
    link: function (scope, element) {
      $(element).unslider({
        autoplay: true,
        delay: 3000,
        arrows: false
      });
    }
  };
};
