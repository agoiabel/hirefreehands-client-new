/**
 * Created by Lawrence on 14/12/2016.
 */

module.exports = function ($timeout) {

  return {

    restrict: "A",
    link: function (scope, element) {

      function scrollToBottom(container_element) {
        container_element.scrollTop(container_element[0].scrollHeight);
      }

      // Scroll to bottom when a new child is added to the collection.
      scope.$watchCollection($(element).data("collection"), function (newValue) {
        if (newValue) { scrollToBottom($(element)); }
      });

      // Scroll to bottom when element renders.
      $(element).ready(function () {
        $timeout(function () {
          scrollToBottom($(element));
        }, 500);
      });
    }
  }
};
