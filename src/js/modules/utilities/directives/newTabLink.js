/**
 * newTabLink Directive
 *
 * click-to-open link in a new tab.
 *
 * This can be used by any element
 * to mimic the 'open-in-new-tab'
 * behaviour of anchor tags.
 *
 * Dependencies
 * ============
 * @param $window
 *
 * @returns newTabLink directive object
 *
 */
module.exports = function($window) {

  'use strict';

  return {
    restrict: 'A',
    scope: {
      url: '@'
    },
    link: function (scope, element) {

      var linkElem = $(element);

      if (linkElem.css('cursor') !== 'pointer') {
        $(element).css('cursor', 'pointer');
      }

      linkElem.click(function (e) {
        e.preventDefault();

        var url = scope.url;

        if (url) {
          $window.open(url, '_blank');
        } else {
          console.warn('new-tab-link: Missing `url` parameter.');
        }
      });
    }
  };
};