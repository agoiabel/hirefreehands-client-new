/**
*	Toggle Side Panel
*/
module.exports = function(){

	return {
		'restrict': 'A',
		link: function($scope,element){
			$(element).bind('click',function(){
				$('.side-panel').fadeToggle('fast');
			});
		}
	};
};