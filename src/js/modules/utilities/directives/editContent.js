/**
*	Edit content
*
*	Creates LinkedIn like functionality
*	for editing content
*/
module.exports = function(){

	/**
	*	Init
	*
	*	Initialise component
	*
	*	@param object $scope
	*	@param object element
	*/
	function init($scope,element){

		//the directive needs to have been activated before the button to edit is revealed
		if($scope.isActive){
			$(element).find('.trigger').show();
		}else{
			$(element).find('.trigger').hide();
		}
	}

	return {

		restrict: 'A',
		required: ['is-active'],
		scope:{
			"isActive": "=active"
		},
		link: function($scope,element,attrs){

			$(element).find('.trigger,[close-edit-content]')
			.on('click',function(){

				//toggle between read and edit modes
				$(element).find('[edit-mode]').toggle();
				$(element).find('[read-mode]').toggle();

				//focus the input elements(just the first for now), to solve the placeholder distortion with chips
				if($(element).find('[edit-mode]').is(':visible')){
					$(element).find('[edit-mode]').find('input:first').focus();
				}
			});

			$scope.$watch('isActive',function(){

				//(re-)inititialise
				init($scope,element);
			});
		}
	};
};
