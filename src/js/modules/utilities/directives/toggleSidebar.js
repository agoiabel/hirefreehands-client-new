/**
*	Toggle Utility Project Sidebar
*
*	Toggle the project switching window
*/
module.exports = function(){

	return {

		restrict: 'AC',
		link: function($scope,element){

			element.bind('click',function(evt){					
								
				$('.sidebar-overlay').fadeToggle('fast').toggleClass('active');				
			});
		}
	};
};