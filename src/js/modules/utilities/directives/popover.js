/**
 *  The popover directive
 *
 *  Toggles a popover card with information specified
 *  on the card.
 */
module.exports = function () {

  return {

    restrict: 'A',
    link: function ($scope, element) {

      $(element).hover(function (e) {

        if ($('body').children('.popover').get(0) === undefined) {

          // create the popover
          var popover = document.createElement('div');
          popover.classList = 'popover';

          // evaluate position of popover, offset by 10px
          var top = e.pageY + $(this).height();
          var left = e.pageX - ($(this).width() / 2);

          popover.style = 'position:absolute;top:' + top + 'px;left:' + left + 'px';

          // input content into the popover
          popover.innerHTML = $(element).find('popover-content').html();

          //attach to the new element to the 'body'
          $('body').append(popover);

          e.preventDefault();
        } else {
          $('.popover').remove();
        }
      });
    }
  };
};