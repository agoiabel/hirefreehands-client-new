/**
*	Confirm Dialog Service
*
*	Used to present users with questions
*	and multiple options
*/
module.exports = function($mdDialog) {

	'use strict';

	var factory = {};

	/**
	*	Show
	*
	*	Initializes and shows a confirm dialog
	*
	*	@param object config object
	*/
	factory.show = function(config){

		$mdDialog.show({

			locals: {config:config},
			controller: function($scope,config){

				$scope.heading = config.heading;
				$scope.message = config.message;
				$scope.options = config.options;
				$scope.doAction = doAction;

				//make all option actions available via action<#option number>
				config.options.forEach(function(item,index){

					$scope[index] = item.action;
				});

				/**
				*	Do Action
				*
				*	Function to actually call the passed in function
				*	that exists on the $scope. Basically a work around
				*	for not being to interpolate in the ng-click directive
				*
				*	@param string function name [that exists on the $scope]
				*/
				function doAction(functionName){
					$scope[functionName]();
				}
			},
			templateUrl: 'templates/utilities/confirm-dialog.html',
	      	parent: angular.element(document.body),
	      	clickOutsideToClose:true
		});
	};

	return factory;
};
