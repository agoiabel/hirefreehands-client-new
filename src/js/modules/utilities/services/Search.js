/**
*	Search Service
*
*	Used to query
*/
module.exports = function (Restangular) {

	'use strict';

	var factory = {cache:{}},
      hermes = Restangular;

	/**
	*	Go
	*
	*	Queries the API for both jobs and freelancer resources
	*
	*	@param string query
  *	@param object resources to embed eg freelancers
	*/
	factory.go = function(query,embeds){

    var getVars;
    if(embeds !== undefined){
      getVars = {q:query,embed: embeds};
    }else{
      getVars = {q:query};
    }
    return hermes.one('search').get(getVars);
  };

	/**
	*	Get Cache
	*/
	factory.getCache = function(){
		return factory.cache;
	};

	/**
	*	Get Cache Item
	*
	*	@param string itemKey
	*/
	factory.getCacheItem = function(itemKey){
		return factory.cache[itemKey];
	};

	/**
	*	Update Cache
	*
	*	@param string key
	*	@param object data to update cache with
	*/
	factory.updateCache = function(key,data){

		factory.cache[key] = data;
	};

  return factory;
};
