/**
*	The Notification Service
*
*	Makes available an API for creating Taskme wide notifications
*/
module.exports = function ($localStorage,$firebaseArray,$location,firebaseURL) {

	'use strict';

	var that = this, //maintain this scope for "this"
		store = $localStorage,

		notificationRef = new Firebase(firebaseURL+'/notifications');

	//stores the notifications
	that.store = [];

	/**
	*	Get User Notifications Ref
	*
	*	@param string user id
	*/
	that.getUserNotificationsRef = function(userId){

		return notificationRef.child(userId);
	};

	/**
	*	Get User Notifications Array
	*
	*	@param string user id
	*/
	that.getUserNotificationsArray = function(userId){

		return $firebaseArray(that.getUserNotificationsRef(userId));
	};

	/**
	*	Send
	*
	*	Send notification to the specified user
	*	Notificaion send object expects the following:
	*
	*		notification text
	*		from id
	*		from full name
	*		to id
	*		type
	*		link
	*
	*	@param object notification object
	*/
	that.send = function(notification){

		//append meta data
		notification.sent = (new Date()).getTime();
		notification.read = false;

		return $firebaseArray(that.getUserNotificationsRef(notification.to_id)).$add(notification);
	};

	/**
	*	Mark All As Read
	*
	*	Mark all notifications as read on the firebase
	*
	*	@param userId owner of notification
	*/
	that.markAllAsRead = function(userId){

		var notificationsArray = that.getUserNotificationsArray(userId);

		notificationsArray.$loaded(function(notifications){

			//mark all as read
			notifications.forEach(function(notification){
				notification.read = true;

				//persist
				notificationsArray.$save(notification);
			});
		});
	};

	/**
	*	Delete
	*
	*	Delete a notification record from the firebase
	*
	*	@param object notification
	*/
	that.delete = function(notification){

		that.getUserNotificationsRef(notification.to_id).child(notification.refKey).remove();

		//remove locally
		_.remove(that.store,{refKey: notification.refKey});
	};

	/**
	*	Evaluate
	*
	*	Decide what action to take depending on the type of notification
	*/
	that.evaluate = function(notification){

		switch(notification.type){

			case "POST_BID":
			case "CLIENT_ACCEPT_BID":
				evaluateBid(notification);
				break;
			default: $location.url(notification.resource_id); //redirect to page
		}
	};

	/**
	*	Evaluate Bid
	*
	*	Evaluate Bid related notifications
	*
	*	@param object notification
	*/
	function evaluateBid(notification){

    switch (notification.type) {

		case "NEW_MESSAGE_WORKSTREAM":

	  	$location.url("/jobs/" + notification.resource_id);
	  	break;

	  	case "MILESTONE_UPDATE":

	  	$location.url("/jobs/" + notification.resource_id);
	  	break;

      case "POST_BID":

      	$location.url("/bids/" + notification.resource_id);
      	break;

      case "CLIENT_ACCEPT_BID":

        $location.url("/jobs");
        break;

			default:
				return;
		}

		// delete notification worldwide
		that.delete(notification);
	}

	return that;
};
