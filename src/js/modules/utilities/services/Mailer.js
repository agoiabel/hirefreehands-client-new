/**
*	Mailer Service
*
*	Used to send transactional email for required services where the user
* needs to be kept in the loop.
*/
module.exports = function (Restangular) {

	'use strict';

	var factory = {},
      hermes  = Restangular;

  /**
  * Send Template Mail
  *
  * Send emails that already have templates defined on the backend
  *
  * @param {String} template name
	* @param {String} email title
  * @param {String} email to send to
  * @param {Object} data payload
  */
  factory.sendTemplateMail = function (templateName, title, toUserId, payload) {

    hermes.all('mailer/send').post({template:templateName, title: title, to_user_id: toUserId, data:payload});
  };

  /**
  * Send Mail
  *
  * Send generic emails that already DON'T have templates defined on the backend
	*
	* @param {String} email title
  * @param {String} email to send to
  * @param {Object} data payload
  */
  factory.sendMail = function (title, toEmail, payload) {

    hermes.all('mailer/send').post({title: title, to_email: toEmail, data:payload});
  };

  return factory;
};
