/**
*	Uploader
*
*	Handles all asset uploading activities
*	to server, AWS S3, Cloudinary etc
*/
module.exports = function ($q,Upload,Restangular) {

	'use strict';

	var factory = {},
		hermes	= Restangular;

	/**
	*	Upload
	*
	*	Uploads an asset to the appropriate storage
	*	facility.
	*	Images should be sent to cloudinary
	*	All other documents should be sent to AWS S3
	*	[All assets will be sent to S3 in the meantime]
	*
	*	@param object file to upload
	*	@param string name to save file as
	*/
	factory.upload = function(file,saveAs){

		return factory.S3Upload(file,saveAs);
	};

	/**
	*	S3Upload
	*
	*	Upload files to S3
	*
	*	@param object file
	*	@param string name to save file as
	*/
	factory.S3Upload = function(file,saveAs){

		var fileExt = file.type.split('/')[1];
		var filename = (saveAs !== undefined) ? saveAs : (new Date()).getTime()+'.'+fileExt;

		//generate AWS S4 signature from the server
		return hermes.one('jobs/initialize_aws_upload').get()
		.then(function(response){

			response = response.data;

			return  Upload.upload({
				    url: response.url, //S3 upload url including bucket name
				    method: 'POST',
				    data: {
				        key: filename, // the key to store the file on S3, could be file name or customized
				        acl: 'public-read', // sets the access to the uploaded file in the bucket: private, public-read, ...
				        policy: response.policy,
				        'X-amz-credential': response.x_amz_credential,
				        'X-amz-algorithm': response.x_amz_algorithm,
				        'X-amz-date': response.x_amz_date,
				        'X-amz-expires': response.x_amz_expires,
				        'X-amz-signature': response.x_amz_signature,
				        'Content-Type': file.type !== '' ? file.type : 'application/octet-stream', // content type of the file (NotEmpty)
				        file: file
				    }
				});

		},function(error){
			return $q.defer().reject(error);
		});
	};

	/**
	*	Get Asset Url
	*
	*	Evaluate absolute url for an asset after successful
	*	upload to store
	*
	*	@param object data from which to evaluate url
	*/
	factory.getAssetUrl = function(info){

		var url = '';

		//if config.data.file is not undefined, this is an S3 upload
		if(info.config.data.file !== undefined){

			url = info.config.url + info.config.data.key;
		}

		return url;
	};

	return factory;
};
