/**
*	nbsp-2-br
*
*	Converts all newline/carrige return represented as 
*	_br_ to the <br> tag
*
*	@param string to convert
*/
module.exports = function(){

	'use strict';
	
	return function(arg){

		return (arg !== undefined) ? arg.replace(/(?:\n|_br_)/g,'<br>') : "";
	};
};