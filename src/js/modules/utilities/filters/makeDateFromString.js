/**
*	Make Date From String
*	Replace spaces with hyphens
*	
*	@param string Date string to convert to Date object
*/
module.exports = function(){

	return function(date){		
		return new Date(date);
	};
};