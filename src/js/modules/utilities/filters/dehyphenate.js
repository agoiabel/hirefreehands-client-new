/**
*	De-Hyphenate
*	Replace spaces with hyphens
*	
*	@param string argument to hyphenate
*/
module.exports = function(){

	return function(arg){

    if (/-/g.test(arg)) {
      arg = arg.replace(/-/g, " ");
    }

		return arg;
	};
};