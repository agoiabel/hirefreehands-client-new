/**
*	Change currency price base on location of user,s
*
*	@param string argument to clean contacts from
*/
module.exports = function($filter)	{

	/**
	 * Lookup currency 
	 * 
	 * @param country_code 
	 * @return              
	 */
	var currencyLookUp = function (country_code) {
	  var currency = {
	    'NG': 'NGN',
	    'CA': 'C$',
	    'US': '$',
	    'GB': 'GBP',
	    'AU': 'EUR',
	    'BE': 'EUR',
	    'CY': 'EUR',
	    'EE': 'EUR',
	    'FI': 'EUR',
	    'FR': 'EUR',
	    'DE': 'EUR',
	    'GI': 'EUR',
	    'IE': 'EUR',
	    'IT': 'EUR',
	    'LV': 'EUR',
	    'LT': 'EUR',
	    'LV': 'EUR',
	    'LU': 'EUR',
	    'MT': 'EUR',
	    'NL': 'EUR',
	    'PT': 'EUR',
	    'SK': 'EUR',
	    'SI': 'EUR',
	    'ES': 'EUR',
	    'AE': 'AED',
	    'default': '$'
	  }
	  return currency[country_code] || currency['default'];
	}

	/**
	 * check for empty string
	 * 
	 * @param string 
	 * @return        
	 */
	var checkEmpty = function (string) {
	    if (typeof string == 'undefined' || !string || string.length === 0 || string === "" || !/[^\s]/.test(string) || /^\s*$/.test(string) || string.replace(/\s/g,"") === "")
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	};

	var returnFomattedPrice = function(price) {
		var user_country = sessionStorage.getItem('user_country');

		var currencySymbol = currencyLookUp(user_country);

	    if (currencySymbol == 'NGN') {
	      return 'NGN' +" "+ $filter('number')(price);      
	    }
	    if (currencySymbol == '$') {
	      return '$' +" "+$filter('number')(Math.round((0.0036 * price)));
	      // return '$' +" "+Math.round((0.0036 * price));
	    }
	    if (currencySymbol == 'C$') {
	      return 'C$' +" "+$filter('number')(Math.round((0.0046 * price)));
	    }
	    if (currencySymbol == 'GBP') {
	      return 'GBP' +" "+$filter('number')(Math.round((0.0028 * price)));
	    } 
	    if (currencySymbol == 'EUR') {
	      return 'EUR' +" "+$filter('number')(Math.round((0.0031 * price)));
	    }
	    if (currencySymbol == 'AED') {
	      return 'AED' +" "+$filter('number')(Math.round((0.0133 * price)));
	    }
	}

	filterStub.$stateful = true;


	/**
	 * Handle the process of conversion
	 * 
	 * @param  price 
	 * @return       
	 */
	 function filterStub(price)	{
		
		// return price;

		//check to see if the user_country session storage isn't empty

		if ( checkEmpty( sessionStorage.getItem('user_country') )) {
			$.getJSON('//freegeoip.net/json/?callback=?', function(data) {
				var visitor_info = data.country_code;
				sessionStorage.setItem('user_country', visitor_info);
				return returnFomattedPrice(price);
			});			
		} else {
			return returnFomattedPrice(price);
		}

		

	};

	return filterStub;

};
