/**
*	Clean contacts from bidding messages
*
*	@param string argument to clean contacts from
*/
module.exports = function(){

	return function(arg){
		//Remove emails
		arg = arg.replace(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi, "***EMAIL***");
		//Remove phone numbers
		arg = arg.replace(/[\s()+-]*([0-9][\s()+-]*){10,20}/gi, " ***PHONE*** ")

		return arg;
	};
};