/**
*	Hyphenate
*	Replace spaces with hyphens
*	
*	@param string argument to hyphenate
*/
module.exports = function(){

	return function(arg){

		if (/\s+/g.test(arg)) {
      arg = arg.replace(/\s/g, "-");
		}

		return arg;
	};	
};

