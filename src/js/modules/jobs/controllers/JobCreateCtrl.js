/**
 *  Projects Controller
 *
 *  This controller handles all generic project related
 *  activities for all users
 */
module.exports = function ($scope, $rootScope, $mdDialog, $localStorage, $mdToast, $state, $stateParams, $location, $q, $sce, $filter, Notification, Job, Freelancer, Auth, Search, Paystack) {

  'use strict';

  $scope.createJob = createJob;

  //retrieve all categories and nested properties
  Job.getAllCategories().then(function (response) {

    //filter out the digital and domestic categories
    // $scope.categories = categories = _.groupBy(categories.data, 'type');
    $scope.categories = response.categories;

    //chunk categories into sets of 3
    // $scope.categoryList = _.chunk(categories.digital, 3);
    $scope.categoryList = _.chunk($scope.categories, 3);

    console.dir($scope.categories);
    console.dir($scope.categoryList);


    //initialize the domestic categories list
    // $scope.domesticCategoryList = categories.domestic;

  }, function (error) {

    $mdToast.show(
      $mdToast.simple().content('Error while retrieving job categories')
        .position('bottom right')
        .hideDelay(2000)
    );
  });


  /**
   * initialize job
   * 
   * @type
   */
  $scope.job = {
    addons: [],
    hasMilestones: '0'
  };

  /**
   *  Create Job
   *
   *  Reveal create job dialog with the
   *  form to create a new job
   *
   *  @param object event
   *  @param object chosen job category
   *  @param string type of category[digital/domestic]
   *  @param object freelancer job is being posted to[optional]
   */
  function createJob(ev, category, freelancer) {

    var type = category.type;
    $scope.job.category = category;

    if (type == 'hfhe') {
      return $state.go('app.createhfhejob');
    }

    var modalTemplates = {
      'digital': 'templates/jobs/create-form.html',
      // 'website': 'templates/jobs/website-form.html',
    };

    $mdDialog.show({
      locals: {
        job: $scope.job,
        postJob: postJob,
        categories: $scope.categories,
        freelancer: freelancer,
        imageUrl: $scope.imageUrl,
      },
      controller: type === 'digital' ? require('./CreateDigitalJobCtrl.js') : require('./CreateWebsiteJobCtrl.js'),
      templateUrl: modalTemplates[type],
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: false
    });
  }

  /**
   *  Post Job
   *
   *  Posts a new Job
   *
   *  @param object new job
   */
  function postJob(job, ev) {

    var newJob = {};

    newJob.title = job.title;
    newJob.job_category_id = job.category.id;
    newJob.description = job.description;
    newJob.price = job.price;
    newJob.hasMilestones = job.hasMilestones;
    newJob.addons = _.map(job.addons, 'id');

    newJob.job_category_scope_id = job.scope.id;
    newJob.scope_duration_point_id = job.tier.duration_id;

    newJob.coupon_id = job.coupon_id;
    newJob.discount = job.discount;
    newJob.job_category_child_scope_id = job.job_category_child_scope_id;

    job.isPostingInProgress = true;

    Job.post(newJob).then(function (response) {
      console.dir(response.message);
      $mdToast.show(
        $mdToast.simple().content('Job posted successfully')
          .position('bottom right')
          .hideDelay(2000)
      );

      $mdDialog.cancel();

      $state.go('app.jobs');

    }, function (error) {

      $mdToast.show(
        $mdToast.simple().content('You have errors on the job posting form')
          .position('bottom right')
          .hideDelay(2000)
      );
    }).finally(function () {
      job.isPostingInProgress = false;
    });

    /**
     * display pop to collect number
     * 
     * @param  $scope            
     * @param  verification_code 
     * @param  newJob
     * @return                   
     */
      // $mdDialog.cancel();
      // $mdDialog.show({

      //   locals: {newJob: newJob},
      //   controller: function ($scope, newJob) {

      //     $scope.newJob = newJob;
      //     $scope.isPostingInProgress = false;
      //     $scope.codeSent = false;                 


      //     $scope.sendCode = function (user) {
      //       $scope.isPostingInProgress = true;

      //       var number = parseInt(user.phoneNumber);
      //       var country_code = parseInt(user.countryCode);

      //       Job.postNumber('/sendtoken/store/', {country_code: country_code, number: number}).then(function (response) {

      //             $scope.isPostingInProgress = false; 
      //             $scope.codeSent = true;                 
      //             $scope.phone = {code: ''};

      //             //handle the case of wrong phone number
      //             // if (response.nexmo) {
      //             //   $mdToast.show(
      //             //     $mdToast.simple().content('You provided an invalid number')
      //             //       .position('top right')
      //             //       .hideDelay(2000)
      //             //   );
      //             //   return;
      //             // }

      //             var client_phone_number = response.data.phone_number;
      //             var verification_code = response.data.token;

      //             $scope.verifyCode = function (phone) {

      //                 console.dir("verify code");
      //                 console.dir(phone.code);
      //                 console.dir(verification_code);

      //                 // $scope.isPostingInProgress = true;

      //                 if (verification_code == phone.code) {

      //                     //user,s phone number
      //                     $scope.newJob.client_phone_number = client_phone_number;

      //                     Job.post($scope.newJob).then(function (response) {

      //                       $scope.isPostingInProgress = false;
                            
      //                       $mdToast.show(
      //                         $mdToast.simple().content('Job posted successfully')
      //                           .position('bottom right')
      //                           .hideDelay(2000)
      //                       );

      //                       $mdDialog.cancel();

      //                       $state.go('app.jobs');
      //                     }, function (error) {

      //                       $mdToast.show(
      //                         $mdToast.simple().content('You have errors on the job posting form')
      //                           .position('bottom right')
      //                           .hideDelay(2000)
      //                       );

      //                     }).finally(function () {
      //                       job.isPostingInProgress = false;
      //                     });

      //                 } else {
      //                       $mdToast.show(
      //                         $mdToast.simple().content('The verification code is not correct')
      //                           .position('top right')
      //                           .hideDelay(2000)
      //                       );
      //                 }

      //             }

      //       }, function (error) {
      //         console.dir(error);
      //       });

      //       //send phone phone number and return code
      //     }

      //     $scope.close = function () {
      //       $mdDialog.cancel();
      //     };
      //   },

      //   templateUrl: 'templates/jobs/get_user_phone_number.html',
      //   parent: angular.element(document.body),
      //   clickOutsideToClose: true
      // });
  }

};
