/**
 *  Bid Ctrl
 */
module.exports = function ($scope, $filter, $state, $stateParams, $sce, $mdDialog, $mdToast, $location, Notification, Job, Freelancer, Auth, Paystack, Uploader, Mailer, User) {

  'use strict';

  $scope.job = $stateParams.job;

  //get Job Title
  var jobTitle = $stateParams.jobTitle;

  //call the worker class/function for bids
  var BidClass = require('./BidClass.js');

  // Check if this job belongs to the current logged in user
  $scope.belongsToCurrentUser = function (job) {

    if (job !== undefined) {
      return Auth.getUser().uid === job.client_id;
    }

    return false;
  };

  //get job details as specified by the ID extracted from the URL
  Job.get(jobTitle).then(function (response) {

    var currentJob = $scope.job = response.data;
    var job = $scope.job;

    if ($scope.job === null) {

      //send user back to the "BROWSE JOBS" section
      $mdToast.show(
        $mdToast.simple().content('That Job does not exist')
          .position('bottom right')
          .hideDelay(2000)
      );

      $state.go('app.jobs');
    }


    //initialize bid and bid history
    BidClass($scope, $filter, $sce, $state, $mdDialog, currentJob, showTnC, Job, Auth, User, Notification, Uploader, Mailer);

    if ($scope.job.freelancer_signed == "0" && $scope.job.freelancer_id == Auth.getUser().uid) {
      $mdDialog.show({

        locals: { currentJob: currentJob, belongsToCurrentUser: $scope.belongsToCurrentUser },
        controller: function ($scope, $mdDialog, $filter, currentJob, belongsToCurrentUser, Job, Auth, Notification, Freelancer, Mailer, Paystack) {

          // Call the job metadata<freelancer,addons and milestones>
          $scope.isLoadingJobMeta = true;

          // Reload the job information
          Job.get($filter('hyphenate')(job.title), 'addons,milestones,categorysize').then(function (job) {

            $scope.currentJob = _.assign(currentJob, job.data);
            $scope.rightNow = new Date();
            $scope.isClient = job.data.client_id === Auth.getUser().uid;

          }, function () {

            $mdToast.show(
              $mdToast.simple()
                .content('There was a problem while retrieving all the required job information.')
                .position('bottom right')
                .hideDelay(2000)
            );
          })
            .finally(function () {
              $scope.isLoadingJobMeta = false;
            });

          $scope.agree = function (job) {

            if (belongsToCurrentUser(job)) {

              // Client signs the agreement and pays upfront.
              Job.signAgreement({job_id: job.id, type: "client"}).then(function () {

                // Close Contract modal
                $mdDialog.cancel();

                // Generate a payment transaction ID on the server
                Paystack.generateTransxId('/jobs/initiate_payment/', {job_id: job.id}).then(function (response) {

                  //Configure and initialise Paystack for first time client[has no authorization code]
                  if (!response.data.hasAuthorizationCode) {

                    Paystack.init({

                      amount: response.data.amount_in_kobo, //amount in Kobo
                      ref: response.data.transaction_ref, //transaction reference generated on the server
                      email: response.data.email, // Client email
                      callback: function (paystackResponse) {
                        verify(paystackResponse);
                      }
                    });

                  } else {

                    /**
                     *  this means this customer already has an authorization code
                     *  and payment has been initiated on the server
                     */
                    if (response.data.status) {
                      onVerificationSuccess();
                    } else {

                      alert("An error occured while finalising your payment, please try again...");
                    }
                  }

                  /**
                   *  Verify the transaction on the server
                   */
                  function verify(paystackResponse) {

                    //verify transaction on the server
                    Paystack.verify('jobs/verify_paystack_payment', {data: paystackResponse}).then(function (verificationResponse) {

                      if (verificationResponse.status) {
                        onVerificationSuccess();
                      } else {

                        alert("An error occured while finalising your payment, please try again...");
                      }
                    });
                  }

                  /**
                   *   Yippe! Transaction successful! Send notification to
                   *   freelancer to get working.
                   */
                  function onVerificationSuccess() {

                    Job.setFreelancer({job_id: job.id, freelancer_id: job.assigned_freelancer_id}).then(function () {

                      Freelancer.get(job.assigned_freelancer_id).then(function (freelancer) {

                        // Send in-app notification
                        var notification = {
                          message: '<span class="capitalize bold">' + Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> just accepted your bid for the <span class="capitalize bold">"' + job.title + '"</span> job',
                          sender_id: Auth.getUser().uid,
                          sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                          to_id: job.assigned_freelancer_id,
                          type: "CLIENT_ACCEPT_BID",
                          resource_id: $filter('hyphenate')(job.title)
                        };

                        Notification.send(notification);

                        // Send email notification
                        Mailer.sendTemplateMail('bid_freelancer', 'Congratulations! Your Bid Has Been Approved.', freelancer.data.email, {
                          job_title: job.title,
                          firstname: freelancer.data.name.split(' ')[0],
                          lastname: freelancer.data.name.split(' ')[1],
                          start_job_link: '/jobs'
                        });

                        $state.go('app.job', {job: job});
                      });
                    });
                  }
                }, function () {

                  alert('An error occured while initialising transaction on the server');
                });
              });
            } else {
              console.dir('testing okay');
              Job.signAgreement({job_id: job.id, type: "freelancer"}).then(function () {

                // Close contract modal
                $mdDialog.cancel();

                // Proceed to job stream
                $state.go('app.job', {job: job});
              });

            }
          };

          $scope.cancel = function () {
            $mdDialog.cancel();
          };
        },
        templateUrl: 'templates/jobs/tnc.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        clickOutsideToClose: false
      });
    }


  }, function () {

    //send user back to the "BROWSE JOBS" section
    $mdToast.show(
      $mdToast.simple().content('An error occured while your job was loading')
        .position('bottom right')
        .hideDelay(2000)
    );

    $state.go('app.jobs');
  });

  /**
   *  Show T&C
   *
   *  @param object event
   *  @param object job
   */
  function showTnC (ev, job) {
    $mdDialog.show({

      locals: { currentJob: job, belongsToCurrentUser: $scope.belongsToCurrentUser },
      controller: function ($scope, $mdDialog, $filter, currentJob, belongsToCurrentUser, Job, Auth, Notification, Freelancer, Mailer, Paystack) {

        // Call the job metadata<freelancer,addons and milestones>
        $scope.isLoadingJobMeta = true;

        // Reload the job information
        Job.get($filter('hyphenate')(job.title), 'addons,milestones,categorysize').then(function (job) {

          $scope.currentJob = _.assign(currentJob, job.data);
          $scope.rightNow = new Date();
          $scope.isClient = job.data.client_id === Auth.getUser().uid;

        }, function () {

          $mdToast.show(
            $mdToast.simple()
              .content('There was a problem while retrieving all the required job information.')
              .position('bottom right')
              .hideDelay(2000)
          );
        })
          .finally(function () {
            $scope.isLoadingJobMeta = false;
          });

        $scope.agree = function (job) {

          if (belongsToCurrentUser(job)) {

            // Client signs the agreement and pays upfront.
            Job.signAgreement({job_id: job.id, type: "client"}).then(function () {

              // Close Contract modal
              $mdDialog.cancel();

              var country_code = localStorage.getItem('user_country');
              console.dir(country_code);

              //Generate a payment transaction ID on the server
              Paystack.generateTransxId('/jobs/initiate_payment/', {job_id: job.id, country_code: country_code}).then(function (response) {

                console.dir(response.data);
                //Configure and initialise Paystack for first time client[has no authorization code]
                if (!response.data.hasAuthorizationCode) {

                  Paystack.init({

                    amount: response.data.amount_in_kobo, //amount in Kobo
                    ref: response.data.transaction_ref, //transaction reference generated on the server
                    email: response.data.email, // Client email
                    callback: function (paystackResponse) {
                      verify(paystackResponse);
                    }
                  });

                } else {

                  /**
                   *  this means this customer already has an authorization code
                   *  and payment has been initiated on the server
                   */
                  if (response.data.status) {
                    onVerificationSuccess();
                  } else {

                    alert("An error occured while finalising your payment, please try again...");
                  }
                }

                /**
                 *  Verify the transaction on the server
                 */
                function verify(paystackResponse) {

                  //verify transaction on the server
                  Paystack.verify('jobs/verify_paystack_payment', {data: paystackResponse, country_code: country_code}).then(function (verificationResponse) {

                    if (verificationResponse.status) {
                      onVerificationSuccess();
                    } else {
                      alert("An error occured while finalising your payment, please try again...");
                    }
                  });
                }

                /**
                 *   Yippe! Transaction successful! Send notification to
                 *   freelancer to get working.
                 */
                function onVerificationSuccess() {

                  Job.setFreelancer({job_id: job.id, freelancer_id: job.assigned_freelancer_id}).then(function () {

                    Freelancer.get(job.assigned_freelancer_id).then(function (freelancer) {

                      // Send in-app notification
                      var notification = {
                        message: '<span class="capitalize bold">' + Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> just accepted your bid for the <span class="capitalize bold">"' + job.title + '"</span> job',
                        sender_id: Auth.getUser().uid,
                        sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                        to_id: job.assigned_freelancer_id,
                        type: "CLIENT_ACCEPT_BID",
                        resource_id: $filter('hyphenate')(job.title)
                      };

                      Notification.send(notification);

                      // Send email notification
                      Mailer.sendTemplateMail('bid_freelancer', 'Congratulations! Your Bid Has Been Approved.', freelancer.data.email, {
                        job_title: job.title,
                        firstname: freelancer.data.name.split(' ')[0],
                        lastname: freelancer.data.name.split(' ')[1],
                        start_job_link: '/jobs'
                      });

                      $state.go('app.job', {job: job});
                    });
                  });
                }
              }, function () {

                alert('An error occured while initialising transaction on the server');
              });

            });
          } else {

            Job.signAgreement({job_id: job.id, type: "freelancer"}).then(function () {

              // Close contract modal
              $mdDialog.cancel();

              // Proceed to job stream
              $state.go('app.job', {job: job});
            });
          }
        };

        $scope.cancel = function () {

          $mdDialog.cancel();
        };
      },
      templateUrl: 'templates/jobs/tnc.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  }
};
