/**
*	BidClass
*
*	Utility construct common to all
*	Job bid contexts
*/
module.exports = function($scope, $filter, $sce, $state, $mdDialog, currentJob, showTnC, Job, Auth, User, Notification, Uploader, Mailer) {

	'use strict';

	$scope.currentJob = currentJob;
	$scope.currentJob.acceptedBid = false;

	console.dir($scope.currentJob);

	$scope.newBid = {

		id: Auth.getUser().id,
		name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
		status: "pending",
		bidderImageUrl: Auth.getUser().image == undefined ? '' : Auth.getUser().image,
		created: (new Date()).getTime()
	};

	//retrieve all bids
	$scope.isLoading = true;
	$scope.allBids = Job.getBids(currentJob);

	$scope.allBids.$loaded(function(response){

		$scope.isLoading = false;

		//make the logged in user the active freelancer
		$scope.activeFreelancerId = Auth.getUser().id;

		if(response.length > 0){

			$scope.groupedChat = _.groupBy($scope.allBids,'freelancerId');

			//only crunch freelancers when it's the client viewing
			if($scope.belongsToCurrentUser(currentJob)) {

	  			$scope.freelancers = _.clone($scope.allBids);

	  			$scope.freelancers = _.uniqBy($scope.freelancers,'freelancerId');

	  			$scope.makeActive($scope.freelancers[0].id);
	  		} else {
	  			$scope.makeActive($scope.activeFreelancerId);
	  		}
		}
	});

	//listen on additions to job bids
	Job.getBidsRef(currentJob).on('child_added',function() {

		$scope.allBids.$loaded(function(response) {

			//recrunch the chat
			$scope.groupedChat = _.groupBy($scope.allBids,'freelancerId');

			//only crunch freelancers when it's the client viewing
			if($scope.belongsToCurrentUser(currentJob)){

	  			$scope.freelancers = _.clone($scope.allBids);

	  			$scope.freelancers = _.uniqBy($scope.freelancers,'freelancerId');

	  			$scope.makeActive($scope.activeFreelancerId);
	  		}else{

	  			$scope.makeActive($scope.activeFreelancerId);
	  		}
  		});
	});

	//listen for changes on the individual bids
	Job.getBidsRef(currentJob).on('child_changed',function(bid){

		//if the current user is a freelancer and his/her bid just got accepted, alert them
		if(bid.val().status === "accepted") {

			//update flag to reflect new bid status
			currentJob.acceptedBid = true;
		}
	});

	/**
	*	Upload
	*
	*	Upload and save file details
	*	used for job bids
	*
	*	@param object file
	*/
  $scope.upload = function (file) {

  	if(file !== null){

  		$scope.showProgress = true;

  		//save job documents in /jobs/<jobId>/<unique_document_ref>
  		var fileTypeData = file.type.split('/'),
  		fileExt = fileTypeData[1],
  		fileType = fileTypeData[0].toUpperCase();

  		var saveAs = 'jobs/'+currentJob.title+'/'+(new Date()).getTime()+'.'+fileExt;

		//call the uploader
    	Uploader.upload(file,saveAs)
    	.then(function(response){

			//retrieve the file url
			var url = Uploader.getAssetUrl(response);

			var newBidItem = {

				id: Auth.getUser().id,
				attachment: {
					type: fileType,
					url: url
				},
				freelancerId: $scope.activeFreelancerId,
				name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
				bidderImageUrl: Auth.getUser().image,
				created: (new Date()).getTime()
			};

			//post comment
			$scope.allBids.$add(newBidItem);

		},function(error){

			$mdToast.show(
		      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
		        .position('bottom right')
		        .hideDelay(2000)
		    );

		},function(evt){

			//reveal and update upload bar
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			$scope.uploadProgress = progressPercentage+'%';
			$scope.showProgress = true;

		}).finally(function(){

			//hide progress bar
			$scope.showProgress = false;
		});
	}
  };

  $scope.viewFreelancerProfile = function (uid, ev) {
  	User.get(uid, true).then(function (user) {

  		$mdDialog.show({
	      locals:{freelancer:user.data,imageUrl:$scope.imageUrl,hireMe:$scope.hireMe,getUserRole:$scope.getUserRole,getUserInitials:$scope.getUserInitials},
	      controller: function($scope,freelancer,imageUrl,hireMe,getUserRole,getUserInitials){

	      	$scope.imageUrl = imageUrl;
	      	$scope.getUserRole = getUserRole;
			$scope.getUserInitials = getUserInitials;

	      	//make freelancer info available in the view
	      	$scope.user = freelancer;
	      	$scope.user.skills = $scope.user.skills.data;

	      	//this view is for other users that want to see a freelancer's profile and probably hire, create flag for this
	      	$scope.isOwner = false;

	      	/**
			*	Go To Tab
			*
			*	Makes selected tab index active
			*
			*	@param int tab index
			*/
			$scope.goToTab = function(tabIndex){
				$scope.selectedIndex = tabIndex;
			};	
	      },
	      templateUrl: 'templates/freelancers/profile.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true
	    });
  	})
  }

	/**
	*	Post Bid
	*
	*	@param object new Bid
	*/
	$scope.postBid = function(newBid){

		//specify which freelancer this message is associated
		newBid.freelancerId = $scope.activeFreelancerId;

		//prevent empty comments/bids
		if(Boolean(newBid.comment)){

			//clean comment
			newBid.comment = $filter('cleanContact')(newBid.comment);

			//post bid
			$scope.allBids.$add(newBid);

			//clear text field
			newBid.comment = '';

			//notify client
			var rhetoric = $scope.belongsToCurrentUser(currentJob) ? 'responded to your bid for the' : 'posted a bid for the';
			var notification = {

				message: '<span class="capitalize bold">'+Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> ' + rhetoric+' <span class="capitalize bold">"'+ currentJob.title + '"</span> job',
				sender_id: Auth.getUser().id,
				sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
				to_id: $scope.belongsToCurrentUser(currentJob) ?  $scope.activeFreelancerId : currentJob.client_id,
				type: "POST_BID",
				resource_id: $filter('hyphenate')(currentJob.title)
			};

			Notification.send(notification);

			//send email notification //to be completed
			Mailer.sendTemplateMail('bid_client', 'You Have a New Bid on Your Job!', currentJob.owner.id, {
				bid_link: '/bids/' + $filter('hyphenate')(currentJob.title)
			});
		}

	};

	/**
	* Format Comment
	*
	*	Convert end of line to <br>
	*
	*	@param string raw comment
	*/
	$scope.formatComment = function(comment){

		return $sce.trustAsHtml($filter('nbsp2br')(comment));
	};

	/**
	* 	makeActive
	*
	*	Make the chat history active for a particular freelancer
	*
	*	@param string freelancer id
	*/
	$scope.makeActive = function(freelancerId){

		$scope.activeFreelancerId = freelancerId;
		$scope.activeBid = $scope.groupedChat[freelancerId];
	};

	/**
	* 	isActiveFreelancer
	*
	*	@param string freelancer id
	*/
	$scope.isActiveFreelancer = function(freelancerId){

		return $scope.activeFreelancerId === freelancerId;
	};

	/**
	*	Check if this job belongs to the current logged in user
	*/
	$scope.belongsToCurrentUser = function(job){
	    if (!Boolean(Auth.getUser()) || !Boolean(job)) {
	      return false;
	    }
	    
	    return Auth.getUser().id === job.client_id;
	};

	/**
	*	Determine when to show the chat box
	*/
	$scope.showChatbox= function(job){

		if($scope.allBids !== 'loading'){

			if($scope.belongsToCurrentUser(job) && $scope.freelancers !== undefined && $scope.freelancers.length > 0){
				return true;
			}else if(!$scope.belongsToCurrentUser(job)){
				return true;
			}
		}

		return false;
	};

	/**
	*	Determine when to show client bid options
	*/
	$scope.showBidOptions = function(chat){
		if(chat != undefined)
		{
			return $scope.belongsToCurrentUser(currentJob) && !$scope.ownsPost(chat);
		}
	};

	/**
	*	Determine whether the current user owns a chat message
	*/
	$scope.ownsPost = function(chat){
		return chat.id === Auth.getUser().id;
	};

	/**
	*	Accept a freelancer's bid
	*/
	$scope.acceptBid = function(bid){

		// Activate spinner
		$scope.isAcceptingBid = true;

		// Update Bid status
		bid.status = "accepted";

	    $scope.allBids.$save(bid).then(function(){

	    	// Temporarily assign freelancer to current job on bid acceptance
	    	currentJob.assigned_freelancer_id = bid.freelancerId;
	    	currentJob.assigned_freelancer_full = bid.name;

	    	// Stop spinner
			$scope.isAcceptingBid = false;

	      // load dynamic T&C for client to sign
	      showTnC(null,currentJob);
	    });
	};

	/**
	*	Decline a freelancer's bid
	*/
	$scope.declineBid = function(bid){

		// Activate spinner
		$scope.isDecliningBid = true;

		// Update Bid status
		bid.status = "declined";

		$scope.allBids.$save(bid).then(function(){

			// Stop spinner
			$scope.isDecliningBid = false;

			// TODO: send notification to freelancer
		});
	};

  $scope.undoBidAction = function(bid){

    // Activate spinner
    $scope.isUndoing = true;

    // Update Bid status
    bid.status = "pending";

    $scope.allBids.$save(bid).then(function(){

    	// Remove assigned freelancer if his/her bid becomes unaccepted
			if (currentJob.assigned_freelancer_id) {
				currentJob.assigned_freelancer_id = undefined;
			}

      // Stop spinner
      $scope.isUndoing = false;
    });
  };
};
