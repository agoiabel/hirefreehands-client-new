module.exports = function ($state, $scope, $timeout, $mdDialog, job, postJob, categories, freelancer, imageUrl, Job, $filter) {

  console.dir(job.category);

  'use strict';

  // Hold coupons internally.
  var coupons = [];

  $scope.job = job;
  $scope.job.moreSkills = [];
  $scope.job.coupon = null;
  $scope.job.discount = 0;

  $scope.postJob = postJob;
  $scope.imageUrl = imageUrl;

  $scope.resetJobPrice = resetJobPrice;
  $scope.setScopeDurationAndAddon = setScopeDurationAndAddon;
  $scope.evaluateJobPrice = evaluateJobPrice;
  
  $scope.addAddon = addAddon;
  $scope.job_scope_has_milestone = 0;
  $scope.effectCoupon = effectCoupon;
  $scope.closeJobPostingDialog = closeJobPostingDialog;
  $scope.postJob = postJob;


  Job.getAllCoupons().then(function (response) {
    coupons = response.coupons;
  });

  // monitor for changes to the milestone option
  $scope.$watch('job.hasMilestones', function () {
    //removeSelectedAddons(); //why is pay on milestone removing addons
    toggleMilestones();
  });

  /**
   * Evaluate current job price
   * based on it's surrounding factors.
   * 
   * @param job - entire job object
   */
  function evaluateJobPrice(job) {

    var tierBasePrice = job.tier ? Number(job.tier.price) : 0; //duration_of_job
    var selectedAddonsTotalPrice = 0;
    var milestonesTotalPrice = 0;

    switch (true) {

      case job.addons.length > 0:

        job.addons.forEach(function (addon) {
          //work on updating the add base on operation
          if (addon.operator == "addition") {
            selectedAddonsTotalPrice += tierBasePrice * (Number(addon.weight / 100));
          } else {
            selectedAddonsTotalPrice -= tierBasePrice * (Number(addon.weight / 100));
          }
        });

        break;

      case job.hasMilestones === '1':

        milestonesTotalPrice = tierBasePrice * Number(job.category.milestone_weight / 100);
        break;

      default:

        $scope.job.price = tierBasePrice;
        return;
    }

    $scope.job.price = tierBasePrice + selectedAddonsTotalPrice + milestonesTotalPrice;
  }

  /**
   * check if addon exists
   * 
   * @param addon
   * @return      
   */
  function checkIfExists(addon)
  {
    for (var j = 0; j < $scope.job.addons.length; j++) {
      if ( addon.id == $scope.job.addons[j].id ) {
        return true;
      }
    }
    return false;
  }

  /**
   * remove addon
   * 
   * @param addon 
   * @return
   */
  function removeAddon(addon) 
  {
    var addons = $scope.job.addons;

    for (var j = 0; j < addons.length; j++) {  
      if ( addon.id == addons[j].id ) {
        var index = addons.indexOf(addons[j]);
        if (index > -1) {
          addons.splice(index, 1);
        }
      }
    }  

    $scope.job.addons = addons;
  }

  /**
   *  Add an add on
   *
   *  @param object addon
   */
  function addAddon(addon) 
  {
    var tierBasePrice = Number(job.tier.price);
    $scope.job.price = Number($scope.job.price);
    addon.weight = Number(addon.weight);

    if ( ! checkIfExists(addon) ) {

      $scope.job.addons.push(addon);
      if (addon.operator == "addition") {
        $scope.job.price += tierBasePrice * (addon.weight / 100);
      } else {
        $scope.job.price -= tierBasePrice * (addon.weight / 100);
      }
    } else {

      removeAddon(addon);
      if (addon.operator == "addition") {
        $scope.job.price -= tierBasePrice * (addon.weight / 100);
      } else {
        $scope.job.price += tierBasePrice * (addon.weight / 100);
      }
    }
  }

  /**
   *  Add Milestones
   *
   *  Increases evaluated price by the given
   *  categories milestone weight value
   */
  function toggleMilestones() {

    //if milestones have been selected, increase evaluated job price
    if ($scope.job.price) {

      var milestoneWeight = Number($scope.job.category.milestone_weight);
      var tierBasePrice = job.tier ? Number($scope.job.tier.price) : 0;

      $scope.job.price = Number($scope.job.price);

      if ($scope.job.hasMilestones === '0') {

        $scope.job.price -= tierBasePrice * (milestoneWeight / 100);
      } else {
        $scope.job.price += tierBasePrice * (milestoneWeight / 100);
      }
    }
  }

  /**
   * Remove all selected addons
   */
  function removeSelectedAddons() {

    var selectedAddons = $scope.job.addons;
    var tierBasePrice = job.tier ? Number(job.tier.price) : 0;

    if (selectedAddons.length > 0) {

      $scope.job.addons = selectedAddons.map(function (addon) {

        addon.isChecked = false;
        if (addon.operator == "addition") {
          $scope.job.price += tierBasePrice * (addon.weight / 100);
        } else {        
          $scope.job.price -= tierBasePrice * (addon.weight / 100);
        }
        return addon;
      });

      $scope.job.addons = [];
    }
  }

  /**
   * Reset just the job's `price` binding.
   */
  function resetJobPrice() {    
    if ($scope.job.price) {
      $scope.job.price = undefined;
    }
  }

  /**
   * make sure duration shown is only for the selected job_scope
   * 
   * @param 
   */
  function setScopeDurationAndAddon (scope) {
    $scope.job_scope_durations = scope.scope_duration_points;
    $scope.job_scope_addons = scope.job_category_scope_addons;

    if (scope.has_milestone == '1') {
      $scope.job_scope_has_milestone = 1;
    }
  }
 
  /**
   * Close job posting dialog
   * and reset the form within it.
   */
  function closeJobPostingDialog() {
    $mdDialog.cancel();
    $state.reload();
  }


  /**
   * Discount job price if coupon is valid, else, error out.
   */
  function effectCoupon(jobCoupon) {
    //make sure only one coupon can be use, and when used...cant be use twice
    $scope.job.discount = 0;
    $scope.couponExistsError = false;

    var validCoupon = couponExists(jobCoupon, coupons);

    if (validCoupon) {
      $scope.job.coupon_id = validCoupon.id;
      $scope.job.discount = (validCoupon.weight/100 * $scope.job.price);
      $scope.job.price = $scope.job.price - (validCoupon.weight/100 * $scope.job.price);
    } else {
      $scope.couponExistsError = true;
    }
  }

  /**
   * Check for coupon existence.
   *
   * @param jobCoupon
   * @param coupons
   * @returns {*}
   */
  function couponExists(jobCoupon, coupons) {
    return _.find(coupons, function (coupon) {
      return jobCoupon.toUpperCase() === coupon.code.toUpperCase();
    });
  }

  
};
