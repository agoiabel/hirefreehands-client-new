/**
 *  Projects Controller
 *
 *  This controller handles all generic project related
 *  activities for all users
 */
module.exports = function ($scope, $rootScope, $mdDialog, $localStorage, $mdToast, $state, $stateParams, $location, $q, $sce, $filter, Notification, Job, Freelancer, Auth, Search, Paystack) {

  'use strict';

  $scope.allJobs = [];

  $scope.job = {

    addons: [],
    hasMilestones: '0'
  };

  //define filter models
  $scope.filter = {category: {}, duration: {boundary: 0}, cost: {amount: 0, boundary: 3}};

  //internally hold the giant job model
  var allJobs;

  //query string model for job Search
  $scope.query = "";

  //setup watch task for when the query string changes
  $scope.$watch('query', startSearch);

  $scope.$on('jobStart', function(event, job){ startJob(event, job)});

  //scope variables for monitoring AJAX status of jobs and categories resources
  $scope.isLoadingJobs = $scope.isLoadingCategories = true;

  //make the create job available on the front end
  $scope.createJob = createJob;

  //retrieve all categories and nested properties
  var jobs = Job.getAllCategories().then(function (categories) {

    console.dir(categories.data);

    //filter out the digital and domestic categories
    // $scope.categories = categories = _.groupBy(categories.data, 'type');
    $scope.categories = categories = categories.data;

    //chunk categories into sets of 3
    // $scope.categoryList = _.chunk(categories.digital, 3);
    $scope.categoryList = _.chunk($scope.categories, 3);

    //initialize the domestic categories list
    // $scope.domesticCategoryList = categories.domestic;
  }, function (error) {

    $mdToast.show(
      $mdToast.simple().content('Error while retrieving job categories')
        .position('bottom right')
        .hideDelay(2000)
    );
  });

  $q.all([jobs]).then(function () {
    $scope.isLoadingCategories = false;
  });

  /**
   *  Check if this job belongs to the current logged in user
   */
  $scope.belongsToCurrentUser = function (job) {

    if (!Boolean(Auth.getUser()) || !Boolean(job)) {
      return false;
    }
    return Auth.getUser().uid === job.client_id;
  };

  //check if a client is trying to post to a freelancer
  if ($stateParams.freelancer !== undefined) {

    $scope.createJob(undefined, undefined, 'digital', $stateParams.freelancer);
  }

  //get all jobs
  Job.getAll().then(function (jobs) {

    //initialize giant model
    allJobs = jobs.data;

    //initialize job listing on side nav
    $rootScope.activeJobs = allJobs.active;
    $rootScope.pendingJobs = allJobs.pending;
    $rootScope.completedJobs = allJobs.completed;

    //chunk into rows of 3
    $scope.allJobsChunked = chunkAndPad(allJobs.all);

    }, function () {

    $mdToast.show(
      $mdToast.simple().content('Error while retrieving jobs')
        .position('bottom right')
        .hideDelay(2000)
    );
  }).finally(function () {
    $scope.isLoadingJobs = false;
  });

  /**
   *  View Job
   *
   *  View a Job which already has a freelancer assigned
   *  and the current user is niether the client nor
   *  the freelancer
   *
   *  @param object new job
   */
  $scope.viewJob = function (ev, job) {

    $mdDialog.show({

      locals: {job: job, formatComment: $scope.formatComment},
      controller: function ($scope, job, formatComment) {

        $scope.job = job;
        $scope.formatComment = formatComment;

        $scope.close = function () {
          $mdDialog.cancel();
        };
      },

      templateUrl: 'templates/jobs/view-job.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  };

  /**
   *  Define filters
   *
   *  Ensure the filter has already been activated
   *  before the actual filter function is passed.
   *  This will ensure we don't do more work than is necessary eg,
   * calling all filters when the user has only clicked on the category filter
   *  and the rest are still at default.
   */
  var isFilterActive = {category: false, duration: false, cost: false};
  var filters = [

    function filterByCategory() {

      /**
       *  Are All Categories Off
       *
       *  Evaluates if all categories are switched off
       */
      function areAllCategoriesOff() {

        var filterValues = _.map($scope.filter.category);
        var areAllOff;

        //if the category keys have not been set, then they are off....:(
        if (filterValues.length === 0) {
          return true;
        } else if (filterValues.length === 1) {

          areAllOff = filterValues[0] === true ? false : true;

        } else if (filterValues.length > 1) {

          areAllOff = _.reduce(filterValues, function (current, next) {
            return filterValues[0] === false && next === false ? true : false;
          });
        }

        return areAllOff;
      }

      /**
       *  Category Filter
       *
       *  @param object job to filter
       */
      function filter(job) {

        if ($scope.filter.category[job.category] !== undefined && $scope.filter.category[job.category] === true) {
          return true;
        }
      }

      if (isFilterActive.category) {

        /**
         *  Worker function that filters the entirety of the
         * giant job object based on the specified filter
         */
        return function (jobs) {

          var result = {};

          /**
           *  If this filter has been activated but all categories are turned off,
           *  then return all the jobs, this is applicable when the user toggles off a category
           */

          var categoriesAreAllOff = areAllCategoriesOff();

          result.all = categoriesAreAllOff ? jobs.all : _.filter(jobs.all, filter);
          result.posted = categoriesAreAllOff ? jobs.posted : _.filter(jobs.posted, filter);
          result.freelance = categoriesAreAllOff ? jobs.freelance : _.filter(jobs.freelance, filter);
          result.posted_to_you = categoriesAreAllOff ? jobs.posted_to_you : _.filter(jobs.posted_to_you, filter);

          return result;
        };
      }

      //ensure we don't unecessarily call the filter
      return false;
    },
    function filterByCost() {

      /**
       *  Cost Filter
       *
       *  @param object job to filter
       */
      function filter(job) {

        switch ($scope.filter.cost.boundary) {

          case 1://less than

            return Number(job.price) < Number($scope.filter.cost.amount);

          case 2://equal to

            return Number(job.price) === Number($scope.filter.cost.amount);

          case 3://more than

            return Number(job.price) > Number($scope.filter.cost.amount);
        }
      }

      if (isFilterActive.cost) {

        /**
         *  Worker function that filters the entirety of the
         * giant job object based on the specified filter
         */
        return function (jobs) {

          var result = {};

          result.all = _.filter(jobs.all, filter);
          result.posted = _.filter(jobs.posted, filter);
          result.freelance = _.filter(jobs.freelance, filter);
          result.posted_to_you = _.filter(jobs.posted_to_you, filter);

          return result;
        };
      }

      //ensure we don't unecessarily call the filter
      return false;
    },
    function filterByDuration() {

      /**
       *  Duration Filter
       *
       *  @param object job to filter
       */
      function filter(job) {

        var durationInDays = Number(job.duration.replace(/\sdays/g, ''));

        switch ($scope.filter.duration.boundary) {

          case 1:// 0 - 24 hours

            return durationInDays <= 1;

          case 2://1 - 3 days

            return durationInDays >= 1 && durationInDays <= 3;

          case 3://3 days - 1 week

            return durationInDays > 3 && durationInDays <= 7;

          case 4://1 week - 2 weeks

            return durationInDays > 7 && durationInDays <= 14;


          case 5://2 weeks - 1 month

            return durationInDays > 14 && durationInDays <= 31;


          case 6://1 month - 3 months

            return durationInDays > 31 && durationInDays <= 93;


          case 7://over 3 months

            return durationInDays > 93;

          default:
            return true;
        }
      }

      if (isFilterActive.duration) {

        //pass the giant job object
        return function (jobs) {

          var result = {};

          result.all = _.filter(jobs.all, filter);
          result.posted = _.filter(jobs.posted, filter);
          result.freelance = _.filter(jobs.freelance, filter);
          result.posted_to_you = _.filter(jobs.posted_to_you, filter);

          return result;

        };
      }

      //ensure we don't unecessarily call the filter
      return false;
    }
  ];

  /**
   *  Do Filter
   *
   *  Triggers and runs all filters on all data models
   *
   *  @param string filter type
   */
  $scope.doFilter = function (type) {

    isFilterActive[type] = true;

    var result = allJobs;

    //run all filters on the grand job object
    for (var index in filters) {

      //retrieve filter function
      var filter = filters[index]();

      //check if the filter is available and run filter
      if (Boolean(filter)) {
        result = filter(result);
      }
    }

    //update jobs view model - chunk into rows of 3
    $scope.allJobsChunked = chunkAndPad(result.all);
    $scope.postedJobsChunked = chunkAndPad(result.posted);
    $scope.freelanceJobsChunked = chunkAndPad(result.freelance);
    $scope.privateJobsChunked = chunkAndPad(result.posted_to_you);
  };

  /**
   * Format Comment
   *
   *  Convert end of line to <br>
   *
   *  @param string raw string
   */
  $scope.formatComment = function (string) {

    return $sce.trustAsHtml($filter('nbsp2br')(string));
  };

  /**
   *  Search for jobs ONLY
   */
  function startSearch() {

    var minLen = 3; //only start search when the length of the query is more than minLen
    if ($scope.query.length >= minLen) {

      $scope.isLoadingJobs = true;

      //check query Cache for results first
      if (Search.getCacheItem($scope.query) !== undefined) {

        $scope.allJobsChunked = $scope.postedJobsChunked = $scope.freelanceJobsChunked = $scope.privateJobsChunked = chunkAndPad(Search.getCacheItem($scope.query).jobs);
        $scope.isLoadingJobs = false;

      } else {

        Search.go($scope.query).then(function (response) {

          $scope.allJobsChunked = $scope.postedJobsChunked = $scope.freelanceJobsChunked = $scope.privateJobsChunked = chunkAndPad(response.data.jobs);

          //cache results
          Search.updateCache($scope.query, response.data);
        }, function (error) {
          $mdToast.show(
            $mdToast.simple().content('We could not find that job!')
              .position('bottom right')
              .hideDelay(2000)
          );
        }).finally(function () {
          $scope.isLoadingJobs = false;
        });
      }
    } else if ($scope.query.length === 0 && _.map(Search.getCache()).length > 0) {

      //if user deletes all of the search query,return full job list to normal
      $scope.allJobsChunked = chunkAndPad(allJobs.all);
      $scope.postedJobsChunked = chunkAndPad(allJobs.posted);
      $scope.freelanceJobsChunked = chunkAndPad(allJobs.freelance);
      $scope.privateJobsChunked = chunkAndPad(allJobs.posted_to_you);
    }
  }

  /**
   *  Post Job
   *
   *  Posts a new Job
   *
   *  @param object new job
   */
  function postJob(job, ev) {

    //pluck relevant details
    
    //pluck relevant details
    var newJob = {};

    newJob.title = job.title;
    newJob.category_id = job.category.id;
    newJob.description = job.description;
    newJob.price = job.price;
    newJob.hasMilestones = job.hasMilestones;
    newJob.skills = _.map(job.skills, 'id');
    newJob.addons = _.map(job.addons, 'id');

    newJob.size_id = job.size.id;
    newJob.size_tier = job.tier.level;

    newJob.posted_to = job.posted_to || 0;
    newJob.coupon = job.coupon;
    newJob.discount = job.discount;
    newJob.website_type = job.website_type;

    if (newJob.website_type == "content_website") {
      newJob.addons.push("5");
    }

    job.isPostingInProgress = true;

    Job.post(newJob).then(function (response) {

      $mdToast.show(
        $mdToast.simple().content('Job posted successfully')
          .position('bottom right')
          .hideDelay(2000)
      );

      $mdDialog.cancel();

      $state.go('app.jobs');
    }, function (error) {

      $mdToast.show(
        $mdToast.simple().content('You have errors on the job posting form')
          .position('bottom right')
          .hideDelay(2000)
      );
    }).finally(function () {
      job.isPostingInProgress = false;
    });

    /**
     * display pop to collect number
     * 
     * @param  $scope            
     * @param  verification_code 
     * @param  newJob
     * @return                   
     */
      // console.dir('display pop up');
      // $mdDialog.cancel();

      // $mdDialog.show({

      //   locals: {newJob: newJob},
      //   controller: function ($scope, newJob) {

      //     $scope.newJob = newJob;
      //     $scope.isPostingInProgress = false;

      //     $scope.sendCode = function (user) {
      //       $scope.isPostingInProgress = true;
            
      //       Job.postNumber('/jobs/generate_token_for_number/', {userPhoneNumber: user.phoneNumber}).then(function (response) {
      //             $scope.isPostingInProgress = false;                  
      //             $mdDialog.cancel();

      //             var verification_code = response.data.token;
      //             console.dir(verification_code);

      //             $mdDialog.show({

      //               locals: {verification_code: verification_code, newJob: $scope.newJob},
      //               controller: function ($scope, verification_code, newJob) {

      //                   $scope.verification_code = verification_code;        
      //                   $scope.newJob = newJob;

      //                   $scope.verifyCode = function (user) {

      //                       $scope.isPostingInProgress = true;
                            
      //                       console.dir($scope.verification_code);
      //                       console.dir('compare code');
      //                       console.dir(user);

      //                       if ($scope.verification_code == user.code) {

      //                           Job.post($scope.newJob).then(function (response) {

      //                             $scope.isPostingInProgress = false;
                                  
      //                             $mdToast.show(
      //                               $mdToast.simple().content('Job posted successfully')
      //                                 .position('bottom right')
      //                                 .hideDelay(2000)
      //                             );

      //                             $mdDialog.cancel();

      //                             $state.go('app.jobs');
      //                           }, function (error) {

      //                             $mdToast.show(
      //                               $mdToast.simple().content('You have errors on the job posting form')
      //                                 .position('bottom right')
      //                                 .hideDelay(2000)
      //                             );

      //                           }).finally(function () {
      //                             job.isPostingInProgress = false;
      //                           });

      //                       } else {
      //                             $mdToast.show(
      //                               $mdToast.simple().content('The verification code is not correct')
      //                                 .position('top right')
      //                                 .hideDelay(2000)
      //                             );
      //                       }

      //                   }

      //                   $scope.close = function () {
      //                     $mdDialog.cancel();
      //                   };

      //               },

      //               templateUrl: 'templates/jobs/get_code.html',
      //               parent: angular.element(document.body),
      //               clickOutsideToClose: true
      //             });

      //       });

      //       //send user phone number and return code
      //     }

      //     $scope.close = function () {
      //       $mdDialog.cancel();
      //     };
      //   },

      //   templateUrl: 'templates/jobs/get_user_phone_number.html',
      //   parent: angular.element(document.body),
      //   clickOutsideToClose: true
      // });

  }


  /**
   *  Chunk And Pad
   *
   *  Chunk the array into sets of 3
   *  and pad the las row to get to 3 if not already at 3.
   *
   *  @param array array to chunk and pad
   */
  function chunkAndPad(dataSet) {

    if (dataSet.length === 0) return dataSet;

    var dataChunked = _.chunk(dataSet, 3);

    var lastRow = dataChunked.length - 1;

    //pad the row, so the jobs on the last row don't stretch
    if (dataChunked[lastRow].length < 3) {

      var padLength = 3 - dataChunked[lastRow].length;

      for (var i = 0; i < padLength; i++) {
        dataChunked[lastRow].push({});
      }
    }

    return dataChunked;
  }

  /**
   *  Create Job
   *
   *  Reveal create job dialog with the
   *  form to create a new job
   *
   *  @param object event
   *  @param object chosen job category
   *  @param string type of category[digital/domestic]
   *  @param object freelancer job is being posted to[optional]
   */
  function createJob(ev, category, freelancer) {

    var type = category.type;
    $scope.job.category = category;

    var modalTemplates = {
      'digital': 'templates/jobs/create-form.html',
      'website': 'templates/jobs/website-form.html',
      'driver': 'templates/jobs/driver-form.html'
    };

    $mdDialog.show({
      locals: {
        job: $scope.job,
        postJob: postJob,
        categories: $scope.categories,
        freelancer: freelancer,
        imageUrl: $scope.imageUrl,
      },
      controller: type === 'digital' ? require('./CreateDigitalJobCtrl.js') : require('./CreateWebsiteJobCtrl.js'),
      templateUrl: modalTemplates[type],
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: false
    });
  }

  /**
   *  Check if this job was assigned to the current user as the freelancer
   */
  $scope.isFreelancer = function (job) {

    if (!Boolean(Auth.getUser()) || !Boolean(job)) {
      return false;
    }
    return Auth.getUser().uid === job.freelancer_id;
  };

  /**
   *  Determine whether to send user into the job view page or
   *  or enforce signature of agreement
   */
   function startJob($event, job) {

    //is this the client logged in?
    if ($scope.belongsToCurrentUser(job)) {

      //has client signed the agreement?
      if (Number(job.client_signed) === 0) {

        showTnC($event, job);
      } else {

        $state.go('app.job', {job: job});
      }
    } else {

      // has the client signed the agreement earlier than freelancer?
      if (Number(job.client_signed) !== 0 && Number(job.freelancer_signed) === 0) {

        showTnC($event, job);
      } else {

        $state.go('app.job', {job: job});
      }
    }
  };

  /**
   *  Show T&C
   *
   *  @param object event
   *  @param object job
   */
   function showTnC (ev, job) {

    $mdDialog.show({

      locals: { currentJob: job, belongsToCurrentUser: $scope.belongsToCurrentUser },
      controller: function ($scope, $mdDialog, $filter, currentJob, belongsToCurrentUser, Job, Auth, Notification, Freelancer, Mailer, Paystack) {

        // Call the job metadata<freelancer,addons and milestones>
        $scope.isLoadingJobMeta = true;

        // Reload the job information
        Job.get($filter('hyphenate')(job.title), 'addons,milestones,categorysize').then(function (job) {

          $scope.currentJob = _.assign(currentJob, job.data);
          $scope.rightNow = new Date();
          $scope.isClient = job.data.client_id === Auth.getUser().uid;

        }, function () {

          $mdToast.show(
            $mdToast.simple()
              .content('There was a problem while retrieving all the required job information.')
              .position('bottom right')
              .hideDelay(2000)
          );
        })
          .finally(function () {
            $scope.isLoadingJobMeta = false;
          });

        $scope.agree = function (job) {

          var element = angular.element(document.getElementById('tnc-container'));
          var content = element[0].innerHTML; 
          var buttons = content.indexOf('<!--Options-->');
          content = content.slice(0, buttons); // buttons to remove the button text at the end of the contract.

          var email = Auth.getUser().email;
          var firstname = Auth.getUser().firstname;
          //send email here
          // Send email notification
          Mailer.sendTemplateMail('generic', 'You have agreed to the Hirefreehands Contract Agreement', email, {
            firstname: firstname,
            body: content,
            notification_url: ""
            //TODO: call_to_action
            //TODO: call_to_action_title
          });

          if (belongsToCurrentUser(job)) {

            // Client signs the agreement and pays upfront.
            Job.signAgreement({job_id: job.id, type: "client"}).then(function () {

              // Close Contract modal
              $mdDialog.cancel();

              // Generate a payment transaction ID on the server
              Paystack.generateTransxId('/jobs/initiate_payment/', {job_id: job.id, country_code: sessionStorage.getItem('user_country')}).then(function (response) {

                //Configure and initialise Paystack for first time client[has no authorization code]
                if (!response.data.hasAuthorizationCode) {

                  Paystack.init({

                    amount: response.data.amount_in_kobo, //amount in Kobo
                    ref: response.data.transaction_ref, //transaction reference generated on the server
                    email: response.data.email, // Client email
                    callback: function (paystackResponse) {
                      verify(paystackResponse);
                    }
                  });

                } else {

                  /**
                   *  this means this customer already has an authorization code
                   *  and payment has been initiated on the server
                   */
                  if (response.data.status) {
                    onVerificationSuccess();
                  } else {

                    alert("An error occured while finalising your payment, please try again...");
                  }
                }

                /**
                 *  Verify the transaction on the server
                 */
                function verify(paystackResponse) {

                  //verify transaction on the server
                  Paystack.verify('jobs/verify_paystack_payment', {data: paystackResponse, country_code: sessionStorage.getItem('user_country')}).then(function (verificationResponse) {
                    // console.dir(verificationResponse);
                    if (verificationResponse.status) {

                      onVerificationSuccess();
                    } else {

                      alert("An error occured while finalising your payment, please try again...");
                    }
                  });
                }

                /**
                 *   Yippe! Transaction successful! Send notification to
                 *   freelancer to get working.
                 */
                function onVerificationSuccess() {

                  Job.setFreelancer({job_id: job.id, freelancer_id: job.assigned_freelancer_id}).then(function (response) {

                    Freelancer.get(job.assigned_freelancer_id).then(function (freelancer) {

                      // Send in-app notification
                      var notification = {
                        message: '<span class="capitalize bold">' + Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> just accepted your bid for the <span class="capitalize bold">"' + job.title + '"</span> job',
                        sender_id: Auth.getUser().uid,
                        sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                        to_id: job.assigned_freelancer_id,
                        type: "CLIENT_ACCEPT_BID",
                        resource_id: $filter('hyphenate')(job.title)
                      };

                      Notification.send(notification);

                      // Send email notification
                      Mailer.sendTemplateMail('bid_freelancer', 'Congratulations! Your Bid Has Been Approved.', freelancer.data.email, {
                        job_title: job.title,
                        firstname: freelancer.data.name.split(' ')[0],
                        lastname: freelancer.data.name.split(' ')[1],
                        start_job_link: '/jobs'
                      });

                      // Proceed to job stream
                      $state.go('app.job', {job: job});
                    });
                  });
                }
              }, function () {

                alert('An error occured while initialising transaction on the server');
              });
            });
          } else {

            Job.signAgreement({job_id: job.id, type: "freelancer"}).then(function () {

              // Close contract modal
              $mdDialog.cancel();

              // Proceed to job stream
              $state.go('app.job', {job: job});
            });
          }
        };

        $scope.cancel = function () {

          $mdDialog.cancel();
        };
      },
      templateUrl: 'templates/jobs/tnc.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  }

  /**
   *  Show Bid History
   *
   *  @param object event
   *  @param object job
   */
  $scope.showBidHistory = function (ev, job, price) {
    console.dir(price);
    //only allow clients who own the job or BONAFIDE VERIFIED freelancers view bids/send
    if (!$scope.belongsToCurrentUser(job) && !Auth.isVerifiedRole('FREELANCER')) {

      showVerificationChecklist();
      return;
    }

    $mdDialog.show({

      locals: {currentJob: job, showTnC: showTnC, imageUrl: $scope.imageUrl, jobPrice: price},
      controller: function ($scope, $filter, currentJob, showTnC, Job, Auth, User, Uploader, imageUrl, Mailer, jobPrice) {

        $scope.currentJob = currentJob;
        $scope.jobPrice = jobPrice;
        $scope.imageUrl = imageUrl;

        // console.dir($scope.jobPrice);

        $scope.close = function () {
          $mdDialog.cancel();
        }

        //initialize bid and bid history
        var BidClass = require('./BidClass.js');
        BidClass($scope, $filter, $sce, $state, $mdDialog, currentJob, showTnC, Job, Auth, User, Notification, Uploader, Mailer);
      },

      templateUrl: 'templates/jobs/bid-history.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  };

  /**
   *  Show Verification Checklist
   *
   *  Initiate verification sequence for the logged in user
   */
  function showVerificationChecklist() {

    $mdDialog.show({
      //locals:{currentJob:job,showTnC: $scope.showTnC},
      controller: function ($scope, $auth, $mdToast, $mdDialog, Auth, Uploader, Freelancer) {

        //hide progress bar
        $scope.showProgress = false;

        //get verification checklist
        $scope.checklist = Auth.getVerificationChecklist();

        //perform signin to linked in as verification activity
        $scope.signinViaLinkedin = function () {

          //send system message encouraging freelancer to continue verification process
          var firstname = Auth.getUser().firstname.substr(0, 1).toUpperCase() + Auth.getUser().firstname.substr(1);
          var message_agent = {
            comment: "Welcome " + firstname + ". You are a few steps away from becoming verified and earning a living on Hirefreehands. To get started, click the button below to verify your LinkedIn Profile",
            posterId: 'SYSTEM',
            nextItem: 'LINKEDIN',
            posterName: 'HFH ADMIN',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().uid,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          //$state.go('app.freelancerVerification');
          
          //Go straight to support
          $state.go('app.support');
        };

        //upload image for means of identification
        $scope.uploadIdentity = function (file) {

          if (file !== null) {

            //save freelancers documents in /freelancers/<freelancerId>/<unique_document_ref>
            var fileExt = file.type.split('/')[1];
            var saveAs = 'freelancers/' + Auth.getUser().uid + '/' + (new Date()).getTime() + '.' + fileExt;

            //call the uploader
            Uploader.upload(file, saveAs)
              .then(function (response) {

                //retrieve the file url
                var url = Uploader.getAssetUrl(response);

                //Save to chat thread for the current logged in prospective freelancer
                var message_freelancer = {
                  attachment: {
                    type: 'IMAGE',
                    url: url
                  },
                  posterId: Auth.getUser().uid,
                  posterName: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                  posted: (new Date()).getTime()
                };

                //send system message encouraging freelancer to continue verification process
                var message_agent = {
                  comment: 'Great! You have successfully uploaded your means of identification and we will go ahead to verify it in a short while.\n In the meantime, please send us your phone number,so we can have a brief chat to verify your credentials.\nThank you.',
                  posterId: 'SYSTEM',
                  posterName: 'HIRE FREE HANDS AGENT',
                  nextItem: 'UPLOAD_PHONE',
                  posted: (new Date()).getTime()
                };

                var posts = [message_agent];

                Freelancer.appendPosts({

                  uid: Auth.getUser().uid,
                  name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                  posts: posts
                });

                //continue verification process
                $state.go('app.freelancerVerification');
              }, function (error) {

                $mdToast.show(
                  $mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
                    .position('bottom right')
                    .hideDelay(2000)
                );

              }, function (evt) {

                //reveal and update upload bar
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.uploadProgress = progressPercentage + '%';
                $scope.showProgress = true;

              }).finally(function () {

              //hide progress bar
              $scope.showProgress = false;
            });
          }
        };

        $scope.uploadPhoneNumber = function () {

          //send system message encouraging freelancer to continue verification process
          var message_agent = {
            comment: "Send us your phone number so we can conduct a quick interview to verify your identity.\nThank you.",
            posterId: 'SYSTEM',
            posterName: 'HIRE FREE HANDS AGENT',
            nextItem: 'UPLOAD_PHONE',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().uid,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          $state.go('app.freelancerVerification');
        };

        $scope.uploadBVN = function () {

          //send system message encouraging freelancer to continue verification process
          var message_agent = {
            comment: "Send us your bank verification number(BVN) so we can further verify your identity.\nThank you.",
            posterId: 'SYSTEM',
            nextItem: 'UPLOAD_BVN',
            posterName: 'HIRE FREE HANDS AGENT',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().uid,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          $state.go('app.freelancerVerification');
        };
      },

      templateUrl: 'templates/jobs/verification-checklist.html',
      parent: angular.element(document.body),
      targetEvent: null,
      clickOutsideToClose: true
    });
  }
  
};
