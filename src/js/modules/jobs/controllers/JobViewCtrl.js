/**
*	Job/Project View Controller
*
*	This controller handles all activities for a specific
*  	job[milestones, comments et al]
*/
module.exports = function($scope,$state,$stateParams,$location,$sce,$filter,$mdToast,$mdDialog,$interval,Notification,Auth,Job,Uploader,Confirm,Mailer){

	'use strict';

  //get the job if it wasn't passed in but has id in the URI
  var currentURI = $location.path();

  //get Job Title
  var jobTitle = $stateParams.jobTitle;


	// Initialize controller defaults when view loads.
	$scope.init = function () {

    //job to view
    $scope.job = $stateParams.job;

    //timer of job to view
    $scope.timer = {days:0,hours:0,mins:0};

    if(($scope.job.id === undefined && jobTitle !== '') || $scope.job.id !== undefined){

      if(!Boolean(jobTitle)){
        jobTitle = $filter('hyphenate')($scope.job.title);
      }

      //get job details as specified by the ID extracted from the URL
      Job.get(jobTitle).then(function(response){

        $scope.job = response.data;

        // make sure the client or freelancer has satisfies all criteria before proceeding
        if ((Number($scope.job.freelancer_id) === 0) ||
          (isJobClient($scope.job) && Number($scope.job.client_signed) === 0)
          /**(isJobFreelancer($scope.job) && Number($scope.job.freelancer_signed) === 0)**/) {

          // TODO: Do this validation onRouteChange

          $location.url('/bids/'+$filter('hyphenate')($scope.job.title));
        }

        if($scope.job === null){

          //send user back to the "BROWSE JOBS" section
          $mdToast.show(
            $mdToast.simple().content('That Job does not exist')
              .position('bottom right')
              .hideDelay(2000)
          );
          $state.go('app.jobs');
        }else{

          //initialize chat history
          initializeChatHistory();

          //initialize project timer
          initializeJobTimer();

          //if job has milestones, run milestone initialization sequence
          if($scope.job.milestones.data.length > 0){
            $scope.job.milestones.data.forEach(function(milestone){

              isMilestoneChecked(milestone,'freelancer');
              isMilestoneChecked(milestone,'client');
            });
          }

          //show welcome message to either freelancer or client if they just signed the agreement (1min window)
          if(isJobFreelancer($scope.job) && ((new Date()).getTime() - ($scope.job.freelancer_signed * 1000) <= 60000)){

            $mdDialog.show({

              controller: function($scope){

                /**
                 *	Close Dialog
                 */
                $scope.closeDialog = function(){$mdDialog.cancel();};
              },
              clickOutsideToClose: true,
              templateUrl: 'templates/jobs/freelancer_welcome.html',
              parent: angular.element(document.body)
            });
          }else if(isJobClient($scope.job) && ((new Date()).getTime() - ($scope.job.client_signed * 1000) <= 60000)){

            $mdDialog.show({

              controller: function($scope){

                /**
                 *	Close Dialog
                 */
                $scope.closeDialog = function(){$mdDialog.cancel();};
              },
              clickOutsideToClose: true,
              templateUrl: 'templates/jobs/client_welcome.html',
              parent: angular.element(document.body)
            });
          }
        }

      },function(){

        //send user back to the "BROWSE JOBS" section
        $mdToast.show(
          $mdToast.simple().content('An error occured while your job was loading')
            .position('bottom right')
            .hideDelay(2000)
        );
        $state.go('app.jobs');
      });

    }else if($scope.job.id === undefined){

      //send user back to the "BROWSE JOBS" section
      $mdToast.show(
        $mdToast.simple().content('No Job Specified')
          .position('bottom right')
          .hideDelay(2000)
      );
      $state.go('app.jobs');
    }

    //let the url reflect the current job title if not already set
    if(currentURI === '/jobs/' || currentURI === '/bids/'){
      $location.path(currentURI+$filter('hyphenate')($scope.job.title));
    }

    $scope.newComment = {

      id: Auth.getUser().uid,
      name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
      chatImageUrl: Auth.getUser().image,
      created: (new Date()).getTime()
    };

    if($scope.job.id !== undefined){
      initializeChatHistory();
    }
	};

  	/**
	* 	Format Comment
	*
	*	Convert end of line to <br>
	*
	*	@param string raw comment
	*/
	$scope.formatComment = function(comment){

		return $sce.trustAsHtml($filter('nbsp2br')(comment));
	};

	/**
	*	Determine whether the current user owns a chat message
	*/
	$scope.ownsPost = function(chat){
		return	chat.id === Auth.getUser().uid;
	};

	/**
	 *  Check if this job belongs to the current logged in user
	 */
	$scope.belongsToCurrentUser = function (job) {

	  if (!Boolean(Auth.getUser()) || !Boolean(job)) {
	    return false;
	  }
	  return Auth.getUser().uid === job.client_id;
	};

	/**
	*	Post Comment
	*
	*	@param object new Bid
	*/
	$scope.post = function(newComment){

		//specify which freelancer this message is associated
		newComment.freelancerId = $scope.activeFreelancerId;

		//prevent empty comments/bids
		if(newComment.comment !== ''){

			//clean comment
			newComment.comment = $filter('cleanContact')(newComment.comment)

			//post comment
			$scope.allChat.$add(newComment);

			//clear text field
			newComment.comment = '';

			var currentJob = $scope.job;
			var rhetoric = 'sent you a new message on the';
			var notification = {

				message: '<span class="capitalize bold">'+Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> ' + rhetoric+' <span class="capitalize bold">"'+ currentJob.title + '"</span> job',
				sender_id: Auth.getUser().uid,
				sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
				to_id: $scope.belongsToCurrentUser(currentJob) ?  $scope.activeFreelancerId : currentJob.client_id,
				type: "NEW_MESSAGE_WORKSTREAM",
				resource_id: $filter('hyphenate')(currentJob.title)
			};

			Notification.send(notification);

			// Decide who the recipient/sender is from job ownership status
			var email = isJobClient(currentJob) ? job.freelancer_email : currentJob.client_email;
			var recipientName = isJobClient(currentJob) ? currentJob.freelancer_full : currentJob.firstname;
			var senderName = isJobClient(currentJob) ? currentJob.freelancer_full : currentJob.firstname;
			var notificationUrl = "/jobs" + jobTitle;

			// Send email notification
			Mailer.sendTemplateMail('notification', 'You have a new notification!', email, {
					job_title: currentJob.title,
					from: senderName,
					firstname: recipientName,
					notification_url: notificationUrl
			});
		}
	};

	/**
  	*	Upload
  	*
  	*	Upload and save file(s) pertaining to the job
  	*
  	*	@param object file
  	*/
    $scope.upload = function (file) {

    	if(file !== null){

    		$scope.showProgress = true;

    		//save job documents in /jobs/<jobId>/<unique_document_ref>
    		var fileTypeData = file.type.split('/'),
    		fileExt = fileTypeData[1],
    		fileType = fileTypeData[0].toUpperCase();

    		var saveAs = 'jobs/'+jobTitle+'/'+(new Date()).getTime()+'.'+fileExt;

				//call the uploader
	    	Uploader.upload(file,saveAs).then(function(response){

				//retrieve the file url
				var url = Uploader.getAssetUrl(response);

				var newPost = {

					id: Auth.getUser().uid,
					attachment: {
						type: fileType,
						url: url
					},
					freelancerId:$scope.activeFreelancerId,
					name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
					chatImageUrl: Auth.getUser().image || null,
					created: (new Date()).getTime()
				};

				//notify client
				var currentJob = $scope.job;
				var rhetoric = 'sent you a new message on the';
				var notification = {

					message: '<span class="capitalize bold">'+Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> ' + rhetoric+' <span class="capitalize bold">"'+ currentJob.title + '"</span> job',
					sender_id: Auth.getUser().uid,
					sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
					to_id: $scope.belongsToCurrentUser(currentJob) ?  $scope.activeFreelancerId : currentJob.client_id,
					type: "NEW_MESSAGE_WORKSTREAM",
					resource_id: $filter('hyphenate')(currentJob.title)
				};

				Notification.send(notification);

				// Decide who the recipient/sender is from job ownership status
				var email = isJobClient(currentJob) ? currentJob.freelancer_email : currentJob.client_email;
				var recipientName = isJobClient(currentJob) ? currentJob.freelancer_full : currentJob.firstname;
				var senderName = isJobClient(currentJob) ? currentJob.freelancer_full : currentJob.firstname;
				var notificationUrl = "/jobs" + jobTitle;

				// Send email notification
				Mailer.sendTemplateMail('notification', 'You have a new notification!', email, {
						job_title: currentJob.title,
						from: senderName,
						firstname: recipientName,
						notification_url: notificationUrl
				});

				//post comment
				$scope.allChat.$add(newPost)
				.then(function(){

					//create job log update
					var log = {

						from_uid:	Auth.getUser().uid,
						to_uid:	Auth.getUser().uid === $scope.job.client_id ? $scope.job.freelancer_id : $scope.job.client_id,
						job_id:	$scope.job.id,
						type: 'ATTACHMENT',
						details: {
							type: fileType,
							url: url
						},
						created: Math.round((new Date()).getTime()/1000)
					};

					Job.createLog(log);
				});

			},function(error){

				$mdToast.show(
			      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
			        .position('bottom right')
			        .hideDelay(2000)
			    );

			},function(evt){

				//reveal and update upload bar
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadProgress = progressPercentage+'%';
				$scope.showProgress = true;

			}).finally(function(){

				//hide progress bar
				$scope.showProgress = false;
			});
		}
    };


    /**
    *	Send Job Update
    *
    *	Used to update the status of the job or
    *	milestone.
    *
    *	0  -> Job is in progress/milestone is approved as done
	*	1  -> Job is in complete/milestone is done
	*	2  -> Job is in awaiting approval/milestone is marked as done by freelancer
	*
    *	@param string job ID
    *	@param string status
    *	@param string milestone ID
    */
    $scope.sendJobUpdate = function(jobId,status,milestoneId){

    	//set job status to complete on the front end
	    var currentStatus = $scope.job.status;
    	$scope.job.status = status;

    	var rhetoric;
    	switch(status){
    		case '0': rhetoric = 'still in progess';break;
    		case '1': rhetoric = 'complete';break;
    		case '2': rhetoric = 'pending approval';break;
    	}

		Job.updateStatus({job_id:jobId,status:status})
		.then(function(response){

			var userFullname = Auth.getUser().firstname + ' ' + Auth.getUser().lastname;

			//send notification
			var notification = {

				message: '<span class="capitalize bold">'+userFullname +'</span> updated the <span class="capitalize bold">"'+ job.title + '"</span> job status '+rhetoric,
				sender_id: Auth.getUser().uid,
				sender_fullname: userFullname,
				to_id: isJobClient($scope.job) ?  $scope.job.freelancer_id : $scope.job.client_id,
				type: "STATUS_UPDATE",
				resource_id: 'jobs/'+$filter('hyphenate')($scope.job.title)
			};

			Notification.send(notification);

			$mdToast.show(
		      	$mdToast.simple().content(response.message)
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		},function(){

			//set job status to complete on the front end
    		$scope.job.status = currentStatus;

			$mdToast.show(
		      	$mdToast.simple().content('An error occured while we tried to update the job\'s status')
		        .position('bottom right')
		        .hideDelay(2000)
		    );
		});
    };

    /**
    *	Toggle Milestone Status
    *
    *	Updates milestone's status
    *
    *	@param object job
    *	@param object milestone
    */
    $scope.toggleMilestoneStatus = function(job,milestone){

    	//set job status to complete on the front end
	    var currentStatus = $scope.job.status;
    	$scope.job.status = status;

    	//determine what status to send
    	var status,rhetoric;
    	if(isJobFreelancer(job) && milestone.isFreelancerChecked){
    		milestone.status = status = '2'; //freelancer marks milestone as done
    		rhetoric = 'If you choose to continue, the client will be notified that you are done with this milestone. Are you sure you want to proceed?';
    	}else if((isJobFreelancer(job) && !milestone.isFreelancerChecked) || (isJobClient(job) && !milestone.isClientChecked)){
    		milestone.status = status = '0'; //freelancer/client marks milestone as undone
    		rhetoric = 'If you choose to continue, the client will be notified that you are NOT done with this milestone. Are you sure you want to proceed?';
    	}else if(isJobClient(job) && milestone.isClientChecked){
    		milestone.status = status = '1'; //client approves milestone as done
    		rhetoric = 'If you choose to continue, the frelancer will be notified that you approved this milestone and the percentage of the total amount of the job\'s worth will be released. Are you sure you want to proceed?';
    	}

    	Confirm.show({

    			heading: 'Confirm Milestone Update',
    			message: rhetoric,
    			options: [

    				{
    					title: 'yes',
	    				classList: 'md-button danger',
	    				action: function(){

					    	//collate data
					    	var data = {job_id:job.id,status:status,milestone_id:milestone.id};

					    	Job.updateStatus(data)
							.then(function(response){

								var userFullname = Auth.getUser().firstname + ' ' + Auth.getUser().lastname;

								//send notification
								var notification = {

									message: '<span class="capitalize bold">'+ userFullname +'</span> updated a milestone in the <span class="capitalize bold">"'+ job.title + '"</span> job',
									sender_id: Auth.getUser().uid,
									sender_fullname: userFullname,
									to_id: isJobClient(job) ?  job.freelancer_id : job.client_id,
									type: "MILESTONE_UPDATE",
									resource_id: 'jobs/'+$filter('hyphenate')(job.title)
								};

								Notification.send(notification);

								// Decide who the recipient is from job ownership status
								var body = userFullname + 'updated a milestone in the '+ job.title +'job';
								var email = isJobClient(job) ? job.freelancer_email : job.client_email;
								var firstname = isJobClient(job) ? job.freelancer_full : job.firstname;
								var notificationUrl = "/jobs" + jobTitle;
								// Send email notification
						        Mailer.sendTemplateMail('generic', 'You have a new milestone update!', email, {
						        	firstname: firstname,
						        	body: body,
						        	notification_url: notificationUrl
						        	//TODO: call_to_action
						        	//TODO: call_to_action_title
						       	});

								$mdToast.show(
							      	$mdToast.simple().content(response.message)
							        .position('bottom right')
							        .hideDelay(2000)
							    );
							},function(){

								//set job status to complete on the front end
					    		$scope.job.status = currentStatus;

								$mdToast.show(
							      	$mdToast.simple().content('An error occured while we tried to update the job\'s status')
							        .position('bottom right')
							        .hideDelay(2000)
							    );
							});

	    					$mdDialog.cancel();
	    				}
    				},
    				{
    					title: 'cancel',
	    				classList: 'md-button',
	    				action: function(){

	    					if(isJobClient(job)){
	    						milestone.isClientChecked = !milestone.isClientChecked;
	    					}else{
	    						milestone.isFreelancerChecked = !milestone.isFreelancerChecked;
	    					}

	    					$mdDialog.cancel();
	    				}
    				}
    			]
    	});
    };

    /**
    *	End Job
    *
    *	Make job inactive
    */
    $scope.endJob = function(){

    	//if this is the freelancer logged in, let them confirm that the job is done
    	if(isJobFreelancer($scope.job)){

    		Confirm.show({

    			heading: 'Confirm Job Completion',
    			message: 'If you choose to continue, the client will be notified that you are done with the job and will also be prompted to approve the job completion. Are you sure you want to proceed?',
    			options: [

    				{
    					title: 'yes',
	    				classList: 'md-button danger',
	    				action: function(){

	    					//update job status to pending
	    					$scope.job.status = '2';

	    					//gather payload
							var data = {
								job_id: $scope.job.id,
								status: $scope.job.status
							};

							Job.updateStatus(data)
							.then(function(response){

								//send notification to client so they can approve job completion
								var userFullname = Auth.getUser().firstname + ' ' + Auth.getUser().lastname;
								var notification = {

									message: '<span class="capitalize bold">'+userFullname +'</span> updated the <span class="capitalize bold">"'+ $scope.job.title + '"</span> job status to pending approval',
									sender_id: Auth.getUser().uid,
									sender_fullname: userFullname,
									to_id: isJobClient($scope.job) ?  $scope.job.freelancer_id : $scope.job.client_id,
									type: "STATUS_UPDATE",
									resource_id: 'jobs/'+$filter('hyphenate')($scope.job.title)
								};

								Notification.send(notification);

								$mdToast.show(
							      	$mdToast.simple().content('Client has been notified of your changes!')
							        .position('bottom right')
							        .hideDelay(2000)
						    	);

							},function(error){

								//reset job status to pending on the front end
								$scope.job.status = '0';

								$mdToast.show(
							      	$mdToast.simple().content('An error occured while we tried to save your review and close the job, please try again.')
							        .position('bottom right')
							        .hideDelay(2000)
							    );
							});

	    					$mdDialog.cancel();
	    				}
    				},
    				{
    					title: 'cancel',
	    				classList: 'md-button',
	    				action: function(){$mdDialog.cancel();}
    				}
    			]
    		});
    	}else{

    		//make sure the client confirms that they actually want to close this job
	    	Confirm.show({

	    		heading: 'Confirm Job Completion',
	    		message: 'Ending this job would mean that you can no longer update/modify or receive updates about the job and the freelancer would be compensated for the task(s) completed. Are you sure you want to proceed?',
	    		options: [
	    			{
	    				title: 'yes',
	    				classList: 'md-button danger',
	    				action: function(){
	    					$mdDialog.show({
	    						locals: {job:$scope.job},
	    						controller: function($scope, $mdToast, job){

	    							$scope.reviewAndCloseJob = function(){

	    								//set job status to complete on the front end
	    								job.status = '1';

	    								//gather payload
	    								var data = {
	    									job_id: job.id,
	    									rating: $scope.rating,
	    									comment: $scope.comment
	    								};

	    								Job.reviewAndCloseJob(data)
	    								.then(function(response){

	    									//send notification to frelancer
											var userFullname = Auth.getUser().firstname + ' ' + Auth.getUser().lastname;
											var notification = {

												message: '<span class="capitalize bold">'+userFullname +'</span> updated the <span class="capitalize bold">"'+ job.title + '"</span> job status to complete',
												sender_id: Auth.getUser().uid,
												sender_fullname: userFullname,
												to_id: isJobClient(job) ? job.freelancer_id : job.client_id,
												type: "STATUS_UPDATE",
												resource_id: 'jobs/'+$filter('hyphenate')(job.title)
											};

											Notification.send(notification);

	    									$mdToast.show(
										      	$mdToast.simple().content('Your review has been saved and the job was closed successfully')
										        .position('bottom right')
										        .hideDelay(2000)
										    );

	    								},function(error){

	    									//reset job status to pending on the front end
	    									$scope.job.status = '0';

	    									$mdToast.show(
										      	$mdToast.simple().content('An error occured while we tried to save your review and close the job, please try again.')
										        .position('bottom right')
										        .hideDelay(2000)
										    );
	    								});

	    								$mdDialog.cancel();
	    							};
	    						},
								templateUrl: 'templates/jobs/review.html',
						      	parent: angular.element(document.body),
						      	clickOutsideToClose:true
	    					});
	    				}
	    			},
	    			{
	    				title: 'cancel',
	    				classList: 'md-button',
	    				action: function(){$mdDialog.cancel();}
	    			}
	    		]
	    	});
	    }
    };

    $scope.isJobFreelancer = isJobFreelancer;
    $scope.isJobClient = isJobClient;

  /**
   *	Is Milestone Checked
   *
   *	Evaluate if milestone should be marked as checked/unchecked
   *	and return object that can be bound to the checkbox model
   *
   *	@param object milestone
   *	@param string who checkbox belongs to
   *	@return object flag for checkbox to bind to model
   */
  function isMilestoneChecked(milestone,type){

    //collate scenarios
    var freelancerMarkedAsDone = milestone.status === '2';
    var clientApproved = milestone.status === '1';
    var unApproved = milestone.status === '0';

    if(type === 'freelancer' && (freelancerMarkedAsDone || clientApproved)){

      milestone.isFreelancerChecked = true;
    }else if(type === 'freelancer' && unApproved){

      milestone.isFreelancerChecked = false;
    }else if(type === 'client' && (freelancerMarkedAsDone || unApproved)){

      milestone.isClientChecked = false;
    }else if(type === 'client' && clientApproved){

      milestone.isClientChecked = true;
    }
  }

    /**
    *	Initialize Job Timer
    */
    function initializeJobTimer(){

    	//get job expiry date in epoch milliseconds[javascript is in milliseconds and php seconds]
    	var expiryDate = $scope.job.expires * 1000,//(new Date('May 17 2016').getTime()),
    	counterFunction = function(){

				var now = (new Date()).getTime();

				//only start timer after freelancer has signed agreement
    		if(now < expiryDate && $scope.job.freelancer_signed > 0){

	    		//evaluate time difference
	    		var diff = expiryDate - now;

	    		//convert time diff to seconds
	    		diff /= 1000;

	    		//get the amount of days
	    		var secondsInADay = (3600 * 24),
		    		days = diff / secondsInADay,
		    		hours = (days - Math.floor(days)) * 24,//remainder fraction of a day(24hrs)
		    		mins = (hours - Math.floor(hours)) * 60;//remainder fraction of an hour(60mins)

	    		$scope.timer.days = Math.floor(days);
	    		$scope.timer.hours = Math.floor(hours);
	    		$scope.timer.mins = Math.floor(mins);

	    	}else{
	    		$interval.cancel(counter);
	    	}
    	};

	    //initialize the timer
	    counterFunction();

			//update values every minute
	    var	counter = $interval(counterFunction,60000);
    }

    /**
    *	Is Job Freelancer
    *
    *	@param object job
    */
    function isJobFreelancer(job){

    	return Auth.getUser().uid === job.freelancer_id;
    }

    /**
    *	Is Job Client
    *
    *	@param object job
    */
    function isJobClient(job){

    	return Auth.getUser().uid === job.client_id;
    }

    /**
	* 	makeActive
	*
	*	Make the chat history active for a particular freelancer
	*
	*	@param string freelancer id
	*/
	function makeActive(freelancerId){

		$scope.chatHistory = $scope.groupedChat[freelancerId];
	}

	/**
	*	Initialize Chat History
	*/
	function initializeChatHistory(){

		//retrieve job chat history IFF job is specified
		$scope.isLoading = true;
		$scope.activeFreelancerId = $scope.job.freelancer_id;
		$scope.allChat = Job.getBids($scope.job);

		$scope.allChat.$loaded(function(response){

			$scope.isLoading = false;

			if(response.length > 0){

				$scope.groupedChat = _.groupBy($scope.allChat,'freelancerId');
				makeActive($scope.activeFreelancerId);
			}
		});

		//listen on changes to job bids/chat
		Job.getBidsRef($scope.job).on('child_added',function(bid){

			$scope.allChat.$loaded(function(response){

				//recrunch the chat
				$scope.groupedChat = _.groupBy($scope.allChat,'freelancerId');

				if(response.length > 0){

					$scope.groupedChat = _.groupBy($scope.allChat,'freelancerId');

					makeActive($scope.activeFreelancerId);
				}

			});
		});
	}
};
