module.exports = function ($state, $scope, $timeout, $mdDialog, job, postJob, categories, freelancer, imageUrl, Job, $filter) {

  'use strict';

  // Hold coupons internally.
  var coupons = [];

  $scope.job = job;
  $scope.job.moreSkills = [];
  $scope.job.coupon = null;
  $scope.job.discount = 0;

  $scope.postJob = postJob;
  $scope.imageUrl = imageUrl;

  $scope.addMoreSkills = false;
  $scope.toggleAddMoreSkills = toggleAddMoreSkills;
  $scope.querySkills = querySkills;
  $scope.evaluateJobPrice = evaluateJobPrice;
  $scope.customizeJobPrice = customizeJobPrice;
  $scope.resetJobPrice = resetJobPrice;

  $scope.getAllCategories = getAllCategories;
  $scope.showAllCategories = showAllCategories;
  $scope.effectCoupon = effectCoupon;

  $scope.addAddon = addAddon;
  $scope.processWebsiteType = processWebsiteType;
  $scope.closeJobPostingDialog = closeJobPostingDialog;

  $scope.websiteTypes = [
    { value: 'company_and_personal_website', name: 'Company / Product website'},
    { value: 'content_website', name: 'Content website'},
    { value: 'personal', name: 'Personal / Brand website'},
  ]

  // monitor for changes to the milestone option
  $scope.$watch('job.hasMilestones', function () {

    removeSelectedAddons();
    toggleMilestones();
  });

  //monitor for changes to the milestone option
  $scope.$watch('job.website_type', function () {
      $scope.job.size = undefined;
      $scope.job.tier = undefined;
      $scope.job.price = undefined;
  });


  var allSkills = [];
  if (!freelancer) {

    // get an aggregate of all skills, this is used when user searches for more skills
    _.forEach(categories.digital, function (category) {
      allSkills = allSkills.concat(category.skills.data);
    });

    // make the skills assigned to this category be selected by default
    $scope.job.skills = $scope.job.category.skills.data;

    // Limit pre-selected skills to 5 and preserve their states.
    $scope.job._skills = $scope.job.skills = $scope.job.skills.slice(0, 5);
  } else {

    $scope.freelancer = freelancer;
    $scope.job.posted_to = freelancer.uid;

    Job.getSkills().then(function (skills) {
      allSkills = skills.data;
    });
  }

  Job.getAllCoupons().then(function (response) {
    coupons = response.data;
  });

  /**
   *  Get all categories
   */
  function getAllCategories() {

    return Job.getAllCategories().then(function (response) {
      $scope.categories = response.data;
    });
  }

  /**
   *  Show all categories
   *
   *  Used to decide when to show the full list
   *  of categories as opposed to a preselected option
   */
  function showAllCategories() {

    return !!freelancer;
  }

  /**
   *  Search for skills.
   */
  function querySkills(query) {

    return query ? allSkills.filter(createFilterFor(query)) : allSkills;
  }

  /**
   *  Create filter function for a query string
   */
  function createFilterFor(query) {

    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(skill) {
      return skill.name.toLowerCase().indexOf(lowercaseQuery) !== -1;
    };
  }

  /**
   *  Toggle Add More Skills
   */
  function toggleAddMoreSkills() {

    $scope.addMoreSkills = !$scope.addMoreSkills;
  }

  /**
   * Discount job price if coupon is valid, else, error out.
   */
  function effectCoupon(jobCoupon) {

    $scope.job.discount = 0;
    $scope.couponExistsError = false;

    var validCoupon = couponExists(jobCoupon, coupons);

    if (validCoupon) {
      $scope.job.discount = $scope.job.price - (validCoupon.weight/100 * $scope.job.price);
    } else {
      $scope.couponExistsError = true;
    }
  }

  /**
   * Check for coupon existence.
   *
   * @param jobCoupon
   * @param coupons
   * @returns {*}
   */
  function couponExists(jobCoupon, coupons) {
    return _.find(coupons, function (coupon) {
      return jobCoupon.toUpperCase() === coupon.code.toUpperCase();
    });
  }

  /**
   * Evaluate current job price
   * based on it's surrounding factors.
   * 
   * @param job - entire job object
   */
  function evaluateJobPrice(job) {

    var tierBasePrice = job.tier ? Number(job.tier.price_point) : 0; //duration_of_job
    var selectedAddonsTotalPrice = 0;
    var milestonesTotalPrice = 0;

    switch (true) {

      case job.addons.length > 0:

        job.addons.forEach(function (addon) {
          selectedAddonsTotalPrice += tierBasePrice * (Number(addon.weight / 100));
        });

        break;

      case job.hasMilestones === '1':

        milestonesTotalPrice = tierBasePrice * Number(job.category.milestone_weight / 100);
        break;

      default:

        $scope.job.price = tierBasePrice;
        return;
    }

    $scope.job.price = tierBasePrice + selectedAddonsTotalPrice + milestonesTotalPrice;
  }

  /**
   *  Customize Job Price
   *
   *  Either increase or decrease base tier price by 10% max
   *  in steps of 1000
   *
   * @param direction
   */
  function customizeJobPrice(direction) {

    var tierBasePrice = job.tier ? Number($scope.job.tier.price_point) : 0;
    var currentJobPrice = Number($scope.job.price);
    var upperLimit = tierBasePrice + (tierBasePrice * 0.1);
    var lowerLimit = tierBasePrice - (tierBasePrice * 0.1);

    //steps to increase or decrease by
    var step = 1000;

    if (direction === 'increase' && currentJobPrice + step <= upperLimit) {

      $scope.job.price = currentJobPrice + step;

    } else if (direction === 'decrease' && currentJobPrice - step > lowerLimit) {

      $scope.job.price = currentJobPrice - step;

    } else if (currentJobPrice - step < lowerLimit) {

      alert('You cannot reduce the amount past this point');

    } else if (currentJobPrice + step > upperLimit) {

      alert('You cannot increase the amount past this point');
    }
  }

  /**
   * Handle the process of handling website type
   * 
   * @return 
   */
  function processWebsiteType() {
    $scope.preselectCMS = false;

    if ($scope.job.website_type == "content_website") {
      $scope.preselectCMS = true;
    }
  }

  /**
   *  Add an add on
   *
   *  @param object addon
   */
  function addAddon(addon) {

    var addonExists = _.find($scope.job.addons, function (ad) { return addon.id === ad.id; }); //find if the addon actually exists 
    var tierBasePrice = Number($scope.job.tier.price_point);

    $scope.job.price = Number($scope.job.price);
    addon.weight = Number(addon.weight);

    //check if modify existing job

    if (!Boolean(addonExists)) {

      $scope.job.addons.push(addon);

      /**
       * Check for the case of when user check modify_existing_website addon
       * The addon.id for modify_exisiting_site addon is 44
       * 
       * @param addon.id 
       * @return
       */
      if (addon.id == "44") {
          $scope.job.price -= tierBasePrice * (addon.weight / 100);
          return true;
      }

      //modify the current job evaluated price
      $scope.job.price += tierBasePrice * (addon.weight / 100);
      return true;

    } else {

      _.remove($scope.job.addons, function (ad) {

        if (addon.id === ad.id) {

        /**
         * Check for the case of when user check modify site addon
         * The addon.id for modify_exisiting_site addon is 44
         * 
         * @param addon.id 
         * @return
         */
          if (addon.id == "44") {
              $scope.job.price += tierBasePrice * (addon.weight / 100);
              return true;
          }

          //modify the current job evaluated price
          $scope.job.price -= tierBasePrice * (addon.weight / 100);
          return true;
        }
      });
    }
  }

  /**
   *  Add Milestones
   *
   *  Increases evaluated price by the given
   *  categories milestone weight value
   */
  function toggleMilestones() {

    //if milestones have been selected, increase evaluated job price
    if ($scope.job.price) {

      var milestoneWeight = Number($scope.job.category.milestone_weight);
      var tierBasePrice = job.tier ? Number($scope.job.tier.price_point) : 0;

      $scope.job.price = Number($scope.job.price);

      if ($scope.job.hasMilestones === '0') {

        $scope.job.price -= tierBasePrice * (milestoneWeight / 100);
      } else {
        $scope.job.price += tierBasePrice * (milestoneWeight / 100);
      }
    }
  }

  /**
   * Remove all selected addons
   */
  function removeSelectedAddons() {

    var selectedAddons = $scope.job.addons;
    var tierBasePrice = job.tier ? Number($scope.job.tier.price_point) : 0;

    if (selectedAddons.length > 0) {

      $scope.job.addons = selectedAddons.map(function (addon) {

        addon.isChecked = false;
        $scope.job.price -= tierBasePrice * (Number(addon.weight) / 100);

        return addon;
      });

      $scope.job.addons = [];
    }
  }

  /**
   * Reset just the job's `price` binding.
   */
  function resetJobPrice() {
    if ($scope.job.price) {
      $scope.job.price = undefined;
    }
  }

  /**
   * Reset form bindings.
   */
  function resetFormValues() {

    $scope.job.title = undefined;
    $scope.job.description = undefined;
    $scope.job.price = undefined;
    $scope.job.size = undefined;
    $scope.job.tier = undefined;
    $scope.job.hasMilestones = '0';
    $scope.job.website_type = undefined;
    $scope.job.skills = $scope.job._skills;

    $scope.job.addons = [];

    $scope.createJobForm.$setPristine();
  }

  /**
   * Close job posting dialog
   * and reset the form within it.
   */
  function closeJobPostingDialog() {
    resetFormValues();
    removeSelectedAddons();
    $mdDialog.cancel();
    $state.reload();
  }

};
