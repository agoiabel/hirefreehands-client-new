/**
 *  Projects Controller
 *
 *  This controller handles all the job index
 */
module.exports = function ($scope, $rootScope, $mdDialog, $localStorage, $mdToast, $state, $stateParams, $location, $q, $sce, $filter, Notification, Job, Freelancer, Auth, Search, Paystack) {

  'use strict';

  $scope.isLoadingJobs = true;
  $scope.belongsToCurrentUser = belongsToCurrentUser;
  $scope.showBidHistory = showBidHistory;
  $scope.startJob = startJob;

  /**
   *  Chunk And Pad
   *
   *  Chunk the array into sets of 3
   *  and pad the las row to get to 3 if not already at 3.
   *
   *  @param array array to chunk and pad
   */
  function chunkAndPad(dataSet) {

    if (dataSet.length === 0) return dataSet;

    var dataChunked = _.chunk(dataSet, 3);

    var lastRow = dataChunked.length - 1;

    //pad the row, so the jobs on the last row don't stretch
    if (dataChunked[lastRow].length < 3) {

      var padLength = 3 - dataChunked[lastRow].length;

      for (var i = 0; i < padLength; i++) {
        dataChunked[lastRow].push({});
      }
    }

    return dataChunked;
  }

  /**
   *  Show Verification Checklist
   *
   *  Initiate verification sequence for the logged in user
   */
  function showVerificationChecklist() {

    $mdDialog.show({
      //locals:{currentJob:job,showTnC: $scope.showTnC},
      controller: function ($scope, $auth, $mdToast, $mdDialog, Auth, Uploader, Freelancer) {

        //hide progress bar
        $scope.showProgress = false;

        //get verification checklist
        $scope.checklist = Auth.getVerificationChecklist();

        //perform signin to linked in as verification activity
        $scope.signinViaLinkedin = function () {

          //send system message encouraging freelancer to continue verification process
          var firstname = Auth.getUser().firstname.substr(0, 1).toUpperCase() + Auth.getUser().firstname.substr(1);
          var message_agent = {
            comment: "Welcome " + firstname + ". You are a few steps away from becoming verified and earning a living on Hirefreehands. To get started, click the button below to verify your LinkedIn Profile",
            posterId: 'SYSTEM',
            nextItem: 'LINKEDIN',
            posterName: 'HFH ADMIN',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().id,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          //$state.go('app.freelancerVerification');
          
          //Go straight to support
          $state.go('app.support');
        };

        //upload image for means of identification
        $scope.uploadIdentity = function (file) {

          if (file !== null) {

            //save freelancers documents in /freelancers/<freelancerId>/<unique_document_ref>
            var fileExt = file.type.split('/')[1];
            var saveAs = 'freelancers/' + Auth.getUser().uid + '/' + (new Date()).getTime() + '.' + fileExt;

            //call the uploader
            Uploader.upload(file, saveAs)
              .then(function (response) {

                //retrieve the file url
                var url = Uploader.getAssetUrl(response);

                //Save to chat thread for the current logged in prospective freelancer
                var message_freelancer = {
                  attachment: {
                    type: 'IMAGE',
                    url: url
                  },
                  posterId: Auth.getUser().uid,
                  posterName: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                  posted: (new Date()).getTime()
                };

                //send system message encouraging freelancer to continue verification process
                var message_agent = {
                  comment: 'Great! You have successfully uploaded your means of identification and we will go ahead to verify it in a short while.\n In the meantime, please send us your phone number,so we can have a brief chat to verify your credentials.\nThank you.',
                  posterId: 'SYSTEM',
                  posterName: 'HIRE FREE HANDS AGENT',
                  nextItem: 'UPLOAD_PHONE',
                  posted: (new Date()).getTime()
                };

                var posts = [message_agent];

                Freelancer.appendPosts({

                  uid: Auth.getUser().uid,
                  name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                  posts: posts
                });

                //continue verification process
                $state.go('app.freelancerVerification');
              }, function (error) {

                $mdToast.show(
                  $mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
                    .position('bottom right')
                    .hideDelay(2000)
                );

              }, function (evt) {

                //reveal and update upload bar
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.uploadProgress = progressPercentage + '%';
                $scope.showProgress = true;

              }).finally(function () {

              //hide progress bar
              $scope.showProgress = false;
            });
          }
        };

        $scope.uploadPhoneNumber = function () {

          //send system message encouraging freelancer to continue verification process
          var message_agent = {
            comment: "Send us your phone number so we can conduct a quick interview to verify your identity.\nThank you.",
            posterId: 'SYSTEM',
            posterName: 'HIRE FREE HANDS AGENT',
            nextItem: 'UPLOAD_PHONE',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().id,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          $state.go('app.freelancerVerification');
        };

        $scope.uploadBVN = function () {

          //send system message encouraging freelancer to continue verification process
          var message_agent = {
            comment: "Send us your bank verification number(BVN) so we can further verify your identity.\nThank you.",
            posterId: 'SYSTEM',
            nextItem: 'UPLOAD_BVN',
            posterName: 'HIRE FREE HANDS AGENT',
            posted: (new Date()).getTime()
          };

          var posts = [message_agent];

          Freelancer.appendPosts({

            uid: Auth.getUser().id,
            name: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
            posts: posts
          });

          //close dialog
          $mdDialog.cancel();

          //continue verification process
          $state.go('app.freelancerVerification');
        };
      },

      templateUrl: 'templates/jobs/verification-checklist.html',
      parent: angular.element(document.body),
      targetEvent: null,
      clickOutsideToClose: true
    });
  }

  /**
   *  Show T&C
   *
   *  @param object event
   *  @param object job
   */
  function showTnC (ev, job) {
    $mdDialog.show({

      locals: { currentJob: job, belongsToCurrentUser: $scope.belongsToCurrentUser },
      controller: function ($scope, $mdDialog, $filter, currentJob, belongsToCurrentUser, Job, Auth, Notification, Freelancer, Mailer, Paystack) {

        // Call the job metadata<freelancer,addons and milestones>
        $scope.rightNow = new Date();
        $scope.isClient = currentJob.client_id === Auth.getUser().id;
        $scope.currentJob = currentJob;

        $scope.agree = function (job) {

          var element = angular.element(document.getElementById('tnc-container'));
          var content = element[0].innerHTML; 
          var buttons = content.indexOf('<!--Options-->');
          content = content.slice(0, buttons); // buttons to remove the button text at the end of the contract.

          var email = Auth.getUser().email;
          var firstname = Auth.getUser().firstname;
          //send email here
          // Send email notification
          Mailer.sendTemplateMail('generic', 'You have agreed to the Hirefreehands Contract Agreement', email, {
            firstname: firstname,
            body: content,
            notification_url: ""
            //TODO: call_to_action
            //TODO: call_to_action_title
          });

          if (belongsToCurrentUser(job)) {

            // Client signs the agreement and pays upfront.
            Job.signAgreement({job_id: job.id, type: "client"}).then(function (response) {

              var job_milestone_id = response.jobMilestone.id;
              var country_code = sessionStorage.getItem('user_country');

              // Close Contract modal
              $mdDialog.cancel();

              // Generate a payment transaction ID on the server
              Paystack.generateTransxId('/initiate_payment/', {job_id: job.id, country_code: country_code, job_milestone_id: job_milestone_id}).then(function (response) {

                //Configure and initialise Paystack for first time client[has no authorization code]

                Paystack.init({
                  amount: response.data.amount_in_kobo, //amount in Kobo
                  ref: response.data.ref, //transaction reference generated on the server
                  email: response.data.email, // Client email
                  callback: function (paystackResponse) {
                    verify(paystackResponse);
                  }
                });

                /**
                 *  Verify the transaction on the server
                 */
                function verify(paystackResponse) {

                  //verify transaction on the server
                  Paystack.verify('/verify_payment', {paystackResponse: paystackResponse, country_code: country_code, job_milestone_id: job_milestone_id})
                          .then(function (response) {
                              if (job.category.require_only_one_freelancer == '1') {
                                verificationSuccess();
                                console.dir(response);
                                return;
                              }
                              console.dir('payment logic for more than one freelancer');

                  }, function (error) {
                    error('transaction not successful');
                  });


                }

                /**
                 *   Yippe! Transaction successful! Send notification to
                 *   freelancer to get working.
                 */
                function verificationSuccess() {

                  Job.setFreelancer({job_id: job.id, freelancer_id: job.assigned_freelancer_id}).then(function (response) {

                    Freelancer.get(job.assigned_freelancer_id).then(function (freelancer) {

                      // Send in-app notification
                      var notification = {
                        message: '<span class="capitalize bold">' + Auth.getUser().firstname + ' ' + Auth.getUser().lastname + '</span> just accepted your bid for the <span class="capitalize bold">"' + job.title + '"</span> job',
                        sender_id: Auth.getUser().id,
                        sender_fullname: Auth.getUser().firstname + ' ' + Auth.getUser().lastname,
                        to_id: job.assigned_freelancer_id,
                        type: "CLIENT_ACCEPT_BID",
                        resource_id: $filter('hyphenate')(job.title)
                      };

                      Notification.send(notification);

                      // Send email notification
                      Mailer.sendTemplateMail('bid_freelancer', 'Congratulations! Your Bid Has Been Approved.', freelancer.data.email, {
                        job_title: job.title,
                        firstname: freelancer.data.name.split(' ')[0],
                        lastname: freelancer.data.name.split(' ')[1],
                        start_job_link: '/jobs'
                      });

                      // Proceed to job stream
                      $state.go('app.job', {job: job});
                    });
                  });
                }


              }, function () {

                alert('An error occured while initialising transaction on the server');
              });

            });
          } else {

            Job.signAgreement({job_id: job.id, type: "freelancer"}).then(function () {

              // Close contract modal
              $mdDialog.cancel();

              // Proceed to job stream
              $state.go('app.job', {job: job});
            });
          }

        };

        $scope.cancel = function () {

          $mdDialog.cancel();
        };
      },
      templateUrl: 'templates/jobs/tnc.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: true
    });
  }

  //the first thing is to load all jobs
  Job.getAll().then(function (response) {

    //initialize giant model
    var allJobs = response.jobs;
    //chunk into rows of 3
    $scope.allJobsChunked = chunkAndPad(allJobs);

    console.dir(allJobs);

    }, function () {

    $mdToast.show(
      $mdToast.simple().content('Error while retrieving jobs')
        .position('bottom right')
        .hideDelay(2000)
    );

    }).finally(function () {
    $scope.isLoadingJobs = false;
  });

  /**
   *  Check if this job belongs to the current logged in user
   */
  function belongsToCurrentUser (job) {
    if (!Boolean(Auth.getUser()) || !Boolean(job)) {
      return false;
    }
    
    return Auth.getUser().id === job.client_id;
  };

  /**
   * chose bid history
   * 
   * @param ev 
   * @param job
   * @return    
   */
  function showBidHistory(ev, job) {

      //only allow clients who own the job or BONAFIDE VERIFIED freelancers view bids/send
      //i should work on guard

      $mdDialog.show({

        locals: {currentJob: job, showTnC: showTnC, imageUrl: $scope.imageUrl},
        controller: function ($scope, $filter, currentJob, showTnC, Job, Auth, User, Uploader, imageUrl, Mailer) {

          $scope.currentJob = currentJob;
          $scope.imageUrl = imageUrl;

          console.dir($scope.currentJob);

          $scope.close = function () {
            $mdDialog.cancel();
          }

          //initialize bid and bid history
          var BidClass = require('./BidClass.js');

          BidClass($scope, $filter, $sce, $state, $mdDialog, currentJob, showTnC, Job, Auth, User, Notification, Uploader, Mailer);
        },

        templateUrl: 'templates/jobs/bid-history.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true
      });
  }

  /**
   * Either 
   * @param  {[type]} $event [description]
   * @param  {[type]} job    [description]
   * @return {[type]}        [description]
   */
  function startJob($event, job) {

    //


    //is this the client logged in?
    // if ($scope.belongsToCurrentUser(job)) {

    //   //has client signed the agreement?
    //   if (Number(job.client_signed) === 0) {

    //     showTnC($event, job);
    //   } else {

    //     $state.go('app.job', {job: job});
    //   }
    // } else {

    //   // has the client signed the agreement earlier than freelancer?
    //   if (Number(job.client_signed) !== 0 && Number(job.freelancer_signed) === 0) {

    //     showTnC($event, job);
    //   } else {

    //     $state.go('app.job', {job: job});
    //   }
    // }

  }

};

