module.exports = function ($scope, $state, Job, $filter) {

  $scope.currentQuestion = 1;

  $scope.job = {};

  //fetch the category with the all its dependency
  Job.getCategory(5).then(function (response) {
  	// console.dir(response);
  	$scope.job.category = response.category;
  });

  /**
   * Display option for that of application
   * 
   * @type 
   */
  $scope.application_types = [
  	{
  		name: 'medical record-keeping',
  		type: 'marketplace'
  	},
  	{
  		name: 'symptom',
  		type: 'marketplace'
  	},
  	{
  		name: 'health tracking',
  		type: 'marketplace'
  	},
  	{
  		name: 'job search resource',
  		type: 'marketplace'
  	},
  	{
  		name: 'customer resource management',
  		type: 'marketplace'
  	},

  	{
  		name: 'text messaging application',
  		type: 'social networking'
  	},
  	{
  		name: 'voice messaging',
  		type: 'social networking'
  	},
  	{
  		name: 'video messaging',
  		type: 'social networking'
  	},
  	{
  		name: 'dating',
  		type: 'social networking'
  	},
  	{
  		name: 'photo and video sharing',
  		type: 'social networking'
  	},

  	{
  		name: 'task management',
  		type: 'tooling'
  	},
  	{
  		name: 'calender management',
  		type: 'tooling'
  	},
  	{
  		name: 'translation',
  		type: 'tooling'
  	},
  	{
  		name: 'note taking',
  		type: 'tooling'
  	},
  	{
  		name: 'password management',
  		type: 'tooling'
  	}  	  	
  ];

  /**
   * you will have to change this answer_option to chose the right answer for each questions
   * 
   * @type 
   */
  $scope.questions = [
    {
      question: 1,
      template: "templates/jobs/hfhe/question_1.html",
    },
    {
      question: 2,
      template: "templates/jobs/hfhe/question_2.html",
    },   
    {
      question: 3,
      template: "templates/jobs/hfhe/question_3.html",
    },
    {
      question: 4,
      template: "templates/jobs/hfhe/question_4.html",
    },
    {
      question: 5,
      template: "templates/jobs/hfhe/question_5.html",
    },
    {
      question: 6,
      template: "templates/jobs/hfhe/question_6.html",
    },
    {
      question: 7,
      template: "templates/jobs/hfhe/question_7.html",
    }  
  ];


	/**
	* This is use to get the initial template
	* 
	* @return 
	*/
	$scope.getQuestionTemplate = function() {
		for (var i = 0; i < $scope.questions.length; i++) {
		  if ($scope.currentQuestion == $scope.questions[i].question) {
		    return $scope.questions[i].template;
		  }
		}
	}

	/**
	 * Handle the process of going to next question
	 * 
	 * @param newQuestion 
	 * @return
	 */
	$scope.gotoNextQuestion = function(newQuestion) {

		if (typeof newQuestion === "undefined") {

			var currentQuestion = $scope.currentQuestion;

			//get the current question
			for (var i = 0; i < $scope.questions.length; i++) {
				if ($scope.questions[i].question == currentQuestion) {
					var currentQuestion = $scope.questions[i];
					var currentQuestionKey = i;
				}
			}

			var nextQuestionKey = currentQuestionKey + 1;
			return $scope.gotoNextQuestion($scope.questions[nextQuestionKey].question);
		}

		return $scope.currentQuestion = newQuestion;
	}

	/**
	 * Handle the process of going to previous question
	 * 
	 * @param newQuestion 
	 * @return
	 */
	$scope.gotoPreviousQuestion = function(newQuestion) {

		if (typeof newQuestion === "undefined") {

			var currentQuestion = $scope.currentQuestion;

			//get the current question
			for (var i = 0; i < $scope.questions.length; i++) {
				if ($scope.questions[i].question == currentQuestion) {
					var currentQuestion = $scope.questions[i];
					var currentQuestionKey = i;
				}
			}

			var nextQuestionKey = currentQuestionKey - 1;
			return $scope.gotoNextQuestion($scope.questions[nextQuestionKey].question);
		}

		return $scope.currentQuestion = newQuestion;
	}

	/**
	 * check if its the result page
	 * 
	 * @return 
	 */
	$scope.resultPage = function () {
		if ($scope.currentQuestion == 10) {
			return true;
		}
		return false;
	}

	/**
	 * Handle the process of setting scope features
	 * 
	 * @param 
	 */
	$scope.setScopeFeaturesFor = function (scope) {
		$scope.features = {};
		$scope.features = scope.scope_features;

		console.dir($scope.features);
	}

	


	$scope.selected = [];
	$scope.toggleFeature = function (item, list) {

		var idx = list.indexOf(item);
		if (idx > -1) {
		  list.splice(idx, 1);
		}
		else {
		  list.push(item);
		}
	};

	$scope.exists = function (item, list) {
		return list.indexOf(item) > -1;
	};


	$scope.submit = function () {
		console.dir('submitted');
	}
};
