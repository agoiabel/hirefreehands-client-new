module.exports = function ($scope, $mdDialog) {

  $scope.closeJobPostingDialog = closeJobPostingDialog;

  /**
   * Close job posting dialog
   * and reset the form within it.
   */
  function closeJobPostingDialog() {
    resetFormValues();
    removeSelectedAddons();
    $mdDialog.cancel();
    $state.reload();
  }
};
