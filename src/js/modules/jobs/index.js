/**
*	Jobs/Projects Module Definition
*/

require('../angular-paystack');

//require the dependencies
var angular = require('angular');

//define the Jobs/Projects module
var jobModule = angular.module('jobModule',['angularPaystack']);

jobModule.run(function($rootScope, $localStorage, $mdDialog, Paystack){

	'use strict';
	
	//pk_live_f0dca9c8897514f405b456499c6f2dc73f4cf7f0
	Paystack.setPublicKey('pk_test_07fb1bad94dacd914ae1fc90c8ec9678f0832785'); // pk_test_07fb1bad94dacd914ae1fc90c8ec9678f0832785

});

//define controllers
jobModule.controller('JobCtrl',require('./controllers/JobCtrl.js'));

jobModule.controller('JobCreateCtrl',require('./controllers/JobCreateCtrl.js'));
jobModule.controller('JobCreateHfheCtrl',require('./controllers/JobCreateHfheCtrl.js'));
jobModule.controller('JobIndexCtrl', require('./controllers/JobIndexCtrl.js'));

jobModule.controller('JobViewCtrl',require('./controllers/JobViewCtrl.js'));
jobModule.controller('BidCtrl',require('./controllers/BidCtrl.js'));

//define services
jobModule.service('Job',require('./services/Job.js'));

//define directives
jobModule.directive('jobsFilter',require('./directives/jobsFilter.js'));