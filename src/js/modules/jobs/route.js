/**
 * Route definitions for the jobs Module
 */
module.exports = {
	
	jobview: {
		url: "/jobview",		
		views:{
			"app-sub-view":{
				controller: "JobViewCtrl",
				templateUrl: "templates/jobs/job.html"
			}		
		}
	},

	job: {
		url: "/jobs/:jobTitle",
		params: {job: {}},
		views:{
			"app-sub-view":{
				controller: "JobViewCtrl",
				templateUrl: "templates/jobs/job.html"
			}		
		}
	},

	bids: {
		url: "/bids/:jobTitle",
		params: {job: {}},
		views:{
			"app-sub-view":{
				controller: "BidCtrl",
				templateUrl: "templates/jobs/bid-history-no-dialog.html"
			}		
		}
	},

	jobs: {
		url: "/jobs",		
		views:{
			"app-sub-view":{
				controller: "JobIndexCtrl",
				templateUrl: "templates/jobs/index.html"
			}		
		}
	},

	createjob: {
		url: "/create-job",
		params: {freelancer: undefined},
		views:{
			"app-sub-view":{
				controller: "JobCreateCtrl",
				templateUrl: "templates/jobs/create.html"
			}		
		}
	},

	createhfhejob: {
		url: "/create-hfhe-job",
		views:{
			"app-sub-view":{
				controller: "JobCreateHfheCtrl",
				templateUrl: "templates/jobs/create-hfhe-job.html"
			}		
		}
	},

};