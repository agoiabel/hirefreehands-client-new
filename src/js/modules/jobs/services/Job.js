/**
*	Job Service
*
*	Handles all common functionality
*	for the job resource
*/
module.exports = function($localStorage,$firebaseArray,firebaseURL,Restangular){

	'use strict';

	var that = this, //maintain this scope for "this"
		store = $localStorage,
		hermes = Restangular,
		allCategories = null,
		chatRef = new Firebase(firebaseURL+'/chats');

	/**
	*	Get All Categories
	*
	*	Retrieve the entire set of categories available
	*	equipped with their respective, milestones,skills and add ons
	*/
	that.getAllCategories = function(){
		return hermes.one('category/index').get();
	};

	/**
	 * Get a single category record
	 * 
	 * @param category_id 
	 * @return 
	 */
	that.getCategory = function (category_id) {
		console.dir(category_id);
		return hermes.one('category/show/'+category_id).get();
	}

	// that.getAllCategories = function(){

	// 	return hermes.one('categories').get({include: 'milestones,addons,skills'});
	// };

	/**
	*	Get Skills
	*
	*	Retrieve the entire set of skills available
	*/
	that.getSkills = function(){
		return hermes.one('skills').get();
	};

	/**
	*	Get All Coupons
	*
	*	Retrieve the entire set of coupons available
	*	equipped with their weight
	*/
	that.getAllCoupons = function(){

		return hermes.one('coupon/index').get();
	};

	/**
	*	Post
	*
	*	Post a new job
	*
	*	@param object new job
	*/
	that.post = function(job){
		// console.dir(job);
		return hermes.all('job/store').post(job);
	};

	/**
	*	Get All
	*
	*	Retrieve all available jobs
	*/
	that.getAll = function(){
		return hermes.one('job/index').get();
	};

	/**
	*	Get
	*
	*	Retrieve a single job
	*
	*	@param string job ID
	*	@param string string of data headings to include
	*/
	that.get = function(jobId,includes){

		includes = (includes === undefined) ? 'addons,milestones,categorysize' : includes;
		return hermes.one('jobs/get/'+jobId).get({include:includes});
	};

	/**
	*	Get Bids Ref
	*
	*	Retrieve all bids for a given job
	*
	*	@param object job
	 *	change it back to job-
	*
	*/
	that.getBidsRef = function(job){
		return chatRef.child('testjob-'+job.id);
	};

	/**
	*	Get Bids
	*
	*	Retrieve all bids for a given job
	*
	*	@param object job
	*/
	that.getBids = function(job){

		return $firebaseArray(that.getBidsRef(job));
	};

	/**
	*	Set Freelancer
	*
	*	Assign freelancer to a job, after accepting bid
	*
	*	@param object freelanecer details
	*/
	that.setFreelancer = function(freelancer){

		return hermes.all("job/set_freelancer").post(freelancer);
	};

	/**
	*	Sign Agreement
	*
	*	For both clients and freelancers
	*
	*	@param object details
	*/
	that.signAgreement = function(data){

		return hermes.all("sign_agreement").post(data);
	};

	/**
	*	Review and Close Job
	*
	*	Save client review and close the job
	*
	*	@param object client review and job id
	*/
	that.reviewAndCloseJob = function(data){
		return hermes.all("jobs/review_and_close").post(data);
	};

	/**
	*	Update job/milestone status
	*
	*	0  -> Job is in progress/milestone is approved as done
	*	1  -> Job is in complete/milestone is done
	*	2  -> Job is in awaiting approval/milestone is marked as done by freelancer
	*
	*	@param object job id and optionally milestone_id
	*/
	that.updateStatus = function(data){
		return hermes.all("jobs/update_status").post(data);
	};

	/**
	*	Create Log
	*
	*	Create a log entry for the specified job
	*
	*	@param object job log details
	*/
	that.createLog = function(data){
		return hermes.all("jobs/create_log").post(data);
	};

	/**
	*	Get Updates
	*
	*	Retrieve updates[mostly job related] that were
	*	directed to the current user.
	*/
	that.getUpdates = function(){
		return hermes.one("jobs/get_updates").get();
	};

	/**
	*	Post
	*
	*	Post a new job
	*
	*	@param object new job
	*/
	that.postNumber = function(url, data){
		return hermes.all(url).post(data)
	};


	return that;
};
