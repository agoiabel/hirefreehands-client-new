/**
*	Filter
*
*	Used to filter freelancers.
*/
module.exports = function(Freelancer){

	'use strict';

	return {
		'restrict': 'EA',
		'templateUrl': 'templates/freelancers/filter.html',
		link: function($scope,element,attrs){

			// Define variable to hold the entire freelancers list for future operations. 
			var _freelancers = [];
			
			// Filters action state to determine which filter is acitivated.
			var filtersActionState = { rating: false, skill: false };

			// Define helper functions
			var helpers = {
				getSelectedValues: function (collection) {

					var selectedValues = [];

					 _.forEach(collection, function (value, key) {
						if (value) { selectedValues.push(key); }
					});

					return selectedValues;
				},

				toArrayOfNumbers: function (arrayOfStringNumbers) {

					return _.map(arrayOfStringNumbers, function (strNum) {

						return _.toNumber(strNum);
					});
				}
			};

			// Define filter workers. 
			var filterBy = {
				rating: function () {

					// Activate rating filter.
					filtersActionState.rating = true;

					return function (freelancers) {

						var ratingFilterValues = [];

						var filter = function (freelancer) {

							var freelancerRating = freelancer.average_rating,
								selectedRatings = helpers.toArrayOfNumbers(ratingFilterValues);

							// If all rating switches are off, return all freelancers and deactivate filter.
							if (selectedRatings.length === 0) {
								filtersActionState.rating = false;
								return true; 
							}

							return selectedRatings.indexOf(freelancerRating) > -1;
						};

						ratingFilterValues = helpers.getSelectedValues($scope.filter.selectedRatings);

						return _.filter(freelancers, filter);
					};
				},
				
				skill: function () {

					// Activate skillset filter.
					filtersActionState.skill = true;

					return function (freelancers) {

						var skillFilterValues = [];

						var filter = function (freelancer) {

							var freelancerSkills = _.map(freelancer.skills.data, 'name'),
								selectedSkills = skillFilterValues;

							// If all skill switches are off, return all freelancers and deactivate filter.
							if (selectedSkills.length === 0) { 
								filtersActionState.skill = false;
								return true; 
							}

							return _.intersection(selectedSkills, freelancerSkills).length > 0;
						};						

						skillFilterValues = helpers.getSelectedValues($scope.filter.selectedSkills);
						
						return _.filter(freelancers, filter);
					};
				}
			};

			// Get all skillsets required for filtering.
			Freelancer.getSkills().then(function (response) {

				$scope.skills = response.data;
			}, function (error) {

				$mdToast.show(
	      			$mdToast.simple().content('Error occurred while retrieving skills.')
	        		.position('bottom right')
	        		.hideDelay(2000));
			}).finally(function () {

				$scope.isLoadingSkills =  false;
			});

			$scope.$watch('isLoading', function () {

				if (!$scope.isLoading) { _freelancers = $scope.freelancers; }
			});

			// Define and initialize scope variable to hold ratings (1 - 5).
			$scope.ratings = [1, 2, 3, 4, 5];

			// Initialize scope variable to hold skills to be retrieved.
			$scope.skills = [];

			// Define filter models.
			$scope.filter = {selectedRatings: {}, selectedSkills: {}};

			//scope variable for monitoring the asynchronous status of skills resource.
			$scope.isLoadingSkills =  true;

			// Determine what filter to initialize by specified filter type.
			$scope.filterBy = function (filterType) {

				var _tempFreelancers = _freelancers,
					results = [];
				
				// Start a new filtering operation for the specified filter type.
				results = filterBy[filterType].call()(_tempFreelancers);

				// Run `results` through other activated filters (if any).
				for (var _filterType in filtersActionState) {

					if (_filterType !== filterType && filtersActionState[_filterType]) {
						results = filterBy[_filterType].call()(results);
					}
				}

				// Finally bind completely-filtered freelancers to the scope view.
				$scope.freelancers = results;
			};
		}
	};
};