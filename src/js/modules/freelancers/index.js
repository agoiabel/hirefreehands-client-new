/**
 * 	Freelancers Module Definition
 */

//require the dependencies
var angular = require('angular');

//define the Projects module
var freelancerModule = angular.module('freelancerModule',[]);

//define controllers
freelancerModule.controller('FreelancerCtrl',require('./controllers/FreelancerCtrl.js'));
freelancerModule.controller('FreelancerVerificationCtrl',require('./controllers/FreelancerVerificationCtrl.js'));

//define services
freelancerModule.factory('Freelancer',require('./services/freelancer.js'));

//define directives
freelancerModule.directive('freelancersFilter', require('./directives/freelancersFilter.js'));