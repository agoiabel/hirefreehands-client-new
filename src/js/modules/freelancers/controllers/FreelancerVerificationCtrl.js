/**
*	Freelancer Verification Controller
*/
module.exports = function($scope,$mdDialog,$mdToast,$auth,$timeout,Uploader,Freelancer,Auth,Notification){

	'use strict';

  function init () {

      function showVerificationRestrictionDialog ($event) {

          var parentEl = angular.element(document.querySelector(".dynamic-content-area"));

          $mdDialog.show({
              parent: parentEl,
              clickOutsideToClose: false,
              escapeToClose: false,
              targetEvent: $event,
							scope: $scope, //this $scope will be the scope of the enclosing controller,
              templateUrl: 'templates/utilities/verification-restriction.html',
              controller: function DialogController(scope, $state) {

							scope.goToSupportSection = function () {
								$state.go('app.support');
							};
						}
          });
      }

			if (!Auth.isVerifiedRole('HFH_AGENT')) {
				showVerificationRestrictionDialog();
			}
	}

	//for upload progress bar
	$scope.showProgress = false;

	//flag for loading chat history
	$scope.isLoadingChat = true;

	//for checklist item indicators
	$scope.checklist = Auth.getVerificationChecklist();
	$scope.$watch('isChecklistRefreshed',function(){

		$scope.checklist = Auth.getVerificationChecklist();
	});

	/**
	*	set the current active freelancer which could be the current logged in user
	*	or the one chosen by an HFH Agent
	*/
	$scope.activeFreelancerId = Auth.getUser().uid;
	$scope.freelancers = [];

	//crunch available freelancers
	if(Auth.isVerifiedRole('HFH_AGENT')){

		//retrieve the Whole verification chat history for all freelancers
		var allVerificationChat = Freelancer.getVerificationChatHistory('',true);

		allVerificationChat.$loaded()
		.then(function(){

			$scope.freelancers = _.map(allVerificationChat,function(item){

				return {name:item.name,id: item.$id};
			});

			makeActiveFreelancer($scope.freelancers[0]);
		});
	}else{
		makeActiveFreelancer({id:$scope.activeFreelancerId});
	}

	/**
	*	Identity Verification
	*
	*	Upload image for means of identification
	*
	*	@param object file
	*/
	$scope.uploadIdentity = function(file){

		if(file !== null){

			//save freelancers documents in /freelancers/<freelancerId>/<unique_document_ref>
			var fileExt = file.type.split('/')[1];
			var saveAs = 'freelancers/'+Auth.getUser().uid+'/'+(new Date()).getTime()+'.'+fileExt;

			//call the uploader
	    	Uploader.upload(file,saveAs)
	    	.then(function(response){

				//retrieve the file url
				var url = Uploader.getAssetUrl(response);

				//Save to chat thread for the current logged in prospective freelancer
				var message_freelancer = {
					attachment: {
						type: 'IMAGE',
						url: url
					},
					posterId: Auth.getUser().uid,
					posterName: Auth.getUser().firstname+' '+Auth.getUser().lastname,
					posted: (new Date()).getTime()
				};

				//send system message encouraging freelancer to continue verification process
				var message_agent = {
					comment: 'Your file has been successfully uploaded. For the next step, click the button below to submit your mobile phone number. We will give you a brief call over the next 24 hours on the number you submit and ask you a few questions for authentication',
					posterId: 'SYSTEM',
					posterName: 'HFH ADMIN',
					nextItem: 'UPLOAD_PHONE',
					posted: (new Date()).getTime()
				};

				var posts = [message_freelancer,message_agent];

				Freelancer.appendPosts({

					uid: Auth.getUser().uid,
					name: Auth.getUser().firstname+' '+Auth.getUser().lastname,
					posts: posts
				});

			},function(error){

				$mdToast.show(
			      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
			        .position('bottom right')
			        .hideDelay(2000)
			    );

			},function(evt){

				//reveal and update upload bar
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadProgress = progressPercentage+'%';
				$scope.showProgress = true;

			}).finally(function(){

				//hide progress bar
				$scope.showProgress = false;
			});
		}
	};

	/**
	*	Upload Phone Number
	*/
	$scope.uploadPhoneNumber = function(){

		$mdDialog.show({

			controller: function($scope,Freelancer,User){

				//country code
				$scope.countryCode = "+234";

				//freelancer number suffix scope variable
				$scope.numberSuffix = "";

				//scope error store
				$scope.error = {message:''};

				/**
				*	Upload Phone Number
				*/
				$scope.doUploadPhoneNumber = function(){

					var number = $scope.countryCode + $scope.numberSuffix;

					$scope.error.message = '';
					$scope.isUploadingPhoneNumber = true;

					User.updatePhoneNumber({number:number})
					.then(function(){

						var posterName = Auth.getUser().firstname+' '+Auth.getUser().lastname;

						//update chat with user BVN
						var message_freelancer = {
							comment: "Phone Number: "+number,
							posterId: Auth.getUser().uid,
							posterName: posterName,
							posted: (new Date()).getTime()
						};

						//send system message encouraging freelancer to continue verification process
						var message_agent = {
							comment: "Good!. Now, click the button below to submit your BVN (Bank Verification Number) and bank account info. We need this to ensure the safety of all the income you make from your freelance jobs. Whenever you get paid from a completed job, you can cash out and have your money securely transferred straight to your bank account.",
							posterId: 'SYSTEM',
							posterName: 'HFH ADMIN',
							nextItem: 'UPLOAD_BVN',
							posted: (new Date()).getTime()
						};

						var posts = [message_freelancer,message_agent];

						Freelancer.appendPosts({

							uid: Auth.getUser().uid,
							name: Auth.getUser().firstname+' '+Auth.getUser().lastname,
							posts: posts
						});

						$mdDialog.cancel();
					},function(error){
						$scope.error.message = error.data.message;
					}).finally(function(){
						$scope.isUploadingPhoneNumber = false;
					});
				};

				/**
				*	close dialog
				*/
				$scope.closeDialog = function(){$mdDialog.cancel();};
			},
			clickOutsideToClose: true,
			templateUrl: 'templates/freelancers/upload-phone-number.html',
	      	parent: angular.element(document.body)
		});
	};

	/**
	*	Upload BVN
	*/
	$scope.uploadBVN = function(){

		$mdDialog.show({

			controller: function($scope,Freelancer,User){

				//freelancer BVN scope variable
				$scope.records = {bvn:'',acc_number:'',bank:''};

				//scope error store
				$scope.error = {message:''};

				/**
				*	Get All Banks
				*/
				$scope.getAllBanks = function(){

					return User.getAllBanks()
						   .then(function(response){
						   		$scope.banks = response.data;
						   });
				};

				/**
				*	Upload Bank Verification Number
				*
				*	@param object financial records
				*/
				$scope.doUploadBVN = function(records){

					$scope.error.message = '';
					$scope.isUploadingBVN = true;

					User.updateFinance(records)
					.then(function(){

						var posterName = Auth.getUser().firstname+' '+Auth.getUser().lastname;

						//update chat with user BVN
						var message_freelancer = {
							comment: "BVN: "+records.bvn,
							posterId: Auth.getUser().uid,
							posterName: posterName,
							posted: (new Date()).getTime()
						};

						//send system message encouraging freelancer to continue verification process
						var firstname = Auth.getUser().firstname.substr(0,1).toUpperCase() + Auth.getUser().firstname.substr(1);
						var message_agent = {
							comment: "Thank you "+firstname+". Your banking details have been successfully submitted. We will verify all your information under the next 24 hours and get you active in no time. For now, take some time to make your user profile as interesting and attractive as possible. If you feel you have entered any information in error, click the button below to restart the verification process",
							posterId: 'SYSTEM',
							posterName: 'HFH ADMIN',
							posted: (new Date()).getTime()
						};

						var posts = [message_freelancer,message_agent];

						Freelancer.appendPosts({

							uid: Auth.getUser().uid,
							name: Auth.getUser().firstname+' '+Auth.getUser().lastname,
							posts: posts
						});

						$mdDialog.cancel();
					},function(error){
						$scope.error.message = error.data.message;
					}).finally(function(){
						$scope.isUploadingBVN = false;
					});
				};

				/**
				*	close dialog
				*/
				$scope.closeDialog = function(){$mdDialog.cancel();};
			},
			clickOutsideToClose: true,
			templateUrl: 'templates/freelancers/upload-bvn.html',
	      	parent: angular.element(document.body)
		});
	};

	/**
	*	Continue verification
	*
	*	Leads freelancer to the next step to verify
	*	their accounts
	*
	*	@param string item code for the next verification to open up
	*/
	$scope.continueVerification = function(item){

		switch(item){

			case 'LINKEDIN': $scope.signinViaLinkedin();break;
			case 'UPLOAD_IDENTITY': $scope.uploadIdentity();break;
			case 'UPLOAD_PHONE': $scope.uploadPhoneNumber();break;
			case 'UPLOAD_BVN': $scope.uploadBVN();break;
		}
	};

  	/**
  	*	Upload
  	*
  	*	Upload and save file details
  	*	used for freelancer verification
  	*
  	*	@param object file
  	*/
    $scope.upload = function (file) {

    	if(file !== null){

    		//save freelancers documents in /freelancers/<freelancerId>/<unique_document_ref>
    		var fileExt = file.type.split('/')[1];
    		var saveAs = 'freelancers/'+Auth.getUser().uid+'/'+(new Date()).getTime()+'.'+fileExt;

			//call the uploader
	    	Uploader.upload(file,saveAs)
	    	.then(function(response){

				//retrieve the file url
				var url = Uploader.getAssetUrl(response);

				//Save to chat thread for the current logged in prospective freelancer
				var posterName = Auth.isVerifiedRole('HFH_AGENT') ? 'HIRE FREE HANDS AGENT' : Auth.getUser().firstname+' '+Auth.getUser().lastname;
				var message = {
					attachment: {
						type: 'IMAGE',
						url: url
					},
					posterId: Auth.getUser().uid,
					posterName: posterName,
					posted: (new Date()).getTime()
				};

				$scope.verificationChatHistory.$add(message);

			},function(error){

				$mdToast.show(
			      	$mdToast.simple().content('There was a problem while we were trying to upload, please try again.')
			        .position('bottom right')
			        .hideDelay(2000)
			    );

			},function(evt){

				//reveal and update upload bar
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadProgress = progressPercentage+'%';
				$scope.showProgress = true;

			}).finally(function(){

				//hide progress bar
				$scope.showProgress = false;
			});
		}
    };

    /**
    *	Post
    *
    *	Send message to or from
    *	prospective freelancers
    *
    *	@param string message/comment
    */
    $scope.post = function(comment){

    	if(Boolean(comment)){

    		var posterName = Auth.isVerifiedRole('HFH_AGENT') ? 'HIRE FREE HANDS AGENT' : Auth.getUser().firstname+' '+Auth.getUser().lastname;
    		var message = {
				comment: comment,
				posterId: Auth.getUser().uid,
				posterName: posterName,
				posted: (new Date()).getTime()
			};

			//reset text field
			$scope.comment = '';

    		Freelancer.getVerificationChatHistory(Auth.getUser().uid).$add(message);
    	}
    };

    /**
    *	Owns Post
    *
    *	Confirms if the logged in user is the owner of the post
    *
    *	@param object post
    */
    $scope.ownsPost = function(post){

    	return post !== undefined && post.posterId === Auth.getUser().uid;
    };

    /**
    *	Verify Freelancer Item
    *
    *	Verify an item off a prospective freelancer's checklist.
    *	This function also doubles as the interface for freelancers
    *	to upload and update their information to be verified.
    *
    *	@param string item
    */
    $scope.verifyFreelancerItem = function(item){

    	//prepare data
	    var approverId = Auth.getUser().uid;
	    var userId = $scope.activeFreelancerId;

    	//only agents can do this, and for other users other than themselves
    	if(Auth.isVerifiedRole('HFH_AGENT') && userId !== approverId){

	    	var data = {user_id: userId,approver_id:approverId, item: item};

	    	//toggle item indicator on
	    	$scope.checklist[item] = true;

	    	Freelancer.verifyFreelancerItem(data)
	    	.then(function(response){

	    		if(response.isVerifiedFreelancer){

	    			//get current user roles
	    			var roles = Auth.getUserRoles();
	    			roles.push('FREELANCER');

	    			Auth.setUserRoles(roles);
	    		}

	    		//all is well, no need to toggle the item indicator, notify the freelancer tho
	    		var rhetoric = 'Your '+item+' status has just been verified';
	    		rhetoric += (response.isVerifiedFreelancer) ? '. You are now a fully verified freelancer, now you can bid for any job you want' : '';
	    		var notification = {

					message: rhetoric,
					sender_id: Auth.getUser().uid,
					sender_fullname: 'HIRE FREE HANDS AGENT',
					to_id: $scope.activeFreelancerId,
					type: "VERIFICATION_STATUS",
					resource_id: $scope.activeFreelancerId
				};

				Notification.send(notification);
	    	},function(error){

	    		//toggle item indicator off
	    		$scope.checklist[item] = false;

	    		$mdToast.show(
			      	$mdToast.simple().content('An error occured while trying to verify item, please try again.')
			        .position('bottom right')
			        .hideDelay(2000)
			    );
	    	});
		}else if(!Auth.isVerifiedRole('HFH_AGENT')){

			/*switch(item){

				case 'LINKEDIN': $scope.signinViaLinkedin();break;
				case 'PHONE_INTERVIEW': $scope.uploadPhoneNumber();break;
				case 'BVN': $scope.uploadBVN();break;
			}*/
		}
    };

    /**
    *	Is Agent
    *
    *	Evaluates if logged in user is HFH agent
    */
    $scope.isAgent = function(){

    	return Auth.isVerifiedRole('HFH_AGENT');
    };

    /**
    *	Is Active Freelancer
    *
    *	@param string freelancerId
    */
    $scope.isActiveFreelancer = function(freelancerId){
    	return $scope.activeFreelancerId === freelancerId;
    };

    /**
    *	Make Active Freelancer
    *
    *	@param object freelancer
    */
    $scope.makeActiveFreelancer = function(freelancerId){
    	makeActiveFreelancer(freelancerId);
    };

    function makeActiveFreelancer(freelancer){

    	$scope.activeFreelancerId = freelancer.id;
    	$scope.activeFreelancerName = freelancer.name;
    	$scope.verificationChatHistory = Freelancer.getVerificationChatHistory($scope.activeFreelancerId);

    	$scope.verificationChatHistory.$loaded().finally(function(){
    		$scope.isLoadingChat = false;
    	});
    }

    init();
};
