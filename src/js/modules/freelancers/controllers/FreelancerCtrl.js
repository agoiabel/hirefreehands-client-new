/**
*	Freelancers Controller
*/
module.exports = function($scope,$state,$mdDialog, $mdToast, Freelancer){

	//all available freelancers scoped variable
	$scope.freelancers = [];

	$scope.isLoading = true;
	Freelancer.getAll().then(function(response){

		$scope.freelancers = response.data;
	},function(error){
		$mdToast.show(
	      	$mdToast.simple().content('There was a problem while we were retrieving freelancers.')
	        .position('bottom right')
	        .hideDelay(2000)
	    );
	}).finally(function(){
		$scope.isLoading = false;
	});

	$scope.joinSkills = function(skills){

		var _skills = skills.data;

		if(_skills.length > 0) {
			return _skills.map(function(skill){
			    return skill.name;
			}).join(", ");
		}

		return "No skills specified";
	};

	//reveal create project dialog
	$scope.viewFreelancerProfile = function(freelancer,ev){
	
	    $mdDialog.show({
	      locals:{freelancer:freelancer,imageUrl:$scope.imageUrl,hireMe:$scope.hireMe,getUserRole:$scope.getUserRole,getUserInitials:$scope.getUserInitials},
	      controller: function($scope,freelancer,imageUrl,hireMe,getUserRole,getUserInitials){

	      	$scope.imageUrl = imageUrl;
	      	$scope.hireMe = hireMe;
	      	$scope.getUserRole = getUserRole;
					$scope.getUserInitials = getUserInitials;

	      	//make freelancer info available in the view
	      	$scope.user = freelancer;	      	
	      	$scope.user.skills = $scope.user.skills.data;

	      	//this view is for other users that want to see a freelancer's profile and probably hire, create flag for this
	      	$scope.isOwner = false;

	      	/**
			*	Go To Tab
			*
			*	Makes selected tab index active
			*
			*	@param int tab index
			*/
			$scope.goToTab = function(tabIndex){
				$scope.selectedIndex = tabIndex;
			};	
	      },
	      templateUrl: 'templates/freelancers/profile.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true
	    });
	};
};