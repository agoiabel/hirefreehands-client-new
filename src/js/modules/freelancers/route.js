/**
*	Route definitions for the freelancer module
*/
module.exports = {
	
	freelancers: {
		url: "/freelancers",		
		views:{
			"app-sub-view":{
				controller: "FreelancerCtrl",
				templateUrl: "templates/freelancers/index.html"
			}		
		}
	},
	verification: {
		url: "/freelancers/verification",		
		views:{
			"app-sub-view":{
				controller: "FreelancerVerificationCtrl",
				templateUrl: "templates/freelancers/verification.html"
			}		
		}
	}
};