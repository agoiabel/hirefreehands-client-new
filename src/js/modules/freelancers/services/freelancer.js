/**
*	Freelancer Service
*
*	Handles all common functionality
*	concerning freelacers
*/
module.exports = function($firebaseArray,$q,Restangular,firebaseURL){

	var factory = {},
		hermes = Restangular;
		verificationChatRef = new Firebase(firebaseURL+'/verifications');

	/**
	*	Get Verification Chat Ref
	*
	*	Retrieve either the base Firebase location
	*	for freelancer verification chats or Firebse location for
	*	chats with a particular freelancer.
	*
	*	@param string FreelancerId[optional]
	*/
	factory.getVerificationChatRef = function(uid){
		return (uid === undefined) ? verificationChatRef : verificationChatRef.child(uid);
	};

	/**
	*	Get Skills
	*
	*	Retrieve the entire set of skills available
	*/
	factory.getSkills = function(){
		return hermes.one('skills').get();
	};

	/**
	*	Get Verification Chat History
	*
	*	Retrieve a Firebase Synced array
	*	on the required Firebase location ie
	*	either the entire verification chat history
	*	or for a particular freelancer.
	*
	*	@param string FreelancerId[optional]
	*	@param bool whethter or not to return the whole verification location[optional]
	*/
	factory.getVerificationChatHistory = function(uid,all){

		if(all === true){
			return $firebaseArray(factory.getVerificationChatRef());
		}
		return $firebaseArray(factory.getVerificationChatRef(uid).child('posts').orderByChild('posted').limitToLast(100));
	};

	/**
    *	Verify Freelancer Item
    *
    *	Verify an item off a prospective freelancer's checklist
    *
    *	@param object freelancer id,item type, approver id
    */
	factory.verifyFreelancerItem = function(data){

		return hermes.all('auth/mark_verification_checklist').post(data);
	};

	/**
    *	Append Posts
    *
    *	Appends post to current post thread of the specified
    *	freelancer or initialises the reference with a name and the posts
    *
    *	@param object uid,name,posts
    */
	factory.appendPosts = function(data){

		return $q(function(reject,resolve){

			//check if the ref exists
			var ref = factory.getVerificationChatRef(data.uid+'/'+name);
			ref.once("value", function(snap) {

			  if(snap.val() === null){

			  	//initialise the verification ref
			  	factory.getVerificationChatRef(data.uid).set({
			  		name: data.name,
			  		posts: data.posts
			  	},function(error){
			  		if(error){
			  			reject(error);
			  		}else{
			  			resolve();
			  		}
			  	});
			  } else {

			  	//append posts
			  	var error;//keep track  of the last error
			  	data.posts.forEach(function (post) {

			  		factory.getVerificationChatRef(data.uid+'/posts').push(post,function (err) {
				  		if (err) {
				  			error = err;
				  		}
				  	});
			  	});

			  	if (error !== undefined) {
			  		reject(error);
			  	} else {
			  		resolve();
			  	}
			  }
			});
		});
	};

	/**
	*	Get
	*
	*	Retrieve one verified freelancers
	*
	*	@param {String} user id
	*/
	factory.get = function (uid) {

		return hermes.one('freelancers/get').get({uid:uid});
	};

	/**
	*	Get All
	*
	*	Retrieve all verified freelancers
	*/
	factory.getAll = function () {

		return hermes.one('freelancers').get();
	};

	return factory;
};
