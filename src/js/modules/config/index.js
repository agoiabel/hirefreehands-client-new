/**
 * TaskMe App Wide Config
 *
 * @author KesienaAkp<@boureois247>
 */

// TODO: Handle global app config more dynamically.

module.exports = {

	/**
	*	Specifies What State The App Is Currently In.
	*
	*	Possible options include: ok
	*	development
	*	production
	*	staging
	*/
	environment: 'development',

	/**
	*	Base URL of the App
	*/
	baseUrl: {

		development: 'http://hirefreehands.demo/',
		production:  'http://hirefreehands.demo/',
		staging:  'https://staging.hirefreehands.com/'
	},

	/**
	*	Cloudinary Base Url
	*	Base URL for images in production
	*/
	cloudinary: "http://res.cloudinary.com/careerpedia/image/upload/",

	/**
	*	Base url for API
	*	for different application environments
	*/
	API:{

		development:'http://127.0.0.1:8000/api/',
		staging:'http://staging-api.hirefreehands.com/',
		production:'https://127.0.0.1:8000/api/'
	},

	/**
	*	Retrieve base API endpoint
	*/
	baseAPI: function() {
		return this.API[this.environment];
	},

	/**
	*	Prefix images with the proper address location
	*	in different application states
	*
	*	@param string string to be prefixed|optional
	*	@param string image manipulation specs|optional
	*/
	imageUrl: function(arg,specs){

		var baseUrl = '';

		switch(this.environment){

			case 'production': baseUrl = this.cloudinary; break;
			case 'development': baseUrl = this.baseUrl.development + 'public/dist/imgs/'; break;
			default: baseUrl = this.baseUrl.staging + 'public/dist/imgs/';
		}

		if(arg !== undefined){

			//if this is an absolute url, don't append the baseUrl
			if(/(?:http(s)?:\/\/)+/.test(arg)){
				return arg;
			}

			return (specs !== undefined) ? baseUrl+specs+'/'+arg : baseUrl+arg;
		}
	}
};
