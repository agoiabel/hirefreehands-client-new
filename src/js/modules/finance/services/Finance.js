/**
*	The finance service
*	
*	Handles most activities relating to financial transactions
*/
module.exports = function(Restangular){

	'use strict';

	var that = this,
		hermes = Restangular;

	/**
	*	History
	*	
	*	Retrieve wallet balance and financial history for the
	*	logged in user
	*/
	this.history = function () {
		return hermes.one('finance/history').get();
	};

	/**
	*	Cash out
	*	
	*	Submit request to cash out.
	*/
	this.cashOut = function (params) {
		return hermes.all('jobs/cashout').post(params);
	};

	return that;
};
