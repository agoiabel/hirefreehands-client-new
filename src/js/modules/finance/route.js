/**
 * Route definitions for the Finance Module
 */
module.exports = {
	
	finance: {
		url: "/wallet",		
		views:{
			"app-sub-view":{
				controller: "FinanceCtrl",
				templateUrl: "templates/finance/index.html"
			}		
		}
	}
};