/**
 * Finance Module Definition
 */

//require the dependencies
var angular = require('angular');

//define the Finance module
var financeModule = angular.module('financeModule',[]);

//define controllers
financeModule.controller('FinanceCtrl',require('./controllers/FinanceCtrl.js'));

//define services
financeModule.service('Finance',require('./services/Finance.js'));