/**
 * HireFreeHands AppJS
 *
 * Formerly known as the TaskMe AppJS
 *
 * @version V0.0.1
 * @author KesienaAkp<@bourgeois247>
 *
 * Serves as entry point for the Hirefreehands app.
 * This file requires and bundles all other dependencies
 * of the application namely:
 *
 * Vendor Modules
 * --------------
 *
 * Angular-UI-Router
 * Restangular
 * Angular
 * lodash
 * Firebase
 * Moment
 *
 * Project Modules
 * ----------------
 * Config
 * Auth
 * Dashboard
 * Project
 * Finance
 * Users
 * Jobs
 */

//vendor modules
var angular = require('angular');

var firebaseURL = "https://hfh.firebaseio.com/";

//make JQuery available on the global window
window.$ = require('../../public/lib/jquery/dist/jquery.js');

//make lodash globally available for Restangular
window._ = require('lodash');

//patch the now updated contains method
_.contains = _.includes;

require('angular-ui-router');
require('restangular');
require('ng-animate');
require('ng-aria');
// require('angular-sanitize');
require('ng-material');
require('ng-storage');
require('firebase');
require('moment');
require('angular-moment');
require('ng-file-upload');
require('angularfire');
require('angular-messages');
require('angular-timer');
require('angular-avatar');

//project specific modules
var config = require('./modules/config');
require('./modules/auth');
require('./modules/app');
require('./modules/dashboard');
require('./modules/jobs');
require('./modules/users');
require('./modules/finance');
require('./modules/freelancers');
require('./modules/utilities');

angular.module('taskme',[
	'ui.router',
	'restangular',
	'ngMaterial',
	'authModule',
	'ngStorage',
	'ngMessages',
	// 'ngSanitize',
	'ngAvatar',
	'firebase',
	'ngFileUpload',
	'timer',
	'angularMoment',
	'appModule',
	'dashboardModule',
	'jobModule',
	'freelancerModule',
	'financeModule',
	'utilitiesModule',
	'usersModule'
])
.constant("firebaseURL",firebaseURL)
.run(function($rootScope,$location,$state,$sce,$timeout,$mdToast,Restangular,Auth,Notification,Search){

	//set Restangular base URL
  Restangular.setBaseUrl(config.baseAPI());

  //add authorization header on every request if available
	Restangular.addFullRequestInterceptor(
    function(element, operation, route, url, headers, params, httpConfig) {

      	//if user is logged in, add Authorization Header
      	if(Auth.isLoggedIn()){
        	_.extend(headers,{Authorization: Auth.getAuthorization()});
      	}

      	return {
	        element: element,
	        params: params,
	        headers: headers,
	        httpConfig: httpConfig
      	};
	});

	//Update user freelancer checklist and roles on refresh
	$rootScope.isChecklistRefreshed = false;
	if(Auth.getVerificationChecklist().length === 0) {

		//refresh checklist
		Auth.refreshFreelancerVerificationChecklist()
		.then(function(response){
			$rootScope.isChecklistRefreshed = true;
			Auth.setFreelancerVerificationStatus(response.data);
		});

		//refresh user roles
		Auth.refreshUserRoles()
		.then(function(response){
			Auth.setUserRoles(response.data);
		});
	}

	//Listen for new notifications for this user
	if(Auth.isLoggedIn()) {

		Notification.getUserNotificationsRef(Auth.getUser().uid).on('child_added',function(notification) {

			var refKey = notification.key();
			notification = notification.val();
			notification.refKey = refKey;

			//run in new digest cycle
			$timeout(function(){

				//update notification count
				if(!notification.read){

					$rootScope.unreadNotifications = $rootScope.unreadNotifications === undefined ? 1 : ++$rootScope.unreadNotifications;
				}

				//update notification store
				if($rootScope.notifications === undefined){
					$rootScope.notifications = [notification];
				}else{
					$rootScope.notifications.push(notification);
					$rootScope.notifications = $rootScope.notifications.reverse();
				}

				//update local notification store
				Notification.store = $rootScope.notifications;
			});
		});
	}

	//Deny access to pages that require authentcation
	$rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {

		//if the state being, transitioned to is not whitelisted and user is un-authenticated, then send back to the login page
		isNotWhitelisted = Auth.getWhitelist().indexOf(toState.name.toLowerCase()) === -1;
		if(isNotWhitelisted && !Auth.isLoggedIn()) {
			$location.path('/login');
		}
	});

	/**
	*	Setup a watcher to initiate search for
	*	all searchable resources ie jobs n freelancers @ the moment
	*/
	$rootScope.searchQuery = {value:""};
	$rootScope.$watch('searchQuery.value',function(){

		var minLength = 3;

		if($rootScope.searchQuery.value.length >= minLength){

			//check to see if a cached result exists
			$rootScope.isLoadingSearchResults = true;

			if(Search.getCacheItem($rootScope.searchQuery.value) !== undefined){
				$rootScope.searchResults = Search.getCacheItem($rootScope.searchQuery.value);
				$rootScope.isLoadingSearchResults = false;

			}else{
				Search.go($rootScope.searchQuery.value,'freelancers').then(function(response){

					$rootScope.searchResults = response.data;

					//cache results
					Search.updateCache($rootScope.searchQuery.value,response.data);
				},function(error){
						$mdToast.show(
								$mdToast.simple().content('We could not find that job!')
								.position('bottom right')
								.hideDelay(2000)
						);

				}).finally(function(){
					$rootScope.isLoadingSearchResults = false;
				});
			}
		}
	});

	/**
	* Spew Html
	*
	*	Allow HTML to interpolated on the front end
	*
	*	@param string raw string
	*/
	$rootScope.spewHTML = function(string){

		return $sce.trustAsHtml(string);
	};

	/**
	*	Force re-route to landing/welcome page
	*/
	$rootScope.goHome = function(){
		window.location.href="/";
	};

	/**
	*	Mark All Read
	*
	*	Mark all notifications as read
	*/
	$rootScope.markAllAsRead = function(){

		$rootScope.unreadNotifications = 0;

		//mark unread notifications as read on the firebase
		if($rootScope.unreadNotifications === 0){
			Notification.markAllAsRead(Auth.getUser().uid);
		}
	};

	/**
	*	Evaluate Notification
	*
	*	@param object notification
	*/
	$rootScope.evaluateNotification = function(notification){
		Notification.evaluate(notification);
	};

	/**
	*	Base URL
	*
	*	Prepend arguments with the base url of the
	*	current environment.
	*
	*	@param string URI to prepend
	*/
	$rootScope.baseUrl = function(args){

		args = args || "";
		return config.baseUrl[config.environment]+args;
	};

	/**
	*	ImageUrl
	*
	*	Prefix with the base url for images
	*/
	$rootScope.imageUrl = function(image){

		// TODO: Move function body here. Config has to go!!

		return config.imageUrl(image);
	};

	/**
	*	Set Profile Photo
	*
	*	Sets the global[rootscope] variable for the user's profile
	*	photo with canonical/relative link and also persists to local storage
	*
	*	@param string imageUrl
	*/
	$rootScope.setProfilePhoto = function(imageUrl){

		//update localstorage with imageUrl
		var user = Auth.getUser();
		user.image = imageUrl;

		Auth.saveUser(user);

		//update the global var
		$rootScope.profilePhoto = imageUrl;
	};

	/**
	*	Get Profile Photo
	*
	*	Retrieve the url for the logged
	*	user's profile
	*/
	$rootScope.getProfilePhoto = function(){

		$rootScope.profilePhoto = Auth.getUser().image;
		return $rootScope.profilePhoto;
	};

	/**
	*	Get User Role
	*
	*	Retrieve boolean specifying what kind of
	*	user is logged in.
	*
	*	@param string role to confirm
	*/
	$rootScope.getUserRole = function(role){

		return Auth.isVerifiedRole(role);
	};

	/**
	 * 	Get User Initials
	 * 	
	 * 	Retrieve fullname initials for authenticated user.
	 * 
	 */
	$rootScope.getCurrentUserInitials = function () {	

		var user = Auth.getUser();
		$rootScope.currentUserInitials = (user.lastname[0] + user.firstname[0]).toUpperCase();
		return $rootScope.currentUserInitials;
	};

	/**
	 * 	Get User Initials
	 * 	
	 * 	Retrieve fullname initials for authenticated user.
	 * 
	 */
	$rootScope.getUserInitials = function (lastname, firstname) {

		if (lastname && firstname) {
			return (lastname[0] + firstname[0]).toUpperCase();
		}
	};

	/**
	*	Get Current User
	*
	*	Retrieve current logged in user.
	*/
	$rootScope.getCurrentUser = function(){

		return Auth.getUser();
	};

	/**
	*	Logout
	*/
	$rootScope.logout = function(){

		Auth.logout();
		window.location.href="/";
	};
})
.config(function($stateProvider,$urlRouterProvider,$locationProvider){

	//switch on html5mode
	$locationProvider.html5Mode(true);

	//register routes
	$stateProvider
		.state('login',require('./modules/auth/route.js').login)
		.state('signup',require('./modules/auth/route.js').signup)
		.state('verify',require('./modules/auth/route.js').verify)
		.state('change_password',require('./modules/auth/route.js').change_password)
		.state('forgot_password',require('./modules/auth/route.js').forgot_password)
		.state('app',require('./modules/app/route.js').app)
		.state('app.profile',require('./modules/users/route.js').profile)
		.state('app.settings',require('./modules/users/route.js').settings)
		.state('app.support',require('./modules/users/route.js').support)
		.state('app.analytics',require('./modules/users/route.js').analytics)
		.state('app.dashboard',require('./modules/dashboard/route.js').dashboard)
		.state('app.job',require('./modules/jobs/route.js').job)
		.state('app.bids',require('./modules/jobs/route.js').bids)
		.state('app.jobs',require('./modules/jobs/route.js').jobs)
		.state('app.createjob',require('./modules/jobs/route.js').createjob)
		.state('app.createhfhejob',require('./modules/jobs/route.js').createhfhejob)
		.state('app.finance',require('./modules/finance/route.js').finance)
		.state('app.freelancerVerification',require('./modules/freelancers/route.js').verification)
		.state('app.freelancers',require('./modules/freelancers/route.js').freelancers);

	$urlRouterProvider.otherwise('/login');
});
